<?php defined('BASEPATH') or exit('No direct script access allowed');

function menu($name, $menu, $fa, $display)
{
    $active = ($name == $menu) ? 'active' : '';
    $href = base_url($name);
    echo "<li class = '$active'><a href='$href' ><i class='$fa'></i>$display</a> </li>";
}

function m_active($name, $menu)
{
    echo ($name == $menu) ? '<li class = "active">' : "<li>";
}

function sub($name, $sub, $display, $menu, $menu_name)
{
    $activeMenu = ($menu == $menu_name) ? "style = 'display: block'" : "";
    $activeStyle = ($name == $sub) ? "class = 'active'" : "";
    $href = base_url($name);

    $html = '';
    $html .= "<ul class='nav child_menu' $activeMenu>";
    $html .= "<li $activeStyle style = 'display: block'><a href='$href'>$display</a></li></ul>";

    echo $html;
}

function _action($data)
{
    if ($data['action'] == 'basic_action') {

        return '<a title="Edit" class="btn btn-info btn-xs"
        href="' . $data['edit_url'] . '"><i class="fa fa-pencil-square-o"></i></a>|

        <a id="form-' . $data['id'] . '" title="Hapus" class="btn btn-danger btn-xs form-delete"
        data-id="' . $data['id'] . '" data-url="' . $data['delete_url'] . '" data-confirm="' . $data['confirm'] . '"><i class="fa fa-trash-o"></i></a>';

    } else if ($data['action'] == 'transaction_action') {

        return '<a title="Edit" class="btn btn-info btn-xs"
        href="' . $data['edit_url'] . '"><i class="fa fa-pencil-square-o"></i></a>|

        <a id="form-' . $data['id'] . '" title="Hapus" class="btn btn-danger btn-xs form-delete"
        data-id="' . $data['id'] . '" data-url="' . $data['delete_url'] . '" data-confirm="' . $data['confirm'] . '"><i class="fa fa-trash-o"></i></a>|

        <a onclick="paymentPrint(' . "'" . base_url('transaction/payment_print/' . $data['id']) . "'" . ');"
        class="btn btn-xs btn-default"><i class="fa fa-print"> Kwitansi</i></a>';

    }else if($data['action'] == 'student_action'){

        return '<a title="Edit" class="btn btn-info btn-xs"
        href="' . $data['edit_url'] . '"><i class="fa fa-pencil-square-o"></i></a>|

        <a id="form-' . $data['id'] . '" title="Hapus" class="btn btn-danger btn-xs form-delete"
        data-id="' . $data['id'] . '" data-url="' . $data['delete_url'] . '" data-confirm="' . $data['confirm'] . '"><i class="fa fa-trash-o"></i></a>|

        <a href="'.base_url('student/payment_detail/'.$data['id']).'" class="btn btn-xs btn-default"><i class="fa fa-money"> Riawayat</i></a>';
    }
}
