<?php defined('BASEPATH') or exit('No direct script access allowed');

function response($response = [])
{
    header('Content-Type:application/json');
    echo json_encode($response);
}

function sendMail($email, $message, $subject)
{
    $ci = &get_instance();
    $setting = $ci->db->get('settings')->row();
    $ci->load->library('email');
    $config = array(
        'protocol' => $setting->protocol,
        'smtp_host' => $setting->mail_host,
        'smtp_port' => $setting->mail_port,
        'smtp_user' => SYSTEM_MAIL,
        'smtp_pass' => SYSTEM_MAILPASS,
        'mailtype' => 'html',
        'charset' => 'utf-8',
    );
    $ci->email->initialize($config);
    $ci->email->set_mailtype("html");
    $ci->email->set_newline("\r\n");
    $ci->email->to($email);
    $ci->email->from(SYSTEM_MAIL, SYSTEM_MAIL_ADMIN);
    $ci->email->subject($subject);
    $ci->email->message($message);
    $status = $ci->email->send();
    if ($status) {
        return true;
    } else {
        return false;
    }
}

function showOneTable($dt)
{
    $ci = &get_instance();
    $ci->load->model('datatables_model', 'dm');
    if (
        isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
        !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
        strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
    ) {
        $datatables = $_POST;
        $datatables['table'] = $dt['table'];
        $datatables['col-display'] = $dt['column'];
        $datatables['edit_url'] = $dt['edit_url'];
        $datatables['delete_url'] = $dt['delete_url'];
        $datatables['confirm'] = $dt['confirm'];
        $datatables['action'] = $dt['action'];
        $ci->dm->oneTable($datatables);
    }
    return;
}

function showQueryTable($dt)
{
    $ci = &get_instance();
    $ci->load->model('datatables_model', 'dm');
    if (
        isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
        !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
        strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
    ) {
        $datatables = $_POST;
        $datatables['query'] = $dt['query'];
        $datatables['col-display'] = $dt['column'];
        $datatables['searchOperator'] = $dt['searchOperator'];
        $datatables['edit_url'] = $dt['edit_url'];
        $datatables['delete_url'] = $dt['delete_url'];
        $datatables['confirm'] = $dt['confirm'];
        $datatables['action'] = $dt['action'];
        $ci->dm->queryTable($datatables);
    }
    return;
}

function flash()
{
    $ci = &get_instance();
    $ci->load->database();
    if ($ci->session->flashdata('notify')) {
        $notify = $ci->session->flashdata('notify');
        echo '<div class="alert alert-' . $notify['level'] . ' alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>'
            . $notify['message'] . '
            </div>';
    }
}

function auth()
{
    $ci = &get_instance();

    if ($ci->session->userdata(SESSION_KEY)) {
        if (me()['verified'] == 0) {
            $ci->session->set_flashdata('notify', [
                'message' => "User belum melakukan verifikasi email, silakan cek email dan klik yang sudah kami kirim",
                'level' => "danger",
            ]);
            $ci->session->unset_userdata(SESSION_KEY);
            redirect('login');
        }
    } else {
        redirect('login');
    }

}

function guest()
{
    $ci = &get_instance();
    if ($ci->session->userdata(SESSION_KEY)) {
        redirect('home');
    }
}

function me()
{
    return $data = array(
        'user_id' => $_SESSION[SESSION_KEY]['user_id'],
        'name' => $_SESSION[SESSION_KEY]['name'],
        'email' => $_SESSION[SESSION_KEY]['email'],
        'photo' => $_SESSION[SESSION_KEY]['photo'],
        'verified' => $_SESSION[SESSION_KEY]['verified'],
        'sidebar' => $_SESSION[SESSION_KEY]['sidebar']
    );
}

function generateVerificationToken()
{
    return $token = hash('sha256', time() . '-' . randomString(10));
}

function randomString($length)
{
    $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    $string = '';
    for ($i = 0; $i < $length; $i++) {
        $string .= $chars[rand(0, strlen($chars) - 1)];
    }
    return $string;
}

function dump($data)
{
    echo '<pre>';
    var_dump($data);
    die();
    echo '</pre>';
}

function moneyClean($money)
{
    $step_01 = str_replace("Rp. ", "", $money);
    $step_02 = str_replace(".", "", $step_01);
    return $step_02;
}

function toRp($number)
{
    if ($number >= 0) {
        return 'Rp. ' . strrev(implode('.', str_split(strrev(strval($number)), 3)));
    } else {
        $num = explode('-', $number);
        return 'Rp. - ' . strrev(implode('.', str_split(strrev(strval($num[1])), 3)));
    }
}

function toMysqlDate($date)
{
    $date = explode('/', $date);
    $mysql = $date[2] . '-' . $date[1] . '-' . $date[0];
    return $mysql;
}

function toIndoDate($date)
{
    $date = explode('-', $date);
    $indo = $date[2] . '/' . $date[1] . '/' . $date[0];
    return $indo;
}

function toIndoDatetime($datetime)
{

    $datetime = explode(' ', $datetime);
    $date = $datetime[0];
    $hour = $datetime[1];

    $date_piece = explode('-', $date);

    if ($date_piece[1] == '01') {
        $date_piece[1] = 'Januari';
    } else if ($date_piece[1] == '02') {
        $date_piece[1] = 'Februari';
    } else if ($date_piece[1] == '03') {
        $date_piece[1] = 'Maret';
    } else if ($date_piece[1] == '04') {
        $date_piece[1] = 'April';
    } else if ($date_piece[1] == '05') {
        $date_piece[1] = 'Mei';
    } else if ($date_piece[1] == '06') {
        $date_piece[1] = 'Juni';
    } else if ($date_piece[1] == '07') {
        $date_piece[1] = 'Juli';
    } else if ($date_piece[1] == '08') {
        $date_piece[1] = 'Agustus';
    } else if ($date_piece[1] == '09') {
        $date_piece[1] = 'September';
    } else if ($date_piece[1] == '10') {
        $date_piece[1] = 'Oktober';
    } else if ($date_piece[1] == '11') {
        $date_piece[1] = 'November';
    } else if ($date_piece[1] == '12') {
        $date_piece[1] = 'Desember';
    }

    return $date_piece[2] . ' ' . $date_piece[1] . ' ' . $date_piece[0];
}

function toIndoSlash($datetime)
{

    $datetime = explode(' ', $datetime);
    $date = $datetime[0];
    $hour = $datetime[1];

    $date_piece = explode('-', $date);

    return $date_piece[2] . '/' . $date_piece[1] . '/' . $date_piece[0];

}

function toIndoMonth($month)
{
    if ($month == '01') {
        $month = 'Januari';
    } else if ($month == '02') {
        $month = 'Februari';
    } else if ($month == '03') {
        $month = 'Maret';
    } else if ($month == '04') {
        $month = 'April';
    } else if ($month == '05') {
        $month = 'Mei';
    } else if ($month == '06') {
        $month = 'Juni';
    } else if ($month == '07') {
        $month = 'Juli';
    } else if ($month == '08') {
        $month = 'Agustus';
    } else if ($month == '09') {
        $month = 'September';
    } else if ($month == '10') {
        $month = 'Oktober';
    } else if ($month == '11') {
        $month = 'November';
    } else if ($month == '12') {
        $month = 'Desember';
    }
    return $month;
}

function spelling($nilai) {
    $nilai = abs($nilai);
    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($nilai < 12) {
        $temp = " ". $huruf[$nilai];
    } else if ($nilai <20) {
        $temp = spelling($nilai - 10). " belas";
    } else if ($nilai < 100) {
        $temp = spelling($nilai/10)." puluh". spelling($nilai % 10);
    } else if ($nilai < 200) {
        $temp = " seratus" . spelling($nilai - 100);
    } else if ($nilai < 1000) {
        $temp = spelling($nilai/100) . " ratus" . spelling($nilai % 100);
    } else if ($nilai < 2000) {
        $temp = " seribu" . spelling($nilai - 1000);
    } else if ($nilai < 1000000) {
        $temp = spelling($nilai/1000) . " ribu" . spelling($nilai % 1000);
    } else if ($nilai < 1000000000) {
        $temp = spelling($nilai/1000000) . " juta" . spelling($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
        $temp = spelling($nilai/1000000000) . " milyar" . spelling(fmod($nilai,1000000000));
    } else if ($nilai < 1000000000000000) {
        $temp = spelling($nilai/1000000000000) . " trilyun" . spelling(fmod($nilai,1000000000000));
    }     
    return $temp;
}

function rand_color()
{
    return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
}