<?php defined('BASEPATH') or exit('No direct script access allowed');

function editUnique($value, $params)
{
    $ci = &get_instance();
    list($table, $field, $current_id, $column_id) = explode(".", $params);

    $query = $ci->db->select()->from($table)->where($field, $value)->limit(1)->get()->result_array();

    if ($query && $query[0][$column_id] != $current_id) {
        return false;
    } else {
        return true;
    }
}

function exist($value, $params)
{
    $ci = &get_instance();
    list($table, $field) = explode(".", $params);

    $query = $ci->db->select()->from($table)->where($field, $value)->limit(1)->get();

    if ($query->row()) {
        return true;
    } else {
        return false;
    }
}

function checkPassword($value)
{
    $ci = &get_instance();
    $data = [
        'email' => me()['email'],
        'password' => md5($value),
    ];

    $query = $ci->db->select()->from('users')->where($data)->limit(1)->get();

    if ($query->row()) {
        return true;
    } else {
        return false;
    }
}

function checkPasswordAuth($email, $password)
{
    $ci = &get_instance();
    $data = [
        'email' => $email,
        'password' => $password,
    ];

    $query = $ci->db->select()->from('users')->where($data)->limit(1)->get();

    if ($query->row()) {
        return true;
    } else {
        return false;
    }
}

function checkIndoDate($value)
{
    $date = explode('/', str_replace('_', '', $value));
    $month = $date[1];
    $day = $date[0];
    $year = $date[2];

    if (count($date) == 3 && is_numeric($month) && is_numeric($day) && is_numeric($year)) {
        if (checkdate($month, $day, $year)) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }

}

function checkIndoDate2($value)
{
    $date = explode('/', $value);
    $month = $date[1];
    $day = $date[0];
    $year = $date[2];

    if (count($date) == 3 && is_numeric($month) && is_numeric($day) && is_numeric($year)) {
        if (checkdate($month, $day, $year)) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }

}
