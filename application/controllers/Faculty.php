<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Faculty extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('basic_model', 'bm');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function index()
    {
        $data['title'] = 'Daftar Fakultas';
        $data['view'] = 'admin/faculty/index';
        $data['scripts'] = 'admin/faculty/_scripts';
        $data['menu'] = 'master';
        $data['sub'] =  (me()['sidebar'] == 'nav-md') ? 'faculty' : '';
        $this->load->view('admin/template/app', $data);
    }

    public function facultyTable()
    {
        $datatables = [
            'table' => 'faculties',
            'column' => [
                'faculty_id',
                'faculty_name',
                'created_at',
                'updated_at',
            ],
            'edit_url' => base_url('faculty/edit/'),
            'delete_url' => base_url('faculty/destroy/'),
            'confirm' => [
                'message' => "Apakah anda yakin ingin menghapus fakultas : ", //c_column (Nama, Id Dll)+c
                'c_column' => 'faculty_name', //Pilih salah satu dari table $column
            ],
            'action' => 'basic_action', //Level action
        ];

        showOneTable($datatables);
    }

    public function create()
    {
        $data['view'] = 'admin/faculty/create';
        $data['scripts'] = 'admin/faculty/_scripts';
        $data['title'] = 'Fakultas - Tambah Data';
        $data['menu'] = 'master';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'faculty' : '';
        $this->load->view('admin/template/app', $data);
    }

    public function store()
    {
        if ($this->vm->storeFaculty()) {
            $data = [
                'faculty_name' => ucwords($this->input->post('faculty_name')),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $this->db->insert('faculties', $data);
            response(['message' => "Berhasil menambah fakultas"]);
        } else {
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function edit($id)
    {
        $faculty = $this->bm->getWhere('faculties', ['faculty_id' => $id])->result_array();

        $data['view'] = 'admin/faculty/edit';
        $data['scripts'] = 'admin/faculty/_scripts';
        $data['title'] = 'Fakultas - Edit Data';
        $data['menu'] = 'master';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'faculty' : '';
        $data['faculty'] = $faculty[0];
        $this->load->view('admin/template/app', $data);
    }

    public function update($id)
    {
        if ($this->vm->updateFaculty($id)) {
            $data = [
                'faculty_name' => ucwords($this->input->post('faculty_name')),
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            $this->db->where('faculty_id', $id)->update('faculties', $data);
            $this->session->set_flashdata('notify', [
                'message' => "Berhasil mengubah fakultas",
                'level' => "success",
            ]);
            response();
        } else {
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function destroy($id)
    {
        $this->db->delete('faculties', ['faculty_id' => $id]);
        $this->db->delete('majors', ['faculty_id' => $id]);
        response(['id' => $id]);
    }
}
