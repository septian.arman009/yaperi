<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('basic_model', 'bm');
        auth();
    }

    public function income()
    {
        $data['title'] = 'Pendapatan';
        $data['view'] = 'admin/report/income/index';
        $data['scripts'] = 'admin/report/income/_scripts';
        $data['menu'] = 'report';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'income' : '';
        $data['month'] = date('m');
        $data['year'] = date('Y');
        $this->load->view('admin/template/app', $data);
    }

    public function detailReport($month, $year)
    {
        $thisMonth = $this->bm->thisMonth($month, $year);
        
        if($month - 1 == 0){
            $newMonth = 12;
            $newYear = $year - 1;
        }else{
            $newMonth = $month - 1;
            $newYear = $year;
        }

        $prevMonth = $this->bm->prevMonth($newMonth, $newYear);

        $days = $this->bm->days($month, $year);
        
        $dayNumber = cal_days_in_month(CAL_GREGORIAN,$month,$year);
        $dayIncome = [];
        for ($i=0; $i < $dayNumber; $i++) { 
            $key = array_search($i+1, array_column($days, 'day'));
            if(is_numeric($key)){
                $dayIncome[] = [
                    'day' => 'Tgl: '.($i+1),
                    'income' => $days[$key]['pay']
                ];
            }else{
                $dayIncome[] = [
                    'day' => 'Tgl: '.($i+1),
                    'income' => 0
                ];
            }
        }

        $months = $this->bm->months($year);
        $monthIncome = [];
        for ($i=1; $i <= 12; $i++) { 
            $key = array_search($i, array_column($months, 'month'));
            if(is_numeric($key)){
                $monthIncome[] = [
                    'month' => toIndoMonth($i),
                    'income' => $months[$key]['pay']
                ];
            }else{
                $monthIncome[] = [
                    'month' => toIndoMonth($i),
                    'income' => 0
                ];
            }
        }

        $data['yearIncome'] = $this->bm->yearIncome($year);
    
        $data['day_income'] = $dayIncome;
        $data['month_income'] = $monthIncome;

        $data['thisMonth'] = $thisMonth;
        $data['prevMonth'] = $prevMonth;
        $data['thisName'] = toIndoMonth($month);
        $data['prevName'] = toIndoMonth($newMonth);
        $data['month'] = $month;
        $data['year'] = $year;
        $data['newYear'] = $newYear;

        $this->load->view('admin/report/income/_detail_report', $data);
    }

    public function printMonthly($month, $year)
    {
        $data['month'] = $month;
        $data['year'] = $year;
        $data['payments'] = $this->db->select('a.inv_number, a.bill_amount, a.created_at, a.semester_alias, 
                                                b.student_name, c.generation_name')
                                        ->from('payments as a')
                                        ->join('students as b', 'a.student_id = b.student_id')
                                        ->join('generations as c', 'b.generation_id = c.generation_id')
                                        ->where('month(a.created_at)', $month)
                                        ->where('year(a.created_at)', $year)
                                        ->order_by('a.created_at', 'DESC')
                                        ->get()->result();

        $this->load->view('admin/report/income/print_month', $data);
    }

    public function printYearly($year)
    {
        $data['year'] = $year;
        $months = $this->bm->months($year);
        $monthIncome = [];
        for ($i=1; $i <= 12; $i++) { 
            $key = array_search($i, array_column($months, 'month'));
            if(is_numeric($key)){
                $monthIncome[] = [
                    'month' => toIndoMonth($i),
                    'income' => $months[$key]['pay']
                ];
            }else{
                $monthIncome[] = [
                    'month' => toIndoMonth($i),
                    'income' => 0
                ];
            }
        }

        $data['payments'] = $monthIncome;

        $this->load->view('admin/report/income/print_year', $data);
    }

}