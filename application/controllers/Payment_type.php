<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Payment_type extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('basic_model', 'bm');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function index()
    {
        $data['title'] = 'Jenis Pembayaran';
        $data['view'] = 'admin/type/index';
        $data['scripts'] = 'admin/type/_scripts';
        $data['menu'] = 'master';
        $data['sub'] = me()['sidebar'] == 'nav-md' ? 'type' : '';
        $this->load->view('admin/template/app', $data);
    }

    public function typeTable()
    {
        $query = "SELECT a.type_id, a.type_name, a.created_at, a.updated_at, b.category_name
                    FROM types as a
                    LEFT JOIN categories as b
                    USING(category_id)";

        $datatables = [
            'query' => $query,
            'table' => 'types',
            'column' => [
                'type_id',
                'type_name',
                'category_name',
                'created_at',
                'updated_at',
            ],
            'searchOperator' => 'WHERE',
            'edit_url' => base_url('type/edit/'),
            'delete_url' => base_url('type/destroy/'),
            'confirm' => [
                'message' => "Apakah anda yakin ingin menghapus jenis pembayaran : ", //c_column (Nama, Id Dll)+c
                'c_column' => 'type_name', //Pilih salah satu dari table $column
            ],
            'action' => 'basic_action', //Level action
        ];

        showQueryTable($datatables);
    }

    public function create()
    {
        $data['view'] = 'admin/type/create';
        $data['scripts'] = 'admin/type/_scripts';
        $data['title'] = 'Jenis Pembayaran - Tambah Data';
        $data['menu'] = 'master';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'type' : '';
        $data['categories'] = $this->bm->getAll('categories')->result();
        $this->load->view('admin/template/app', $data);
    }

    public function store()
    {
        if ($this->vm->storeType()) {
            $data = [
                'type_name' => ucfirst($this->input->post('type_name')),
                'category_id' => $this->input->post('category_id'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $this->db->insert('types', $data);
            response(['message' => "Berhasil menambah jenis pembayaran"]);
        } else {
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function edit($id)
    {
        $type = $this->bm->getWhere('types', ['type_id' => $id])->result_array();

        $data['view'] = 'admin/type/edit';
        $data['scripts'] = 'admin/type/_scripts';
        $data['title'] = 'Jenis Pembayaran - Edit Data';
        $data['menu'] = 'master';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'type' : '';
        $data['type'] = $type[0];
        $data['categories'] = $this->bm->getAll('categories')->result();
        $this->load->view('admin/template/app', $data);
    }

    public function update($id)
    {
        if ($this->vm->updateType($id)) {
            $data = [
                'type_name' => ucfirst($this->input->post('type_name')),
                'category_id' => $this->input->post('category_id'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            $this->db->where('type_id', $id)->update('types', $data);
            $this->session->set_flashdata('notify', [
                'message' => "Berhasil mengubah jenis pebayaran",
                'level' => "success",
            ]);
            response();
        } else {
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function destroy($id)
    {
        $this->db->delete('types', ['type_id' => $id]);
        response(['id' => $id]);
    }

}
