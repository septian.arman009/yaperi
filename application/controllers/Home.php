<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('basic_model', 'bm');
        auth();
    }

    public function index()
    {
        $student = $this->bm->getAll('students')->num_rows();
        $major_student = $this->db->select('major_id')->from('students')->group_by('major_id')->get()->num_rows();
        $major = $this->bm->getAll('majors')->num_rows();
        $faculty_major = $this->db->select('faculty_id')->from('majors')->group_by('faculty_id')->get()->num_rows();
        $faculty = $this->bm->getAll('faculties')->num_rows();
        $generation = $this->bm->getAll('generations')->num_rows();
        $user = $this->bm->getAll('users')->num_rows();

        $month = date('m');
        $year = date('Y');

        $today = $this->bm->today();
        $thisMonth = $this->bm->thisMonth($month, $year);
        
        if($month - 1 == 0){
            $newMonth = 12;
            $newYear = $year - 1;
        }else{
            $newMonth = $month - 1;
            $newYear = $year;
        }

        $prevMonth = $this->bm->prevMonth($newMonth, $newYear);

        $data = [
            'view' => 'admin/home/index',
            'scripts' => 'admin/home/_scripts',
            'title' => 'Beranda',
            'menu' => 'home',
            'sub' => '',
            'student' => $student,
            'major_student' => $major_student,
            'major' => $major,
            'faculty_major' => $faculty_major,
            'faculty' => $faculty,
            'generation' => $generation,
            'user' => $user,
            'today' => $today,
            'thisMonth' => $thisMonth,
            'prevMonth' => $prevMonth,
            'thisName' => toIndoMonth($month),
            'prevName' => toIndoMonth($newMonth),
            'year' => $year,
            'newYear' => $newYear
        ];

        $this->load->view('admin/template/app', $data);
    }

    public function monthGraph($month)
    {
        $days = $this->bm->dayHome($month, date('Y'));
        
        $dayNumber = cal_days_in_month(CAL_GREGORIAN,$month,date('Y'));
        $dayIncome = [];
        for ($i=0; $i < $dayNumber; $i++) { 
            $key = array_search($i+1, array_column($days, 'day'));
            if(is_numeric($key)){
                $dayIncome[] = [
                    'day' => 'Tgl: '.($i+1),
                    'income' => $days[$key]['pay']
                ];
            }else{
                $dayIncome[] = [
                    'day' => 'Tgl: '.($i+1),
                    'income' => 0
                ];
            }
        }
        $data['payments'] = $this->bm->paymentHome($month);

        $data['day_income'] = $dayIncome;
        $data['month'] = toIndoMonth($month);
        $this->load->view('admin/home/_month_graph', $data);
    }
}
