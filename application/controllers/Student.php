<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Student extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('basic_model', 'bm');
        $this->load->model('student_model', 'sm');
        $this->load->model('pay_model', 'pm');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function index()
    {
        $data['title'] = 'Daftar Mahasiswa';
        $data['view'] = 'admin/student/index';
        $data['scripts'] = 'admin/student/_scripts';
        $data['menu'] = 'students';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'student' : '';
        $this->load->view('admin/template/app', $data);
    }

    public function studentTable()
    {
        $query = "SELECT student_id, reg_number, student_name, gender, religion,
                         email, mobile, major_name, generation_name, semester_name
                    FROM students
                    LEFT JOIN majors USING(major_id)
                    LEFT JOIN generations USING(generation_id)
                    LEFT JOIN semesters USING(semester_id)";

        $datatables = [
            'query' => $query,
            'column' => [
                'student_id',
                'reg_number',
                'student_name',
                'generation_name',
                'semester_name',
                'major_name',
                'mobile',
            ],
            'searchOperator' => 'HAVING',
            'edit_url' => base_url('student/edit/'),
            'delete_url' => base_url('student/destroy/'),
            'confirm' => [
                'message' => "Apakah anda yakin ingin menghapus student : ", //c_column (Nama, Id Dll)+c
                'c_column' => 'student_name', //Pilih salah satu dari table $column
            ],
            'action' => 'student_action', //Level action
        ];

        showQueryTable($datatables);
    }

    public function create()
    {
        $data['view'] = 'admin/student/create';
        $data['scripts'] = 'admin/student/_scripts';
        $data['title'] = 'Mahasiswa - Tambah Data';
        $data['menu'] = 'students';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'student' : '';
        $data['majors'] = $this->bm->getAll('majors')->result();
        $data['generations'] = $this->bm->getAll('generations')->result();
        $data['semesters'] = $this->bm->getAll('semesters')->result();
        $data['provinces'] = $this->bm->getProvince();
        $this->load->view('admin/template/app', $data);
    }

    public function store()
    {
        if ($this->vm->storeStudent()) {
            $data = [
                'generation_id' => $this->input->post('generation_id'),
                'semester_id' => $this->input->post('semester_id'),
                'semester_alias' => 'Semester-' . $this->input->post('semester_id'),
                'reg_number' => $this->input->post('reg_number'),
                // 'reg_number2' => $this->input->post('reg_number2'),
                'id_card' => $this->input->post('id_card'),
                'npwp' => $this->input->post('npwp'),
                'nisn' => $this->input->post('nisn'),
                'student_name' => ucwords($this->input->post('student_name')),
                'birth_place' => ucwords($this->input->post('birth_place')),
                'birth_date' => toMysqlDate($this->input->post('birth_date')),
                'gender' => $this->input->post('gender'),
                'religion' => $this->input->post('religion'),
                'nationality' => $this->input->post('nationality'),
                'street' => ucwords($this->input->post('street')),
                'rtrw' => $this->input->post('rtrw'),
                'zipcode' => $this->input->post('zipcode'),
                'province_id' => $this->input->post('province_id'),
                'regency_id' => $this->input->post('regency_id'),
                'district_id' => $this->input->post('district_id'),
                'village_id' => $this->input->post('village_id'),
                'phone' => str_replace("_", "", $this->input->post('phone')),
                'mobile' => str_replace("_", "", $this->input->post('mobile')),
                'email' => $this->input->post('email'),
                'major_id' => $this->input->post('major_id'),
                'join_year' => $this->input->post('join_date'),
                'graduate_year' => $this->input->post('graduate_date'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            $student = $this->bm->insertGetId('students', $data);
            $url = base_url('parent/create/' . $student);
            response(['message' => "Berhasil menambah mahasiswa <a href='$url'> KLIK UNTUK TAMBAH DATA ORANG TUA</a>"]);
        } else {
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function edit($id)
    {
        $student = $this->sm->getStudent($id);
        $data['view'] = 'admin/student/edit';
        $data['scripts'] = 'admin/student/_scripts';
        $data['title'] = 'Mahasiswa - Edit Data';
        $data['menu'] = 'students';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'student' : '';
        $data['student'] = $student;
        $data['majors'] = $this->bm->getAll('majors')->result();
        $data['generations'] = $this->bm->getAll('generations')->result();
        $data['semesters'] = $this->bm->getAll('semesters')->result();
        $data['pay'] = $this->bm->getWhere('payments', ['student_id' => $student->student_id])->row();
        $data['provinces'] = $this->bm->getProvince();
        $data['regencies'] = $this->bm->getCity($student->province_id);
        $data['districts'] = $this->bm->getSubDistrict($student->regency_id);
        $data['villages'] = $this->bm->getVillageOffice($student->district_id);
        $this->load->view('admin/template/app', $data);
    }

    public function update($id)
    {
        if ($this->vm->updateStudent($id)) {
            $data = [
                'generation_id' => $this->input->post('generation_id'),
                'semester_id' => $this->input->post('semester_id'),
                'semester_alias' => 'Semester-' . $this->input->post('semester_id'),
                'reg_number' => strtoupper($this->input->post('reg_number')),
                // 'reg_number2' => $this->input->post('reg_number2'),
                'id_card' => $this->input->post('id_card'),
                'npwp' => $this->input->post('npwp'),
                'nisn' => $this->input->post('nisn'),
                'student_name' => ucwords($this->input->post('student_name')),
                'birth_place' => ucwords($this->input->post('birth_place')),
                'birth_date' => toMysqlDate($this->input->post('birth_date')),
                'gender' => $this->input->post('gender'),
                'religion' => $this->input->post('religion'),
                'nationality' => $this->input->post('nationality'),
                'street' => ucwords($this->input->post('street')),
                'rtrw' => $this->input->post('rtrw'),
                'zipcode' => $this->input->post('zipcode'),
                'province_id' => $this->input->post('province_id'),
                'regency_id' => $this->input->post('regency_id'),
                'district_id' => $this->input->post('district_id'),
                'village_id' => $this->input->post('village_id'),
                'phone' => str_replace("_", "", $this->input->post('phone')),
                'mobile' => str_replace("_", "", $this->input->post('mobile')),
                'email' => $this->input->post('email'),
                'major_id' => $this->input->post('major_id'),
                'join_year' => $this->input->post('join_date'),
                'graduate_year' => $this->input->post('graduate_date'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            $this->db->where('student_id', $id)->update('students', $data);
            $this->session->set_flashdata('notify', [
                'message' => "Berhasil mengubah mahasiswa",
                'level' => "success",
            ]);
            response();
        } else {
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function destroy($id)
    {
        $this->db->delete('students', ['student_id' => $id]);
        $this->db->delete('parents', ['student_id' => $id]);
        $this->db->delete('payments', ['student_id' => $id]);
        $this->db->delete('pays', ['student_id' => $id]);
        $this->db->delete('leaves', ['student_id' => $id]);
        response(['id' => $id]);
    }

    public function getCity($provinceId)
    {
        $city = $this->bm->getCity($provinceId);
        $html = "<option value=''>- Pilih Kota -</option>";
        foreach ($city as $ct) {
            $html .= "<option value='$ct->id'>" . $ct->name . "</option>";
        }
        echo $html;
    }

    public function getSubDistrict($cityId)
    {
        $districts = $this->bm->getSubDistrict($cityId);
        $html = "<option value=''>- Pilih Kecamatan -</option>";
        foreach ($districts as $ds) {
            $html .= "<option value='$ds->id'>" . $ds->name . "</option>";
        }
        echo $html;
    }

    public function getVillageOffice($districtId)
    {
        $villages = $this->bm->getVillageOffice($districtId);
        $html = "<option value=''>- Pilih Kelurahan -</option>";
        foreach ($villages as $vill) {
            $html .= "<option value='$vill->id'>" . $vill->name . "</option>";
        }
        echo $html;
    }

    public function paymentDetail($studentId)
    {
        $data['title'] = 'Riwayat Pembayaran';
        $data['view'] = 'admin/student/payment_detail';
        $data['scripts'] = 'admin/student/__scripts';
        $data['menu'] = 'students';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'student' : '';
        $data['_modal'] = 'admin/template/_modal';

        $student = $this->pm->getDetailStudent($studentId); //@List student
        $generationId = $student->generation_id;
        $leaves = $this->pm->getLeaves($studentId, $generationId); //@List cuti
        $prices = $this->pm->getPriceList($generationId); //@List harga
        $paids = $this->pm->getOthersPay($studentId); //@Pembayaran ditransaksi lain

        //@Sisa pembayaran
        foreach ($paids as $paid) {
            $pays[$paid->price_id] = $paid->pay;
        }

        $semesters = $this->bm->getAll('semesters')->result_array(); //@List semester

        $semsterList = array();
        $billing = 0;
        $dataLength = count($semesters) + count($leaves);
        $smCount = 0;
        for ($i = 1; $i <= $dataLength; $i++) {
            $priceList = array();
            $total = 0;
            $totalGeneration = 0;

            $leave = array_search($i, array_column($leaves, 'semester_id'));
            if (is_numeric($leave)) {
                $smCount++;
                $semesterList[$i] = [
                    'leave' => 'yes',
                    'leave_id' => $leaves[$leave]['leave_id'],
                    'price' => toRp($leaves[$leave]['price']),
                    'created_at' => toIndoDatetime($leaves[$leave]['created_at']),
                    'semesterName' => 'Semester-' . $i,
                ];
            } else {

                foreach ($prices as $price) {
                    if ($price->semester_id == $semesters[($i - $smCount) - 1]['semester_id']) {
                        $pay = (!empty($pays[$price->price_id])) ? $pays[$price->price_id] : 0;
                        //@List jenis pembayaran persemester
                        $priceList[] = [
                            'price_id' => $price->price_id,
                            'payment_type' => $price->type_name,
                            'price' => $price->price,
                            'installment' => $price->installment,
                            'pay_left' => $price->price - $pay,
                            'paid' => $pay,
                        ];
                        $total += ($price->price) - $pay;
                        $totalGeneration += $price->price;
                    }
                }

                $semesterList[$i] = [
                    'leave' => 'no',
                    'semesterId' => $semesters[($i - $smCount) - 1]['semester_id'],
                    'semesterName' => 'Semester-' . $i,
                    'priceList' => $priceList,
                    'total' => $total,
                ];
            }

            $billing += $totalGeneration;

        }

        $data['billing'] = $billing;
        $data['generationName'] = $this->bm->getWhere('generations', ['generation_id' => $generationId])->row()->generation_name;
        $data['semesters'] = $semesterList;

        //@Sisa pembayaran
        $paidLeave = $this->bm->getWhere('pay_leaves', ['generation_id' => $generationId])->row();
        if ($paidLeave) {
            $data['paidLeave'] = $paidLeave->price;
        } else {
            $data['paidLeave'] = 0;
        }

        $data['payLists'] = $this->pm->getPayList($studentId); //@List pembayaran

        $semesterPrice = $this->pm->getSemesterPrice($generationId); //@List harga persemester (Grafik donuts)
        $semesterPay = $this->pm->getSemesterPay($studentId); //@List pembayaran masuk persemester (Grafik donust)

        //@Logika grafik donuts
        $total = 0;
        $smCount2 = 0;
        $color = array();
        $donut = array();
        for ($i = 1; $i <= $dataLength; $i++) {
            $leave = array_search($i, array_column($leaves, 'semester_id'));
            if (is_numeric($leave)) {
                $smCount2++;
                $donut[] = [
                    'label' => 'Semester-' . $i . '(Cuti)',
                    'value' => $leaves[$leave]['price'],
                ];
                $color[] = rand_color();
            } else {
                if (!empty($semesterPrice[($i - $smCount2) - 1])) {
                    $key = array_search($semesterPrice[($i - $smCount2) - 1]['semester_name'], array_column($semesterPay, 'semester_name'));
                    if (is_numeric($key)) {
                        $percent = ($semesterPay[$key]['pay'] / $semesterPrice[($i - $smCount2) - 1]['price']) * 100;
                        $label = 'Semester-' . $i . '(' . round($percent) . ' %)';
                    } else {
                        $label = 'Semester-' . $i;
                    }
                    $donut[] = [
                        'label' => $label,
                        'value' => $semesterPrice[($i - $smCount2) - 1]['price'],
                    ];
                    $color[] = rand_color();
                    $total += $semesterPrice[($i - $smCount2) - 1]['price'];
                }
            }
        }

        $data['studentId'] = $studentId;
        $data['student'] = $student;
        $data['semesterPrice'] = $donut;
        $data['color'] = $color;
        $data['length'] = $dataLength;
        $data['year'] = date('Y');
        $this->load->view('admin/template/app', $data);
    }

    public function printPaymentSemester($studentId, $semesterId, $semesterName)
    {
        $generation = $this->sm->getStudent($studentId);
        $generationId = $generation->generation_id;
        $prices = $this->sm->getPriceList($generationId, $semesterId);
        $pays = $this->sm->getPaidList($generationId, $semesterId, $studentId);
        
        $payments = array();
        foreach ($prices as $price) {
            $key = array_search($price->price_id, array_column($pays, 'price_id'));

            if(is_numeric($key)){
                $pay = $pays[$key]['pay'];
            }else{
                $pay = 0;
            }

            if($price->installment > 0){
                $type_name = $price->type_name."(Bulan #$price->installment)";
            }else{
                $type_name = $price->type_name;
            }

            $payments[] = [
                'type_name' => $type_name,
                'bill' => $price->price,
                'pay' => $pay

            ];
        }

        $data['payments'] = $payments;
        $data['generationName'] = $generation->generation_name;
        $data['semesterName'] = $semesterName;
        $data['student'] = $generation;
        $this->load->view('admin/student/print_semester', $data);
    }

    public function studentDetailModal($studentId)
    {
        $data['student'] = $this->pm->getDetailStudent($studentId);
        $this->load->view('admin/student/_student_modal', $data);
    }

    public function printAll($studentId)
    {
        $student = $this->pm->getDetailStudent($studentId); //@List student
        $generationId = $student->generation_id;
        $leaves = $this->pm->getLeaves($studentId, $generationId); //@List cuti
        $prices = $this->pm->getPriceList($generationId); //@List harga
        $paids = $this->pm->getOthersPay($studentId); //@Pembayaran ditransaksi lain

        //@Sisa pembayaran
        foreach ($paids as $paid) {
            $pays[$paid->price_id] = $paid->pay;
        }

        $semesters = $this->bm->getAll('semesters')->result_array(); //@List semester

        $semsterList = array();
        $billing = 0;
        $total_pay = 0;
        $dataLength = count($semesters) + count($leaves);
        $smCount = 0;
        for ($i = 1; $i <= $dataLength; $i++) {
            $priceList = array();
            $total = 0;
            $totalGeneration = 0;

            $leave = array_search($i, array_column($leaves, 'semester_id'));
            if (is_numeric($leave)) {
                $smCount++;
                $semesterList[$i] = [
                    'leave' => 'yes',
                    'leave_id' => $leaves[$leave]['leave_id'],
                    'price' => toRp($leaves[$leave]['price']),
                    'created_at' => toIndoDatetime($leaves[$leave]['created_at']),
                    'semesterName' => 'Semester-' . $i,
                ];
            } else {

                foreach ($prices as $price) {
                    if ($price->semester_id == $semesters[($i - $smCount) - 1]['semester_id']) {
                        $pay = (!empty($pays[$price->price_id])) ? $pays[$price->price_id] : 0;
                        //@List jenis pembayaran persemester
                        $priceList[] = [
                            'price_id' => $price->price_id,
                            'payment_type' => $price->type_name,
                            'price' => $price->price,
                            'installment' => $price->installment,
                            'pay_left' => $price->price - $pay,
                            'paid' => $pay,
                        ];
                        $total += $pay;
                        $totalGeneration += $price->price;
                    }
                }

                $semesterList[$i] = [
                    'leave' => 'no',
                    'semesterId' => $semesters[($i - $smCount) - 1]['semester_id'],
                    'semesterName' => 'Semester-' . $i,
                    'priceList' => $priceList,
                    'total' => $total,
                ];
            }
            $total_pay += $total;
            $billing += $totalGeneration;

        }

        $data['billing'] = $billing;
        $data['pay'] = $total_pay;
        $data['generationName'] = $this->bm->getWhere('generations', ['generation_id' => $generationId])->row()->generation_name;
        $data['semesters'] = $semesterList;
        $data['student'] = $student;

        $this->load->view('admin/student/print_all', $data);
    }

}
