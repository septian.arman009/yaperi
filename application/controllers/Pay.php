<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pay extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('basic_model', 'bm');
        $this->load->model('pay_model', 'pm');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function index()
    {
        $data['title'] = 'Bayar';
        $data['view'] = 'admin/payment/pay/index';
        $data['scripts'] = 'admin/payment/pay/_scripts';
        $data['menu'] = 'payment';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'pay' : '';
        $data['_modal'] = 'admin/template/_modal';
        $data['studentId'] = null;
        $data['semesterPrice'] = [];
        $data['monthData'] = [];
        $data['color'] = [];
        $this->load->view('admin/template/app', $data);
    }

    public function search()
    {
        $search = $this->input->post('search');
        if ($search) {
            $data['students'] = $this->pm->getStudents($search);
        } else {
            $data['students'] = [];
        }

        $this->load->view('admin/payment/pay/_search_result', $data);
    }

    public function formPayment($studentId)
    {
        $data['title'] = 'Form Pembayaran';
        $data['view'] = 'admin/payment/pay/form_payment';
        $data['scripts'] = 'admin/payment/pay/_scripts';
        $data['menu'] = 'payment';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'pay' : '';
        $data['_modal'] = 'admin/template/_modal';

        $student = $this->pm->getDetailStudent($studentId); //@List student
        $generationId = $student->generation_id;
        $leaves = $this->pm->getLeaves($studentId, $generationId); //@List cuti
        $prices = $this->pm->getPriceList($generationId); //@List harga
        $paids = $this->pm->getOthersPay($studentId); //@Pembayaran ditransaksi lain
        
        //@Sisa pembayaran
        foreach ($paids as $paid) {
            $pays[$paid->price_id] = $paid->pay;
        }

        $semesters = $this->bm->getAll('semesters')->result_array(); //@List semester

        $semsterList = array();
        $billing = 0;
        $dataLength = count($semesters) + count($leaves);
        $smCount = 0;
        for ($i = 1; $i <= $dataLength; $i++) {
            $priceList = array();
            $total = 0;
            $totalGeneration = 0;

            $leave = array_search($i, array_column($leaves, 'semester_id'));
            if (is_numeric($leave)) {
                $smCount++;
                $semesterList[$i] = [
                    'leave' => 'yes',
                    'leave_id' => $leaves[$leave]['leave_id'],
                    'price' => toRp($leaves[$leave]['price']),
                    'created_at' => toIndoDatetime($leaves[$leave]['created_at']),
                    'semesterName' => 'Semester-' . $i,
                ];
            } else {

                foreach ($prices as $price) {
                    if ($price->semester_id == $semesters[($i - $smCount) - 1]['semester_id']) {
                        $pay = (!empty($pays[$price->price_id])) ? $pays[$price->price_id] : 0;
                        //@List jenis pembayaran persemester
                        $priceList[] = [
                            'price_id' => $price->price_id,
                            'payment_type' => $price->type_name,
                            'price' => $price->price,
                            'installment' => $price->installment,
                            'pay_left' => $price->price - $pay,
                            'paid' => $pay,
                        ];
                        $total += ($price->price) - $pay;
                        $totalGeneration += $price->price;
                    }
                }

                $semesterList[$i] = [
                    'leave' => 'no',
                    'semesterId' => $semesters[($i - $smCount) - 1]['semester_id'],
                    'semesterName' => 'Semester-' . $i,
                    'priceList' => $priceList,
                    'total' => $total,
                ];
            }

            $billing += $totalGeneration;

        }

        $data['billing'] = $billing;
        $data['generationName'] = $this->bm->getWhere('generations', ['generation_id' => $generationId])->row()->generation_name;
        $data['semesters'] = $semesterList;

        //@Sisa pembayaran
        $paidLeave = $this->bm->getWhere('pay_leaves', ['generation_id' => $generationId])->row();
        if ($paidLeave) {
            $data['paidLeave'] = $paidLeave->price;
        } else {
            $data['paidLeave'] = 0;
        }

        $data['payLists'] = $this->pm->getPayList($studentId); //@List pembayaran
        
        $semesterPrice = $this->pm->getSemesterPrice($generationId); //@List harga persemester (Grafik donuts)
        $semesterPay = $this->pm->getSemesterPay($studentId); //@List pembayaran masuk persemester (Grafik donust)

        //@Logika grafik donuts
        $total = 0;
        $smCount2 = 0;
        $color = array();
        $donut = array();
        for ($i = 1; $i <= $dataLength; $i++) {
            $leave = array_search($i, array_column($leaves, 'semester_id'));
            if (is_numeric($leave)) {
                $smCount2++;
                $donut[] = [
                    'label' => 'Semester-' . $i . '(Cuti)',
                    'value' => $leaves[$leave]['price'],
                ];
                $color[] = rand_color();
            } else {
                if (!empty($semesterPrice[($i - $smCount2) - 1])) {
                    $key = array_search($semesterPrice[($i - $smCount2) - 1]['semester_name'], array_column($semesterPay, 'semester_name'));
                    if (is_numeric($key)) {
                        $percent = ($semesterPay[$key]['pay'] / $semesterPrice[($i - $smCount2) - 1]['price']) * 100;
                        $label = 'Semester-' . $i . '(' . round($percent) . ' %)';
                    } else {
                        $label = 'Semester-' . $i;
                    }
                    $donut[] = [
                        'label' => $label,
                        'value' => $semesterPrice[($i - $smCount2) - 1]['price'],
                    ];
                    $color[] = rand_color();
                    $total += $semesterPrice[($i - $smCount2) - 1]['price'];
                }
            }
        }

        $data['studentId'] = $studentId;
        $data['student'] = $student;
        $data['semesterPrice'] = $donut;
        $data['color'] = $color;
        $data['length'] = $dataLength;
        $data['year'] = date('Y');
        $this->load->view('admin/template/app', $data);
    }

    public function paymentGraph($studentId, $year)
    {
        $monthPay = $this->pm->monthPay($studentId, $year); //@Data grafik pembayaran masuk bulanan
        $leavePay = $this->pm->leavePay($studentId, $year); //@Data grafik pembayaran cuti masuk bulanan
        for ($i = 1; $i <= 12; $i++) {
            $monthPaid = array_search($i, array_column($monthPay, 'month'));
            $leavePaid = array_search($i, array_column($leavePay, 'month'));
            $leavePrice = (is_numeric($leavePaid)) ? $leavePay[$leavePaid]['pay'] : 0;
            if (is_numeric($monthPaid)) {
                $monthData[] = [
                    'month' => toIndoMonth($i),
                    'pay' => $monthPay[$monthPaid]['pay'] + $leavePrice,
                ];
            } else {
                $monthData[] = [
                    'month' => toIndoMonth($i),
                    'pay' => 0 + $leavePrice,
                ];
            }
        }
        $paymentGraph = $monthData;
        $this->load->view('admin/payment/pay/_payment_graph', compact('paymentGraph'));
    }

    public function payList($studentId)
    {
        $query = "SELECT a.payment_id, a.inv_number, a.semester_alias, b.student_name, a.description,
                    CONCAT('Rp. ', fRupiah(a.bill_amount)) AS pay,
                    DATE_FORMAT(a.created_at, '%d/%m/%Y %H:%i:%s') AS created_at,
                    c.generation_name
                        FROM payments as a
                        JOIN students as b USING(student_id)
                        JOIN generations as c USING(generation_id)
                        WHERE b.student_id = $studentId
                        GROUP BY a.inv_number";

        $datatables = [
            'query' => $query,
            'column' => [
                'payment_id',
                'inv_number',
                'generation_name',
                'semester_alias',
                'student_name',
                'pay',
                'description',
                'created_at',
            ],
            'searchOperator' => 'HAVING',
            'edit_url' => base_url('transaction/edit/'),
            'delete_url' => base_url('pay/pay_list/destroy/'),
            'confirm' => [
                'message' => "Apakah anda yakin ingin menghapus transaksi : ", //c_column (Nama, Id Dll)+c
                'c_column' => 'inv_number', //Pilih salah satu dari table $column
            ],
            'action' => 'transaction_action', //Level action
        ];

        showQueryTable($datatables);
    }

    public function storePay($studentId)
    {
        $pay = $this->input->post('pay');
        $payLeft = $this->input->post('pay_left');
        $priceId = $this->input->post('price_id');
        $semesterId = $this->input->post('semester_id');
        $semesterAlias = $this->input->post('semester_alias');
        $type = $this->input->post('type');
        $studentPay = moneyClean($this->input->post('student_pay'));
        $bill = moneyClean($this->input->post('bill'));
        $moneyBack = moneyClean($this->input->post('money_back'));
        $errors = array();

        if ($this->vm->storePay()) {
            if ($studentPay <= 0 || $studentPay < $bill) {
                $errors["student_pay"] = "Pembayaran harus lebih besar atau sama dengan tagihan";
                response(['errors' => $errors]);
            } else if ($studentId > 0 && $studentPay >= $bill) {
                if ($_FILES['photo']['name'] != "") {
                    $upload = $this->upload($_FILES);
                }

                $data = array();
                for ($i = 0; $i < count($pay); $i++) {
                    $amount = moneyClean($pay[$i]);
                    if ($amount > $payLeft[$i]) {
                        $errors["pay" . $priceId[$i]] = "Pembayaran $type[$i] melebihi tagihan/kekurangan sebesar " . toRp($payLeft[$i]);
                    } else if ($amount == 0) {
                        $errors["pay" . $priceId[$i]] = "Pembayaran $type[$i] harus lebih besar dari " . toRp(0);
                    } else {
                        $data[] = [
                            'payment_id' => 0,
                            'student_id' => $studentId,
                            'pay' => moneyClean($amount),
                            'price_id' => $priceId[$i],
                            'created_at' => toMysqlDate($this->input->post('created_at')) . ' ' . date('H:i:s'),
                            'updated_at' => toMysqlDate($this->input->post('created_at')) . ' ' . date('H:i:s'),
                        ];

                    }
                }

                if (empty($errors) && empty($upload['errors'])) {
                    $t_list = "";
                    foreach ($type as $t) {
                        if($t_list == ""){
                            $t_list = $t;
                        }else{
                            $t_list = $t_list.', '.$t;
                        }
                    }

                    $inv = $this->generateInv();
                    $payment = $this->bm->insertGetId('payments', [
                        'inv_number' => $inv,
                        'student_id' => $studentId,
                        'semester_id' => $semesterId,
                        'semester_alias' => $semesterAlias,
                        'pay_amount' => $studentPay,
                        'bill_amount' => $bill,
                        'money_back' => $moneyBack,
                        'photo' => (!empty($upload['file_name'])) ? $upload['file_name'] : null,
                        'description' => $t_list,
                        'created_at' => toMysqlDate($this->input->post('created_at')) . ' ' . date('H:i:s'),
                        'updated_at' => toMysqlDate($this->input->post('created_at')) . ' ' . date('H:i:s'),
                    ]);

                    for ($i = 0; $i < count($pay); $i++) {
                        $data[$i]['payment_id'] = $payment;
                    }

                    $this->db->insert_batch('pays', $data);

                    $ss_id = $this->bm->getWhere('students', ['student_id' => $studentId])->row()->semester_id;
                    if ($ss_id < $semesterId) {
                        $this->db->where('student_id', $studentId)->update('students', ['semester_id' => $semesterId, 'semester_alias' => $semesterAlias]);
                    }

                    $this->session->set_flashdata('notify', [
                        'message' => "Pembayaran berhasil",
                        'level' => "success",
                    ]);
                    response();
                } else {
                    if (!empty($upload['errors'])) {
                        if (empty($errors)) {
                            response(['errors' => $upload['errors']]);
                        } else {
                            array_merge($errors, $upload['errors']);
                            response(['errors' => $errors]);
                        }
                    } else {
                        response(['errors' => $errors]);
                    }
                }

            }
        } else {
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }

    }

    public function generateInv()
    {
        $lastInv = $this->bm->lastInv();
        if(!$lastInv){
            $number = 1;
        }else{
            $inv = explode('/', $lastInv);
            $number = $inv[2]+1;
        }

        return $inv = 'INV/' . date('m') . '' . substr(date('Y'), 2, 2) . '/' . $number;
    }

    public function generateInvLeave()
    {
        $lastId = $this->bm->lastId('leaves');
        return $inv = 'INV/CT/' . date('m') . '' . substr(date('Y'), 2, 2) . '/' . $lastId;
    }

    public function upload($file)
    {
        $config = array();
        $photo = $file['photo']['name'];
        $file_ext = pathinfo($photo, PATHINFO_EXTENSION);
        $new_name = md5(uniqid(rand(), true)) . '.' . $file_ext;
        $config['upload_path'] = "./assets/admin/payments";
        $config['allowed_types'] = "jpg|jpeg|JPG|JPEG|png";
        $config['max_size'] = 1024;
        $config['file_name'] = $new_name;
        $this->load->library('upload', $config, 'file_upload');
        $this->file_upload->initialize($config);

        if ($this->file_upload->do_upload('photo')) {
            $this->file_upload->data();
            return array('file_name' => $new_name);
        } else {
            return array('errors' => ['photo' => $this->file_upload->display_errors()]);
        }
    }

    public function destroy($id)
    {
        $photo = $this->bm->getWhere('payments', ['payment_id' => $id])->row()->photo;
        $this->db->delete('payments', ['payment_id' => $id]);
        $this->db->delete('pays', ['payment_id' => $id]);

        if (!is_null($photo)) {
            if (file_exists("./assets/admin/payments/" . $photo)) {
                unlink("./assets/admin/payments/" . $photo);
            }
        }

        $this->session->set_flashdata('notify', [
            'message' => "Berhasil menghapus transaksi",
            'level' => "danger",
        ]);
        response();

    }

    public function payLeaves($studentId, $generationId, $length)
    {
        //----List semester di pembayaran cuti selesaikan besok kamis
        $data['leaves'] = $this->bm->getWhere('pay_leaves', ['generation_id' => $generationId])->row();
        $data['studentId'] = $studentId;
        $data['generationId'] = $generationId;

        $semesters = $this->bm->getAll('semesters')->result_array();
        $leaves = $this->pm->getLeaves($studentId, $generationId); //@List cuti

        $smCount = 0;
        for ($i = 1; $i <= $length; $i++) {
            $leave = array_search($i, array_column($leaves, 'semester_id'));
            if (is_numeric($leave)) {
                $smCount++;
                $semesterList[$i] = [
                    'leave' => 'yes',
                ];
            } else {
                $semesterList[$i] = [
                    'leave' => 'no',
                    'semesterId' => $i,
                    'semesterName' => 'Semester-' . $i,
                ];
            }
        }
        $data['semesters'] = $semesterList;
        $this->load->view('admin/payment/pay/pay_leaves', $data);
    }

    public function storeLeaves($studentId, $generationId)
    {
        $semesterId = $this->input->post('semester_id');

        $price = moneyClean($this->input->post('price_leave'));
        if ($price >= 0) {
            $data = [
                'inv_number' => $this->generateInvLeave(),
                'student_id' => $studentId,
                'generation_id' => $generationId,
                'semester_id' => $semesterId,
                'price' => $price,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            $this->db->insert('leaves', $data);
            $this->session->set_flashdata('notify', [
                'message' => "Pembayaran cuti Semester-$semesterId berhasil",
                'level' => "success",
            ]);

            $ss_id = $this->bm->getWhere('students', ['student_id' => $studentId])->row()->semester_id;
            if ($ss_id <= $semesterId) {
                $this->db->where('student_id', $studentId)->update('students', ['semester_id' => $semesterId + 1]);
            }

            response();
        } else {
            response(['errors' => ['leaveprice' => '* Harga cuti belum diatur']]);
        }
    }

    // public function repairDate(){
    //     $payments = $this->db->get('payments')->result();
    //     $data = array();
    //     foreach ($payments as $payment){
    //         $oldInv = explode('/', $payment->inv_number);
    //         $newInv = $oldInv[0].'/'.$oldInv[1].'/'.$payment->payment_id;
    //         array_push($data, [
    //             'payment_id' => $payment->payment_id,
    //             'inv_number' => $newInv
    //         ]);
    //     }
    //     // dump($data);
    //     $this->db->update_batch('payments', $data, 'payment_id');
    // }

}
