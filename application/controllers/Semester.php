<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Semester extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('basic_model', 'bm');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function index()
    {
        $data['title'] = 'Semester';
        $data['view'] = 'admin/semester/index';
        $data['scripts'] = 'admin/semester/_scripts';
        $data['menu'] = 'master';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'semester' : '';
        $this->load->view('admin/template/app', $data);
    }

    public function semesterTable()
    {
        $datatables = [
            'table' => 'semesters',
            'column' => [
                'semester_id',
                'semester_name',
                'created_at',
                'updated_at',
            ],
            'edit_url' => null,
            'delete_url' => null,
            // 'edit_url' => base_url('semester/edit/'),
            // 'delete_url' => base_url('semester/destroy/'),
            'confirm' => [
                'message' => "Apakah anda yakin ingin menghapus semester : ", //c_column (Nama, Id Dll)+c
                'c_column' => 'semester_name', //Pilih salah satu dari table $column
            ],
            'action' => '', //Level action
        ];

        showOneTable($datatables);
    }
}
