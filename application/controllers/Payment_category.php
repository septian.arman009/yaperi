<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Payment_category extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('basic_model', 'bm');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function index()
    {
        $data['title'] = 'Kategori Pembayaran';
        $data['view'] = 'admin/category/index';
        $data['scripts'] = 'admin/category/_scripts';
        $data['menu'] = 'master';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'category' : '';
        $this->load->view('admin/template/app', $data);
    }

    public function categoryTable()
    {
        $datatables = [
            'table' => 'categories',
            'column' => [
                'category_id',
                'category_name',
                'created_at',
                'updated_at',
            ],
            'edit_url' => base_url('category/edit/'),
            'delete_url' => base_url('category/destroy/'),
            'confirm' => [
                'message' => "Apakah anda yakin ingin menghapus kategori : ", //c_column (Nama, Id Dll)+c
                'c_column' => 'category_name', //Pilih salah satu dari table $column
            ],
            'action' => 'basic_action', //Level action
        ];

        showOneTable($datatables);
    }

    public function create()
    {
        $data['view'] = 'admin/category/create';
        $data['scripts'] = 'admin/category/_scripts';
        $data['title'] = 'Kategori - Tambah Data';
        $data['menu'] = 'master';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'category' : '';
        $this->load->view('admin/template/app', $data);
    }

    public function store()
    {
        if ($this->vm->storeCategory()) {
            $data = [
                'category_name' => ucfirst($this->input->post('category_name')),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $this->db->insert('categories', $data);
            response(['message' => "Berhasil menambah kategori pembayaran"]);
        } else {
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function edit($id)
    {
        $category = $this->bm->getWhere('categories', ['category_id' => $id])->result_array();

        $data['view'] = 'admin/category/edit';
        $data['scripts'] = 'admin/category/_scripts';
        $data['title'] = 'Kategori - Edit Data';
        $data['menu'] = 'master';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'category' : '';
        $data['category'] = $category[0];
        $this->load->view('admin/template/app', $data);
    }

    public function update($id)
    {
        if ($this->vm->updateCategory($id)) {
            $data = [
                'category_name' => ucfirst($this->input->post('category_name')),
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            $this->db->where('category_id', $id)->update('categories', $data);
            $this->session->set_flashdata('notify', [
                'message' => "Berhasil mengubah kategori pebayaran",
                'level' => "success",
            ]);
            reponse();
        } else {
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function destroy($id)
    {
        $this->db->delete('categories', ['category_id' => $id]);
        response(['id' => $id]);
    }

}
