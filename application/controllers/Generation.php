<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Generation extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('basic_model', 'bm');
        $this->load->model('generation_model', 'gm');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function index()
    {
        $data['title'] = 'Angkatan';
        $data['view'] = 'admin/generation/index';
        $data['scripts'] = 'admin/generation/_scripts';
        $data['menu'] = 'master';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'generation' : '';
        $this->load->view('admin/template/app', $data);
    }

    public function generationTable()
    {
        $datatables = [
            'table' => 'generations',
            'column' => [
                'generation_id',
                'generation_name',
                'year',
                'created_at',
                'updated_at',
            ],
            'edit_url' => base_url('generation/edit/'),
            'delete_url' => base_url('generation/destroy/'),
            'confirm' => [
                'message' => "Apakah anda yakin ingin menghapus angkatan : ", //c_column (Nama, Id Dll)+c
                'c_column' => 'generation_name', //Pilih salah satu dari table $column
            ],
            'action' => 'basic_action', //Level action
        ];

        showOneTable($datatables);
    }

    public function create()
    {
        $data['view'] = 'admin/generation/create';
        $data['scripts'] = 'admin/generation/_scripts';
        $data['title'] = 'Angkatan - Tambah Data';
        $data['menu'] = 'master';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'generation' : '';

        $generations = $this->bm->getAll('generations')->result();
        $genNumber = array();
        $year = array();
        foreach ($generations as $gen) {
            $splitName = explode('-', $gen->generation_name);
            $genNumber[] = $splitName[1];
            $year[] = $gen->year;
        }
        $data['genNumber'] = $genNumber;
        $data['year'] = $year;
        $this->load->view('admin/template/app', $data);
    }

    public function store()
    {
        $data = [
            'generation_name' => 'Angkatan-' . $this->input->post('generation_name'),
            'year' => $this->input->post('year'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        $this->db->insert('generations', $data);
        response(['message' => [
            'name' => $this->input->post('generation_name'),
            'year' => $this->input->post('year'),
        ]]);
    }

    public function edit($id)
    {
        $generation = $this->bm->getWhere('generations', ['generation_id' => $id])->row();
        $data['view'] = 'admin/generation/edit';
        $data['scripts'] = 'admin/generation/_scripts';
        $data['title'] = 'Angkatan - Edit Data';
        $data['menu'] = 'master';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'generation' : '';
        $data['generation'] = $generation;

        $generations = $this->gm->notInYear($generation->year);

        $year = array();
        foreach ($generations as $gen) {
            $year[] = $gen->year;
        }
        $data['year'] = $year;
        $this->load->view('admin/template/app', $data);
    }

    public function update($id)
    {
        $this->db->where('generation_id', $id)->update('generations', [
            'year' => $this->input->post('year'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        $this->session->set_flashdata('notify', [
            'message' => "Berhasil mengubah angkatan",
            'level' => "success",
        ]);
        response();
    }

    public function destroy($id)
    {
        $student = $this->bm->getWhere('students', ['generation_id' => $id])->row();

        if(!$student){
            $this->db->delete('generations', ['generation_id' => $id]);
            $this->db->delete('prices', ['generation_id' => $id]);
            response(['id' => $id]);
        }else{
            response(['errors' => 'Angkatan ini memiliki mahasiswa, tidak dapat dihapus']);
        }
    }

}
