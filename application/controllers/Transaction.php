<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('basic_model', 'bm');
        $this->load->model('transaction_model', 'tm');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function index()
    {
        $data['title'] = 'Daftar Transaksi';
        $data['view'] = 'admin/payment/transaction/index';
        $data['scripts'] = 'admin/payment/transaction/_scripts';
        $data['menu'] = 'payment';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'transaction' : '';
        $this->load->view('admin/template/app', $data);
    }

    public function transactionTable()
    {
        $query = "SELECT a.payment_id, a.inv_number, a.semester_alias, b.student_name, a.description,
                    CONCAT('Rp. ', fRupiah(a.bill_amount)) AS pay,
                    DATE_FORMAT(a.created_at, '%d/%m/%Y %H:%i:%s') AS created_at,
                    c.generation_name
                        FROM payments as a
                        JOIN students as b USING(student_id)
                        JOIN generations as c USING(generation_id)
                        GROUP BY a.inv_number";

        $datatables = [
            'query' => $query,
            'column' => [
                'payment_id',
                'inv_number',
                'generation_name',
                'semester_alias',
                'student_name',
                'pay',
                'description',
                'created_at',
            ],
            'searchOperator' => 'HAVING',
            'edit_url' => base_url('transaction/edit/'),
            'delete_url' => base_url('transaction/destroy/'),
            'confirm' => [
                'message' => "Apakah anda yakin ingin menghapus transaksi : ", //@C_column (Nama, Id Dll)+c
                'c_column' => 'inv_number', //@Pilih salah satu dari table column
            ],
            'action' => 'transaction_action', //@action
        ];

        showQueryTable($datatables);
    }

    public function destroy($id)
    {
        $photo = $this->bm->getWhere('payments', ['payment_id' => $id])->row()->photo;
        $this->db->delete('payments', ['payment_id' => $id]);
        $this->db->delete('pays', ['payment_id' => $id]);
        if (!is_null($photo)) {
            if (file_exists("./assets/admin/payments/" . $photo)) {
                unlink("./assets/admin/payments/" . $photo);
            }
        }
        response(['id' => $id]);
    }

    public function edit($paymentId)
    {
        $data['view'] = 'admin/payment/transaction/edit';
        $data['scripts'] = 'admin/payment/transaction/_scripts';
        $data['title'] = 'Transaksi - Edit Data';
        $data['menu'] = 'payment';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'transaction' : '';
        $data['_modal'] = 'admin/template/_modal';

        $payment = $this->tm->getPaymentList($paymentId); //@List transaksi
        $prices = $this->tm->getPriceList($payment->generation_id, $payment->semester_id); //@List harga
        $pays = $this->tm->getPayList($paymentId)->result(); //@List pembayaran
        $paids = $this->tm->getOthersPay($paymentId, $payment->student_id); //@Pembayaran pada transaksi lain

        //@Pembayaran di transaksi lain, $paid(Array)
        foreach ($paids as $pay) {
            $paid[$pay->price_id] = $pay->pay;
        }

        //@List harga
        foreach ($prices as $price) {
            $priceList[] = [
                'price_id' => $price->price_id,
                'price' => $price->price,
                'type_name' => $price->type_name,
                'installment' => $price->installment,
                'paid' => (!empty($paid[$price->price_id])) ? $paid[$price->price_id] : 0,
                'max_pay' => (!empty($paid[$price->price_id])) ? $price->price - $paid[$price->price_id] : $price->price,
                'semester_name' => $payment->semester_alias,
            ];
        }

        //@List Pembayaran, $priceId(Array), $payList(Array)
        foreach ($pays as $pay) {
            $priceId[] = $pay->price_id;
            $payList[] = [
                'semester_name' => $payment->semester_alias,
                'pay_id' => $pay->pay_id,
                'price_id' => $pay->price_id,
                'pay' => $pay->pay,
                'price' => $pay->price,
                'type_name' => ($pay->installment > 0) ? $pay->type_name . " (Bulan #" . $pay->installment . ")" : $pay->type_name,
                'paid' => (!empty($paid[$pay->price_id])) ? $paid[$pay->price_id] : 0,
                'max_pay' => (!empty($paid[$pay->price_id])) ? $pay->price - $paid[$pay->price_id] : $pay->price,
            ];
        }

        $data['payment_id'] = $paymentId;
        $data['pays'] = $payList;
        $data['prices'] = $priceList;
        $data['price_id'] = $priceId;
        $data['payment'] = $payment;
        $data['student_id'] = $payment->student_id;

        $this->load->view('admin/template/app', $data);
    }

    public function update($paymentId, $studentId)
    {
        //@Array pembayaran lama
        $oldPay = $this->input->post('old_pay');
        $oldPayId = $this->input->post('old_pay_id');
        $oldMaxPay = $this->input->post('old_max_pay');
        $oldPriceId = $this->input->post('old_price_id');
        $oldType = $this->input->post('old_type');

        //@Array pembayaran baru
        $newPay = $this->input->post('new_pay');
        $newPriceId = $this->input->post('new_price_id');
        $newMaxPay = $this->input->post('new_max_pay');
        $newPriceId = $this->input->post('new_price_id');
        $newType = $this->input->post('new_type');

        $payAmount = $this->input->post('pay_amount');

        $total = 0;

        if (!is_null($oldPay) > 0) {
            for ($i = 0; $i < count($oldPay); $i++) {
                $oldAmount = moneyClean($oldPay[$i]);
                if ($oldAmount > 0) {
                    if ($oldAmount <= $oldMaxPay[$i]) {
                        //@Data pembayaran lama
                        $old[] = [
                            'payment_id' => $paymentId,
                            'pay' => $oldAmount,
                            'pay_id' => $oldPayId[$i],
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                        $total += $oldAmount;
                    } else {
                        if ($oldMaxPay[$i] > 0) {
                            $errors['pay' . $oldPriceId[$i]] = "Harus lebih kecil atau sama dengan " . toRp($oldMaxPay[$i]);
                        } else {
                            $errors['pay' . $oldPriceId[$i]] = "Tagihan sudah lunas";
                        }
                    }
                } else {
                    $errors['pay' . $oldPriceId[$i]] = "Tidak boleh Rp. 0";
                }
            }
        }

        if (!is_null($newPay)) {
            for ($i = 0; $i < count($newPay); $i++) {
                $newAmount = moneyClean($newPay[$i]);
                if ($newAmount) {
                    if ($newAmount <= $newMaxPay[$i]) {
                        //@Data pembayaran baru
                        $new[] = [
                            'payment_id' => $paymentId,
                            'student_id' => $studentId,
                            'pay' => $newAmount,
                            'price_id' => $newPriceId[$i],
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                        $total += $newAmount;
                    } else {
                        if ($newMaxPay[$i] > 0) {
                            $errors['pay' . $newPriceId[$i]] = "Harus lebih kecil atau sama dengan " . toRp($newMaxPay[$i]);
                        } else {
                            $errors['pay' . $newPriceId[$i]] = "Tagihan sudah lunas";
                        }
                    }
                } else {
                    $errors['pay' . $newPriceId[$i]] = "Tidak boleh Rp. 0";
                }
            }
        }

        if (empty($errors)) {
            if (!empty($old)) {
                //@Update pembayaran lama
                $this->db->update_batch('pays', $old, 'pay_id');
            }
            if (!empty($new)) {
                //@Tambah pembayaran baru
                $this->db->insert_batch('pays', $new);
            }
            $t_list = "";
            foreach ($oldType as $ot) {
                if($t_list == ""){
                    $t_list = $ot;
                }else{
                    $t_list = $t_list.', '.$ot;
                }
            }
            foreach ($newType as $nt) {
                if($t_list == ""){
                    $t_list = $nt;
                }else{
                    $t_list = $t_list.', '.$nt;
                }
            }
            //@Update data transaksi
            $dataPayment = [
                'bill_amount' => $total,
                'description' => $t_list,
                'money_back' => $payAmount - $total,
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $this->db->where('payment_id', $paymentId)->update('payments', $dataPayment);
            $this->session->set_flashdata('notify', [
                'message' => "Berhasil mengupdate pembayaran",
                'level' => "success",
            ]);
            response();
        } else {
            response(['errors' => $errors]);
        }

    }

    public function remove($paymentId, $payId, $paid)
    {
        $this->db->delete('pays', ['pay_id' => $payId]); //@Hapus pembayaran
        $check = $this->bm->getWhere('pays', ['payment_id' => $paymentId])->row(); //@Cek list pembayaran dari transaksi
        if ($check) {
            $this->bm->updateAmount($paymentId, $paid);
            $this->session->set_flashdata('notify', [
                'message' => "Berhasil menghapus pembayaran",
                'level' => "warning",
            ]);
            response(['status' => 'update']);
        } else {
            $this->db->delete('payments', ['payment_id' => $paymentId]);
            $this->session->set_flashdata('notify', [
                'message' => "Invoice telah dihapus",
                'level' => "danger",
            ]);
            response(['status' => 'empty']);
        }
    }

    public function editPayAmount($paymentId)
    {
        $data['payment'] = $this->bm->getWhere('payments', ['payment_id' => $paymentId])->row();
        $this->load->view('admin/payment/transaction/edit_pay_amount', $data);
    }

    public function updatePayAmount($paymentId)
    {
        $payAmount = moneyClean($this->input->post('pay_amount'));
        $billAmount = moneyClean($this->input->post('bill_amount'));
        if ($payAmount >= $billAmount) {
            $data = [
                'money_back' => $payAmount - $billAmount,
                'pay_amount' => $payAmount,
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $this->db->where('payment_id', $paymentId)->update('payments', $data);
            $this->session->set_flashdata('notify', [
                'message' => "Berhasil mengupdate pembayaran",
                'level' => "success",
            ]);
            response();
        } else {
            response(['errors' => ['pay' => 'Pembayaran harus lebih besar atau sama dengan tagihan']]);
        }
    }

    public function paymentPrint($paymentId)
    {
        $data['title'] = "Cetak Bukti Pembayaran";
        $payment = $this->tm->detailPaymentPrint($paymentId);
        $pays = $this->tm->getPayList($paymentId)->result_array();
        $data['payment'] = $payment;
        $data['pays'] = $pays;
        // $data['prices'] = $this->tm->getPriceListNoInstallment($payment->generation_id, $payment->semester_id);
        // $data['totalPriceSemester'] = $this->tm->totalPriceSemester($payment->semester_id, $payment->generation_id);
        // $data['totalPaySemester'] = $this->tm->totalPaySemester($payment->semester_id, $payment->generation_id, $payment->student_id);
        $this->load->view('admin/payment/transaction/payment_print', $data);
    }

    public function leavePrint($leaveId)
    {
        $data['title'] = "Cetak Bukti Pembayaran Cuti";
        $data['leave'] = $this->tm->detailLeavePayment($leaveId);
        $this->load->view('admin/payment/transaction/leave_print', $data);
    }

}
