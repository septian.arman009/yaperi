<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Parents extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('basic_model', 'bm');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function index()
    {
        $data['title'] = 'Daftar Orang Tua';
        $data['view'] = 'admin/parent/index';
        $data['scripts'] = 'admin/parent/_scripts';
        $data['menu'] = 'students';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'parent' : '';
        $this->load->view('admin/template/app', $data);
    }

    public function parentTable()
    {

        $query = "SELECT a.*, CONCAT('Rp. ', fRupiah(a.income)) AS income, b.student_name, b.reg_number
                            ,b.student_name
                        FROM parents as a
                        JOIN students as b USING(student_id)";

        $datatables = [
            'query' => $query,
            'column' => [
                'parent_id',
                'reg_number',
                'student_name',
                'relation',
                'parent_name',
                'education',
                'job',
                'income',
            ],
            'searchOperator' => 'HAVING',
            'edit_url' => '',
            'delete_url' => '',
            'confirm' => [
                'message' => "Apakah anda yakin ingin menghapus orang tua : ", //c_column (Nama, Id Dll)+c
                'c_column' => 'parent_name', //Pilih salah satu dari table $column
            ],
            'action' => '', //Level action
        ];

        showQueryTable($datatables);
    }

    public function create($studentId)
    {
        $data['title'] = 'Daftar Orang Tua';
        $data['view'] = 'admin/parent/create';
        $data['scripts'] = 'admin/parent/_scripts';
        $data['menu'] = 'students';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'parent' : '';
        $data['parent'] = $this->bm->getWhere('parents', ['student_id' => $studentId, 'relation' => 'Ayah'])->row();
        $data['parent1'] = $this->bm->getWhere('parents', ['student_id' => $studentId, 'relation' => 'Ibu'])->row();
        $data['studentId'] = $studentId;
        $data['student'] = $this->bm->getWhere('students', ['student_id' => $studentId])->row()->student_name;
        $this->load->view('admin/template/app', $data);
    }

    public function store($studentId)
    {
        if ($this->vm->storeParent()) {
            $data[] = [
                'student_id' => $studentId,
                'relation' => 'Ayah',
                'parent_name' => ucwords($this->input->post('parent_name')),
                'birth_place' => ucwords($this->input->post('birth_place')),
                'birth_date' => toMysqlDate($this->input->post('birth_date')),
                'religion' => $this->input->post('religion'),
                'education' => $this->input->post('education'),
                'job' => ucwords($this->input->post('job')),
                'income' => moneyClean($this->input->post('income')),
            ];

            $data[] = [
                'student_id' => $studentId,
                'relation' => 'Ibu',
                'parent_name' => ucwords($this->input->post('parent_name1')),
                'birth_place' => ucwords($this->input->post('birth_place1')),
                'birth_date' => toMysqlDate($this->input->post('birth_date1')),
                'religion' => $this->input->post('religion1'),
                'education' => $this->input->post('education1'),
                'job' => ucwords($this->input->post('job1')),
                'income' => moneyClean($this->input->post('income1')),
            ];

            $this->db->insert_batch('parents', $data);
            response(['message' => "Berhasil menambah data orang tua"]);
        } else {
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function update($parentId, $parent1Id)
    {
        if ($this->vm->storeParent()) {
            $data[] = [
                'parent_id' => $parentId,
                'parent_name' => ucwords($this->input->post('parent_name')),
                'birth_place' => ucwords($this->input->post('birth_place')),
                'birth_date' => toMysqlDate($this->input->post('birth_date')),
                'religion' => $this->input->post('religion'),
                'education' => $this->input->post('education'),
                'job' => ucwords($this->input->post('job')),
                'income' => moneyClean($this->input->post('income')),
            ];

            $data[] = [
                'parent_id' => $parent1Id,
                'parent_name' => ucwords($this->input->post('parent_name1')),
                'birth_place' => ucwords($this->input->post('birth_place1')),
                'birth_date' => toMysqlDate($this->input->post('birth_date1')),
                'religion' => $this->input->post('religion1'),
                'education' => $this->input->post('education1'),
                'job' => ucwords($this->input->post('job1')),
                'income' => moneyClean($this->input->post('income1')),
            ];

            $this->db->update_batch('parents', $data, 'parent_id');
            $this->session->set_flashdata('notify', [
                'message' => "Berhasil mengubah data orang tua",
                'level' => "success",
            ]);
            response();
        } else {
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

}
