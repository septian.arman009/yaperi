<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Price extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('basic_model', 'bm');
        $this->load->model('price_model', 'pm');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function index()
    {
        $data['title'] = 'Daftar Harga';
        $data['view'] = 'admin/payment/price/index';
        $data['scripts'] = 'admin/payment/price/_scripts';
        $data['menu'] = 'payment';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'price' : '';
        $this->load->view('admin/template/app', $data);
    }

    public function priceTable()
    {
        $query = "SELECT a.generation_id, a.generation_name, CONCAT('Rp. ', fRupiah(SUM(b.price))) AS total_price,
                            (b.created_at) AS created_at, max(b.updated_at) AS updated_at
                    FROM generations AS a
                    LEFT JOIN prices AS b ON a.generation_id = b.generation_id
                    GROUP BY a.generation_id";

        $datatables = [
            'query' => $query,
            'column' => [
                'generation_id',
                'generation_name',
                'total_price',
                'created_at',
                'updated_at',
            ],
            'searchOperator' => 'HAVING',
            'edit_url' => base_url('price/edit/'),
            'delete_url' => base_url('price/destroy/'),
            'confirm' => [
                'message' => "Apakah anda yakin ingin menghapus jenis pembayaran : ", //c_column (Nama, Id Dll)+c
                'c_column' => 'generation_name', //Pilih salah satu dari table $column
            ],
            'action' => 'basic_action', //Level action
        ];

        showQueryTable($datatables);
    }

    public function create()
    {
        $this->form(null, 'Harga - Atur Harga');
    }

    public function store()
    {
        $price = moneyClean($this->input->post('price'));
        if ($price > 0) {
            $installment = $this->input->post('installment');
            $type_id = $this->input->post('type_id');
            $type_name = $this->bm->getWhere('types', ['type_id' => $type_id])->row()->type_name;

            if (!is_null($installment)) {
                for ($i = 1; $i <= 6; $i++) {
                    $data[] = [
                        'generation_id' => $this->input->post('generation_id'),
                        'semester_id' => $this->input->post('semester_id'),
                        'type_id' => $type_id,
                        'user_id' => me()['user_id'],
                        'price' => $price / 6,
                        'installment' => $i,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ];
                }
                $this->db->insert_batch('prices', $data);
            } else {
                $data = [
                    'generation_id' => $this->input->post('generation_id'),
                    'semester_id' => $this->input->post('semester_id'),
                    'type_id' => $type_id,
                    'user_id' => me()['user_id'],
                    'price' => $price,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                $this->db->insert('prices', $data);
            }

            response(['message' => "Berhasil menambah harga"]);
        } else {
            response(['errors' => ['price' => "* Harga harus diatas Rp. 0"]]);
        }

    }

    public function typeCheck($generationId, $semesterId)
    {
        $priceData = $this->bm->getWhere('prices', [
            'generation_id' => $generationId,
            'semester_id' => $semesterId,
        ]);

        $html = "<option value=''>- Jenis Pembayaran -</option>";

        if (!$priceData->row()) {
            $typeData = $this->bm->getAll('types')->result();

            foreach ($typeData as $type) {
                $html .= "<option value='$type->type_id'>$type->type_name</option>";
            }
            echo $html;
        } else {
            $typeId = [];
            foreach ($priceData->result() as $price) {
                $typeId[] = $price->type_id;
            }
            $typeData = $this->db->where_not_in('type_id', $typeId)->get('types')->result();
            foreach ($typeData as $type) {
                $html .= "<option value='$type->type_id'>$type->type_name</option>";
            }
            echo $html;
        }
    }

    public function priceDetail($generationId)
    {
        $prices = $this->pm->getPrice($generationId);
        $semesters = $this->bm->getAll('semesters')->result();
        $semsterList = array();

        $billing = 0;
        foreach ($semesters as $semester) {
            $priceList = array();
            $total = 0;
            foreach ($prices as $price) {
                if ($price->semester_id == $semester->semester_id) {
                    $priceList[] = [
                        'price_id' => $price->price_id,
                        'payment_type' => $price->type_name,
                        'price' => $price->price,
                        'installment' => $price->installment,
                    ];
                    $total += $price->price;
                }
            }
            $semsterList[] = [
                'semesterName' => $semester->semester_name,
                'priceList' => $priceList,
                'total' => $total,
            ];
            $billing += $total;
        }

        $data['billing'] = $billing;
        $data['generationName'] = $this->bm->getWhere('generations', ['generation_id' => $generationId])->row()->generation_name;
        $data['semesters'] = $semsterList;

        $paidLeave = $this->bm->getWhere('pay_leaves', ['generation_id' => $generationId])->row();
        if ($paidLeave) {
            $data['paidLeave'] = $paidLeave->price;
        } else {
            $data['paidLeave'] = 0;
        }
        $this->load->view('admin/payment/price/_price_detail', $data);
    }

    public function edit($generationId)
    {
        $this->form($generationId, 'Harga - Atur Harga');
    }

    public function form($generationId, $title)
    {
        $data['view'] = 'admin/payment/price/create';
        $data['scripts'] = 'admin/payment/price/_scripts';
        $data['_modal'] = 'admin/template/_modal';
        $data['title'] = $title;
        $data['menu'] = 'payment';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'price' : '';
        $data['generationId'] = $generationId;
        $data['generations'] = $this->db->order_by('generation_name', 'desc')->get('generations')->result();
        $data['semesters'] = $this->db->order_by('semester_name', 'desc')->get('semesters')->result();
        $data['types'] = $this->db->order_by('type_name', 'desc')->get('types')->result();

        $this->load->view('admin/template/app', $data);
    }

    public function editModal($priceId)
    {
        $price = $this->pm->getPriceEdit($priceId);

        $data['price_id'] = $priceId;
        $data['generation'] = $price->generation_name;
        $data['semester'] = $price->semester_name;
        $data['type'] = $price->type_name;
        $data['price'] = toRp($price->price);

        $this->load->view('admin/payment/price/edit_modal', $data);
    }

    public function update($priceId)
    {
        $price = moneyClean($this->input->post('price_edit'));
        if ($price > 0) {
            $data = [
                'user_id' => me()['user_id'],
                'price' => $price,
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            $this->db->where('price_id', $priceId)->update('prices', $data);
            response(['message' => "Berhasil mengubah harga"]);
        } else {
            response(['errors' => ['price_edit' => "* Harga harus diatas Rp. 0"]]);
        }

    }

    public function destroy($id)
    {
        $generation_name = $this->bm->getWhere('generations', ['generation_id' => $id])->row()->generation_name;
        $this->db->delete('prices', ['generation_id' => $id]);
        $this->session->set_flashdata('notify', [
            'message' => "Berhasil menghapus harga pada " . $generation_name,
            'level' => "success",
        ]);
        response();
    }

    public function destroyOne($id)
    {
        $this->db->delete('prices', ['price_id' => $id]);
        response(['rules' => [
            'rule' => 'custom_delete',
            'message' => "Berhasil menghapus harga",
        ]]);
    }

    public function paidLeave($generationId)
    {
        $leave = $this->pm->getLeave($generationId);
        if (!$leave) {
            $leave = $this->bm->getWhere('generations', ['generation_id' => $generationId])->row();
            $data['price'] = 0;
        } else {
            $data['price'] = $leave->price;
        }

        $data['generation'] = $leave->generation_name;
        $data['generationId'] = $generationId;

        $this->load->view('admin/payment/price/paid_leave_modal', $data);
    }

    public function storeLeave($generationId)
    {
        $check = $this->bm->getWhere('pay_leaves', ['generation_id' => $generationId])->row();
        if ($check) {
            $data = [
                'price' => moneyClean($this->input->post('price_leave')),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $this->db->where('generation_id', $generationId)->update('pay_leaves', $data);
        } else {
            $data = [
                'generation_id' => $generationId,
                'price' => moneyClean($this->input->post('price_leave')),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $this->db->insert('pay_leaves', $data);
        }
        response(['message' => "Berhasil mengubah harga"]);
    }
}
