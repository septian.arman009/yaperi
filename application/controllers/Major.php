<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Major extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('basic_model', 'bm');
        $this->load->model('validate_model', 'vm');
        auth();
    }

    public function index()
    {
        $data['title'] = 'Daftar Jurusan';
        $data['view'] = 'admin/major/index';
        $data['scripts'] = 'admin/major/_scripts';
        $data['menu'] = 'master';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'major' : '';
        $this->load->view('admin/template/app', $data);
    }

    public function majorTable()
    {
        $query = "SELECT a.major_id, a.major_name, a.created_at AS created_at, a.updated_at AS updated_at, b.faculty_name
                    FROM majors AS a
                    LEFT JOIN faculties AS b ON a.faculty_id = b.faculty_id";

        $datatables = [
            'query' => $query,
            'column' => [
                'major_id',
                'major_name',
                'faculty_name',
                'created_at',
                'updated_at',
            ],
            'searchOperator' => 'HAVING',
            'edit_url' => base_url('major/edit/'),
            'delete_url' => base_url('major/destroy/'),
            'confirm' => [
                'message' => "Apakah anda yakin ingin menghapus jurusan : ", //c_column (Nama, Id Dll)+c
                'c_column' => 'major_name', //Pilih salah satu dari table $column
            ],
            'action' => 'basic_action', //Level action
        ];

        showQueryTable($datatables);
    }

    public function create()
    {
        $data['view'] = 'admin/major/create';
        $data['scripts'] = 'admin/major/_scripts';
        $data['title'] = 'Jurusan - Tambah Data';
        $data['menu'] = 'master';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'major' : '';
        $data['faculties'] = $this->db->get('faculties')->result();
        $this->load->view('admin/template/app', $data);
    }

    public function store()
    {
        if ($this->vm->storeMajor()) {
            $data = [
                'major_name' => ucwords($this->input->post('major_name')),
                'faculty_id' => $this->input->post('faculty_id'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $this->db->insert('majors', $data);
            response(['message' => "Berhasil menambah jurusan"]);
        } else {
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function edit($id)
    {
        $major = $this->bm->getWhere('majors', ['major_id' => $id])->result_array();

        $data['view'] = 'admin/major/edit';
        $data['scripts'] = 'admin/major/_scripts';
        $data['title'] = 'Jurusan - Edit Data';
        $data['menu'] = 'master';
        $data['sub'] = (me()['sidebar'] == 'nav-md') ? 'major' : '';
        $data['major'] = $major[0];
        $data['faculties'] = $this->db->get('faculties')->result();
        $this->load->view('admin/template/app', $data);
    }

    public function update($id)
    {
        if ($this->vm->updateMajor($id)) {
            $data = [
                'major_name' => ucwords($this->input->post('major_name')),
                'faculty_id' => $this->input->post('faculty_id'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            $this->db->where('major_id', $id)->update('majors', $data);
            $this->session->set_flashdata('notify', [
                'message' => "Berhasil mengubah jurusan",
                'level' => "success",
            ]);
            response();
        } else {
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function destroy($id)
    {
        $this->db->delete('majors', ['major_id' => $id]);
        response(['id' => $id]);
    }

}
