<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('backend_helper');
        $this->load->model('basic_model', 'bm');
        $this->load->model('validate_model', 'vm');
    }

    public function login()
    {
        guest();
        $data['view'] = 'auth/login';
        $data['title'] = 'Login - Yaperi';
        $this->load->view('auth/app', $data);
    }

    public function loginCheck()
    {
        $email = $this->input->post('email');
        $password = md5($this->input->post('password'));

        if ($this->vm->login($password)) {
            $data = [
                'email' => $email,
                'password' => $password,
            ];

            $user = $this->bm->getWhere('users', $data)->row();

            $session = array(
                'user_id' => $user->user_id,
                'name' => $user->name,
                'email' => $user->email,
                'photo' => $user->photo,
                'verified' => $user->verified,
                'sidebar' => 'nav-md'
            );
            $this->session->set_userdata(SESSION_KEY, $session);
            response();
        } else {
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function forgotPassword()
    {
        guest();
        $data['view'] = 'auth/forgot_password';
        $data['title'] = 'Lupas Password - Yaperi';
        $this->load->view('auth/app', $data);
    }

    public function sendLink()
    {
        $email = $this->input->post('email');
        if ($this->vm->sendLink()) {
            $token = generateVerificationToken();
            $message = 'Link Reset Password : ' . base_url('reset_password/' . $token);
            $subject = "Reset Password - Yaperi.com";

            if (sendMail($email, $message, $subject)) {
                $this->db->where('email', $email)->update('users', ['reset_token' => $token]);
                $this->session->set_flashdata('notify', [
                    'message' => "Berhasil mengirim link reset password",
                    'level' => "success",
                ]);
                response();
            } else {
                $this->session->set_flashdata('notify', [
                    'message' => "Gagal mengirim link reset password",
                    'level' => "danger",
                ]);
                response();
            }

        } else {
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function resetPassword($token)
    {
        $checkToken = $this->bm->getWhere('users', ['reset_token' => $token])->row();
        if ($checkToken) {
            $data['email'] = $checkToken->email;
            $data['view'] = 'auth/reset_password';
            $data['title'] = 'Reset Password - Yaperi';
            $this->load->view('auth/app', $data);
        } else {
            echo "Token tidak terdaftar";
        }

    }

    public function reset()
    {
        if ($this->vm->resetPassword()) {
            $email = $this->input->post('email');
            $password = md5($this->input->post('password'));

            $this->db->where('email', $email)->update('users', ['password' => $password, 'reset_token' => null]);

            $user = $this->bm->getWhere('users', ['email' => $email])->row();
            $session = array(
                'user_id' => $user->user_id,
                'name' => $user->name,
                'email' => $user->email,
                'photo' => $user->photo,
                'verified' => $user->verified,
            );

            $this->session->set_userdata(SESSION_KEY, $session);
            response();
        } else {
            $errors = $this->form_validation->error_array();
            response(['errors' => $errors]);
        }
    }

    public function emailVerification($token)
    {
        $checkToken = $this->bm->getWhere('users', ['verification_token' => $token])->row();
        if ($checkToken) {
            $update = $this->db->where('user_id', $checkToken->id)->update('users', ['verified' => 1, 'verification_token' => null]);
            if ($update) {
                $this->session->unset_userdata(SESSION_KEY);
                $session = array(
                    'user_id' => $checkToken->user_id,
                    'name' => $checkToken->name,
                    'email' => $checkToken->email,
                    'photo' => $checkToken->photo,
                    'verified' => 1,
                );

                $this->session->set_userdata(SESSION_KEY, $session);
                $this->session->set_flashdata('notify', [
                    'message' => "Selamat datang $checkToken->name, verifikasi email berhasil",
                    'level' => "success",
                ]);
                redirect('home');
            } else {
                echo "Terjadi kesalahan";
            }

        } else {
            echo "Token tidak terdaftar";
        }

    }

    public function setSidebar()
    {
      
        if(me()['sidebar'] == 'nav-sm'){
            $_SESSION[SESSION_KEY]['sidebar'] = 'nav-md';
        }else{
            $_SESSION[SESSION_KEY]['sidebar'] = 'nav-sm';
        }

    }

}
