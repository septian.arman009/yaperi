
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Generation_model extends CI_Model
{
    public function notInYear($year)
    {
        return $this->db->select(['generation_name', 'year'])
            ->where('year !=', $year)
            ->get('generations')->result();
    }
}