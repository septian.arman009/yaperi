<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Validate_model extends CI_Model
{

    public function userStore()
    {
        $this->load->library('form_validation');
        $rules = [
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|is_unique[users.email]',
                'errors' => [
                    'is_unique' => '* Email sudah digunakan',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function userUpdate($id)
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|editUnique[users.email.' . $id . '.user_id]',
                'errors' => [
                    'editUnique' => '* Email sudah terdaftar',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function login($password)
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|exist[users.email]|checkPasswordAuth[' . $password . ']',
                'errors' => [
                    'exist' => 'Email tidak terdaftar',
                    'checkPasswordAuth' => 'Password salah',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function sendLink()
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|exist[users.email]',
                'errors' => [
                    'exist' => 'Email tidak terdaftar',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function resetPassword()
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|valid_email|exist[users.email]',
                'errors' => [
                    'exist' => 'Email tidak terdaftar',
                    'valid_email' => 'Email tidak valid',
                ],
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|min_length[8]',
                'errors' => [
                    'min_length' => 'Password minimal adalah 8 karakter',
                ],
            ],
            [
                'field' => 'confirm',
                'label' => 'Confirm',
                'rules' => 'trim|matches[password]',
                'errors' => [
                    'matches' => 'Password tidak sama',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function changePassword()
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'old_password',
                'label' => 'Old Password',
                'rules' => 'trim|checkPassword',
                'errors' => [
                    'checkPassword' => '* Password salah',
                ],
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|min_length[8]',
                'errors' => [
                    'min_length' => '* Password minimal adalah 8 karakter',
                ],
            ],
            [
                'field' => 'confirm',
                'label' => 'Confirm',
                'rules' => 'trim|matches[password]',
                'errors' => [
                    'matches' => '* Password tidak sama',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function storeGeneration($year)
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'generation_name',
                'label' => 'Generation Name',
                'rules' => 'trim|is_unique_custom[generations.generation_name.year.' . $year . ']',
                'errors' => [
                    'is_unique_custom' => '* Angkatan sudah terdaftar',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function storeCategory()
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'category_name',
                'label' => 'Category Name',
                'rules' => 'trim|is_unique[categories.category_name]',
                'errors' => [
                    'is_unique' => '* Kategori sudah terdaftar',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function updateCategory($id)
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'category_name',
                'label' => 'Category Name',
                'rules' => 'trim|editUnique[categories.category_name.' . $id . '.category_id]',
                'errors' => [
                    'editUnique' => '* Kategori sudah terdaftar',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function storeType()
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'type_name',
                'label' => 'Type Name',
                'rules' => 'trim|is_unique[types.type_name]',
                'errors' => [
                    'is_unique' => '* Jenis pembayaran sudah terdaftar',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function updateType($id)
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'type_name',
                'label' => 'Type Name',
                'rules' => 'trim|editUnique[types.type_name.' . $id . '.type_id]',
                'errors' => [
                    'editUnique' => '* Jenis pembayaran sudah terdaftar',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function storeFaculty()
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'faculty_name',
                'label' => 'Faculty Name',
                'rules' => 'trim|is_unique[faculties.faculty_name]',
                'errors' => [
                    'is_unique' => '* Fakultas sudah terdaftar',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function updateFaculty($id)
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'faculty_name',
                'label' => 'Faculty Name',
                'rules' => 'trim|editUnique[faculties.faculty_name.' . $id . '.faculty_id]',
                'errors' => [
                    'editUnique' => '* Fakultas sudah terdaftar',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function storeMajor()
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'major_name',
                'label' => 'Major Name',
                'rules' => 'trim|is_unique[majors.major_name]',
                'errors' => [
                    'is_unique' => '* Jurusan sudah terdaftar',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function updateMajor($id)
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'major_name',
                'label' => 'Major Name',
                'rules' => 'trim|editUnique[majors.major_name.' . $id . '.major_id]',
                'errors' => [
                    'editUnique' => '* Jurusan sudah terdaftar',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function storeStudent()
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'reg_number',
                'label' => 'Nomor Induk Mahasiswa',
                'rules' => 'trim|is_unique[students.reg_number]',
                'errors' => [
                    'is_unique' => '* Nomor induk mahasiswa sudah terdaftar',
                ],
            ],
            [
                'field' => 'reg_number2',
                'label' => 'NIRM',
                'rules' => 'trim|is_unique[students.reg_number2]',
                'errors' => [
                    'reg_number2' => '* NIRM sudah terdaftar',
                ],
            ],
            [
                'field' => 'id_card',
                'label' => 'KTP',
                'rules' => 'trim|is_unique[students.id_card]',
                'errors' => [
                    'id_card' => '* Nomor KTP sudah terdaftar',
                ],
            ],
            [
                'field' => 'npwp',
                'label' => 'NPWP',
                'rules' => 'trim|is_unique[students.npwp]',
                'errors' => [
                    'npwp' => '* NPWP sudah terdaftar',
                ],
            ],
            [
                'field' => 'nisn',
                'label' => 'NISN',
                'rules' => 'trim|is_unique[students.nisn]',
                'errors' => [
                    'nisn' => '* NISN sudah terdaftar',
                ],
            ],
            [
                'field' => 'birth_date',
                'label' => 'Birth Date',
                'rules' => 'trim|checkIndoDate',
                'errors' => [
                    'checkIndoDate' => '* Format tanggal tidak benar',
                ],
            ],
            [
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'required',
                'errors' => [
                    'required' => '* Jenis kelamin belum dipilih',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function updateStudent($id)
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'reg_number',
                'label' => 'Registration Number',
                'rules' => 'trim|editUnique[students.reg_number.' . $id . '.student_id]',
                'errors' => [
                    'is_unique' => '* Nomor induk mahasiswa sudah terdaftar',
                ],
            ],
            [
                'field' => 'reg_number2',
                'label' => 'NIRM',
                'rules' => 'trim|editUnique[students.reg_number2.' . $id . '.student_id]',
                'errors' => [
                    'is_unique' => '* NIRM sudah terdaftar',
                ],
            ],
            [
                'field' => 'id_card',
                'label' => 'KTP',
                'rules' => 'trim|editUnique[students.id_card.' . $id . '.student_id]',
                'errors' => [
                    'is_unique' => '* Nomor KTP sudah terdaftar',
                ],
            ],
            [
                'field' => 'npwp',
                'label' => 'NPWP',
                'rules' => 'trim|editUnique[students.npwp.' . $id . '.student_id]',
                'errors' => [
                    'is_unique' => '* NPWP sudah terdaftar',
                ],
            ],
            [
                'field' => 'nisn',
                'label' => 'NISN',
                'rules' => 'trim|editUnique[students.nisn.' . $id . '.student_id]',
                'errors' => [
                    'is_unique' => '* NISN sudah terdaftar',
                ],
            ],
            [
                'field' => 'birth_date',
                'label' => 'Birth Date',
                'rules' => 'trim|checkIndoDate',
                'errors' => [
                    'checkIndoDate' => '* Format tanggal tidak benar',
                ],
            ],
            [
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'required',
                'errors' => [
                    'required' => '* Jenis kelamin belum dipilih',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function storeParent()
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'birth_date',
                'label' => 'Birth Date',
                'rules' => 'trim|checkIndoDate',
                'errors' => [
                    'checkIndoDate' => '* Format tanggal tidak benar',
                ],
            ],
            [
                'field' => 'birth_date1',
                'label' => 'Birth Date1',
                'rules' => 'trim|checkIndoDate',
                'errors' => [
                    'checkIndoDate' => '* Format tanggal tidak benar',
                ],
            ],

        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

    public function storePay()
    {
        $this->load->library('form_validation');
        $this->load->helper('validation_helper');
        $rules = [
            [
                'field' => 'student_pay',
                'label' => 'Payment',
                'rules' => 'trim|required',
                'errors' => [
                    'required' => '* Nominal pembayaran masih kosong',
                ],
            ],
            [
                'field' => 'created_at',
                'label' => 'Payment Date',
                'rules' => 'trim|required|checkIndoDate2',
                'errors' => [
                    'required' => '* Tanggal pembayaran tidak boleh kosong',
                    'checkIndoDate2' => '* Format tanggal tidak benar',
                ],
            ],
        ];
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }

}
