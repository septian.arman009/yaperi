<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Basic_model extends CI_Model
{
    public function getWhere($table, $data)
    {
        return $this->db->get_where($table, $data);
    }

    public function getAll($table)
    {
        return $this->db->get($table);
    }

    public function lastId($table)
    {
        $query = $this->db->query("SHOW TABLE STATUS LIKE '$table'")->result_array();
        return $query[0]['Auto_increment'];
    }

    public function insertGetId($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    //Query
    public function payDetail($payId)
    {
        $sql = "SELECT sum(a.pay) as payIn, a.student_id, b.price_id, b.price, c.pay_amount, c.bill_amount, c.money_back, a.payment_id, d.type_name
                        FROM pays as a
                        JOIN prices as b ON a.price_id = b.price_id
                        JOIN payments as c ON a.payment_id = c.payment_id
                        JOIN types as d ON b.type_id = d.type_id
                        WHERE a.price_id = (SELECT price_id FROM pays WHERE pay_id = $payId)";
        return $this->db->query($sql)->row();
    }

    public function updateAmount($paymentId, $paid)
    {
        return $this->db->query("UPDATE payments
                                        SET bill_amount = bill_amount - $paid, money_back = pay_amount - bill_amount
                                        WHERE payment_id = $paymentId");
    }

    public function getProvince()
    {
        $sql = "SELECT * FROM provinces order by name";
        return $this->db->query($sql)->result();
    }

    public function getCity($provinceId)
    {
        $sql = "SELECT * FROM regencies where province_id=$provinceId order by name";
        return $this->db->query($sql)->result();
    }

    public function getSubDistrict($cityId)
    {
        $sql = "SELECT * FROM districts where regency_id=$cityId order by name";
        return $this->db->query($sql)->result();
    }

    public function getVillageOffice($districtId)
    {
        $sql = "SELECT * FROM villages where district_id=$districtId order by name";
        return $this->db->query($sql)->result();
    }

    public function lastInv()
    {
        return $this->db->select('inv_number')
                            ->from('payments')
                            ->where('year(created_at)', date('Y'))
                            ->order_by('payment_id', 'desc')
                            ->limit(1)
                            ->get()->row()->inv_number;
    }

    public function thisMonth($month, $year)
    {
        return $this->db->select('sum(pay) as pay')
                            ->from('pays')
                            ->where('month(created_at)', $month)
                            ->where('year(created_at)', $year)
                            ->get()->row()->pay;
    }

    public function prevMonth($newMonth, $newYear)
    {
        return $this->db->select('sum(pay) as pay')
                            ->from('pays')
                            ->where('month(created_at)', $newMonth)
                            ->where('year(created_at)', $newYear)
                            ->get()->row()->pay;
    }

    public function days($month, $year)
    {
        return $this->db->select('sum(pay) as pay, day(created_at) as day')
                            ->from('pays')
                            ->where('month(created_at)', $month)
                            ->where('year(created_at)', $year)
                            ->group_by('day(created_at)')
                            ->get()->result_array();
    }

    public function months($year)
    {
        return $this->db->select('sum(pay) as pay, month(created_at) as month')
                            ->from('pays')
                            ->where('year(created_at)', $year)
                            ->group_by('month(created_at)')
                            ->get()->result_array();

    }

    public function yearIncome($year)
    {
        return $this->db->select('sum(pay) as income')
                            ->from('pays')
                            ->where('year(created_at)', $year)
                            ->get()->row()->income;
    }

    public function paymentHome($month)
    {
        return $this->db->select('a.*, b.student_name')
                        ->from('payments as a')
                        ->join('students as b', 'a.student_id = b.student_id')
                        ->where('month(a.created_at)', $month)
                        ->where('year(a.created_at)', date('Y'))
                        ->order_by('created_at', 'DESC')
                        ->get()->result();
    }

    public function dayHome($month, $year)
    {
        return $this->db->select('sum(pay) as pay, day(created_at) as day')
                            ->from('pays')
                            ->where('month(created_at)', $month)
                            ->where('year(created_at)', $year)
                            ->group_by('day(created_at)')
                            ->get()->result_array();
    }

    public function today()
    {
        return $this->db->select('sum(pay) as pay')->from('pays')->where('date(created_at)', date('Y-m-d'))->get()->row()->pay;
    }

}
