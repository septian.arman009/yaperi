<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Price_model extends CI_Model
{
    public function getPrice($generationId)
    {
        return $this->db->select("a.price_id, a.generation_id, a.semester_id, a.type_id, b.type_name, a.price, a.installment")
            ->from('prices as a')
            ->join('types as b', 'a.type_id = b.type_id')
            ->where('a.generation_id', $generationId)
            ->get()->result();
    }

    public function getPriceEdit($priceId)
    {
        return $this->db->select('a.price_id, b.generation_name, c.semester_name, d.type_name, a.price')
            ->from('prices as a')
            ->join('generations as b', 'a.generation_id = b.generation_id')
            ->join('semesters as c', 'a.semester_id = c.semester_id')
            ->join('types as d', 'a.type_id = d.type_id')
            ->where('a.price_id', $priceId)
            ->get()->row();
    }

    public function getLeave($generationId)
    {
        return $this->db->select('a.leave_id, a.generation_id, a.price, b.generation_name')
            ->from('pay_leaves as a')
            ->join('generations as b', 'a.generation_id = b.generation_id')
            ->where('a.generation_id', $generationId)
            ->get()->row();
    }
}
