<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction_model extends CI_Model
{

    public function getPaymentList($paymentId)
    {
        return $this->db->select('a.*, a.created_at as payment_date, b.student_id, b.generation_id,
                                    c.major_name, d.semester_name, e.generation_name')
            ->from('payments as a')
            ->join('students as b', 'a.student_id = b.student_id')
            ->join('majors as c', 'b.major_id = c.major_id')
            ->join('semesters as d', 'a.semester_id = d.semester_id')
            ->join('generations as e', 'b.generation_id = e.generation_id')
            ->where('a.payment_id', $paymentId)
            ->get()->row();
    }

    public function getPriceList($generationId, $semesterId)
    {
        return $this->db->select('a.price_id, a.price, b.type_name, a.installment')
            ->from('prices as a')
            ->join('types as b', 'a.type_id = b.type_id')
            ->join('semesters as c', 'a.semester_id = c.semester_id')
            ->where('a.generation_id', $generationId)
            ->where('a.semester_id', $semesterId)
            ->get()->result();
    }

    // public function getPriceListNoInstallment($generationId, $semesterId)
    // {
    //     return $this->db->select('a.price_id, a.price, b.type_name, b.type_id')
    //         ->from('prices as a')
    //         ->join('types as b', 'a.type_id = b.type_id')
    //         ->join('semesters as c', 'a.semester_id = c.semester_id')
    //         ->where('a.generation_id', $generationId)
    //         ->where('a.semester_id', $semesterId)
    //         ->group_by('b.type_id')
    //         ->get()->result();
    // }

    public function getPayList($paymentId)
    {
        return $this->db->select('a.pay_id, a.pay, a.price_id, b.price, c.type_name, b.installment, c.type_id')
            ->from('pays as a')
            ->join('prices as b', 'a.price_id = b.price_id')
            ->join('types as c', 'b.type_id = c.type_id')
            ->where('a.payment_id', $paymentId)
            ->get();
    }

    public function getOthersPay($paymentId, $studentId)
    {
        return $this->db->select('price_id, sum(pay) as pay')
            ->from('pays')
            ->where('payment_id !=', $paymentId)
            ->where('student_id', $studentId)
            ->group_by('price_id')
            ->get()->result();
    }

    public function detailPaymentPrint($paymentId)
    {
        return $this->db->select('a.inv_number, a.semester_alias, a.semester_id, a.pay_amount, a.bill_amount, a.money_back, a.created_at,
                                    b.student_name, b.reg_number, b.generation_id, b.student_id,
                                    c.major_name,
                                    d.generation_name')
            ->from('payments as a')
            ->join('students as b', 'a.student_id = b.student_id')
            ->join('majors as c', 'b.major_id = c.major_id')
            ->join('generations as d', 'b.generation_id = d.generation_id')
            ->where('a.payment_id =', $paymentId)
            ->get()->row();
    }

    public function detailLeavePayment($leaveId)
    {
        return $this->db->select('a.leave_id, a.price, a.created_at, a.semester_id, a.inv_number
                                    , b.student_name, b.semester_id as ss_id, b.reg_number
                                    , c.major_name
                                    , d.generation_name')
            ->from('leaves as a')
            ->join('students as b', 'a.student_id = b.student_id')
            ->join('majors as c', 'b.major_id = c.major_id')
            ->join('generations as d', 'b.generation_id = d.generation_id')
            ->where('a.leave_id =', $leaveId)
            ->get()->row();
    }

    // public function totalPriceSemester($semesterId, $generationId)
    // {
    //     return $this->db->select('sum(price) as price')
    //                         ->from('prices')
    //                         ->where('semester_id', $semesterId)
    //                         ->where('generation_id', $generationId)
    //                         ->get()->row()->price;
    // }

    // public function totalPaySemester($semesterId, $generationId, $studentId)
    // {
    //     return $this->db->select('sum(a.pay) as pay')
    //                         ->from('pays as a')
    //                         ->join('prices as b', 'a.price_id = b.price_id')
    //                         ->where('b.semester_id', $semesterId)
    //                         ->where('b.generation_id', $generationId)
    //                         ->where('a.student_id', $studentId)
    //                         ->get()->row()->pay;
    // }
}
