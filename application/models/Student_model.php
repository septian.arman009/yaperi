<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Student_model extends CI_Model
{
    public function getStudent($studentId)
    {
        return $this->db->select('a.*, b.generation_name, c.major_name')
            ->from('students as a')
            ->join('generations as b', 'a.generation_id = b.generation_id')
            ->join('majors as c', 'a.major_id = c.major_id')
            ->where('a.student_id', $studentId)
            ->get()->row();
    }

    public function getPriceList($generationId, $semesterId)
    {
        return $this->db->select('a.price, a.price_id, a.installment, b.type_name')
            ->from('prices as a')
            ->join('types as b', 'a.type_id = b.type_id')
            ->where('a.generation_id', $generationId)
            ->where('a.semester_id', $semesterId)
            ->get()->result();
    }

    public function getPaidList($generationId, $semesterId, $studentId)
    {
        return $this->db->select('a.price_id, sum(b.pay) as pay')
            ->from('prices as a')
            ->join('pays as b', 'a.price_id = b.price_id')
            ->where('a.generation_id', $generationId)
            ->where('a.semester_id', $semesterId)
            ->where('b.student_id', $studentId)
            ->group_by('b.price_id')
            ->get()->result_array();
    }
}
