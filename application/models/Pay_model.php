<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pay_model extends CI_Model
{

    public function getStudents($studentName)
    {
        return $this->db->select('a.*, b.major_name, c.semester_name')
            ->from('students as a')
            ->join('majors as b', 'a.major_id = b.major_id')
            ->join('semesters as c', 'a.semester_id = c.semester_id')
            ->like('a.student_name', $studentName)
            ->get()->result();
    }

    public function getDetailStudent($studentId)
    {
        return $this->db->select('a.*, b.major_name, d.generation_name, e.name as village
                                    , f.name as district, g.name as regency, h.name as province')
            ->from('students as a')
            ->join('majors as b', 'a.major_id = b.major_id')
            ->join('generations as d', 'a.generation_id = d.generation_id')
            ->join('villages as e', 'a.village_id = e.id')
            ->join('districts as f', 'a.district_id = f.id')
            ->join('regencies as g', 'a.regency_id = g.id')
            ->join('provinces as h', 'a.province_id = h.id')
            ->where('a.student_id', $studentId)
            ->get()->row();
    }

    public function getLeaves($studentId, $generationId)
    {
        return $this->db->get_where('leaves', [
            'student_id' => $studentId,
            'generation_id' => $generationId,
        ])->result_array();
    }

    public function getPriceList($generationId)
    {
        return $this->db->select("a.price_id, a.generation_id, a.semester_id, a.type_id, b.type_name, a.price, a.installment")
            ->from('prices as a')
            ->join('types as b', 'a.type_id = b.type_id')
            ->where('a.generation_id', $generationId)
            ->get()->result();
    }

    public function getOthersPay($studentId)
    {
        return $this->db->select('price_id, sum(pay) as pay')
            ->from('pays')
            ->where('student_id', $studentId)
            ->group_by('price_id')
            ->get()->result();
    }

    public function getPayList($studentId)
    {
        return $this->db->select('a.pay_id, a.price_id, c.semester_name, d.type_name, a.pay, a.created_at')
            ->from('pays as a')
            ->join('prices as b', 'a.price_id = b.price_id')
            ->join('semesters as c', 'b.semester_id = c.semester_id')
            ->join('types as d', 'b.type_id = d.type_id')
            ->where('a.student_id', $studentId)
            ->order_by('created_at', 'DESC')
            ->get()->result();
    }

    public function monthPay($studentId, $year)
    {
        return $this->db->select('month(created_at) as month, sum(pay) as pay')
            ->where('student_id', $studentId)
            ->where('year(created_at)', $year)
            ->group_by('month')
            ->get('pays')->result_array();
    }

    public function leavePay($studentId, $year)
    {
        return $this->db->select('month(created_at) as month, sum(price) as pay')
            ->where('student_id', $studentId)
            ->where('year(created_at)', $year)
            ->group_by('month')
            ->get('leaves')->result_array();
    }

    public function getSemesterPrice($generationId)
    {
        return $this->db->select('sum(a.price) as price, b.semester_name')
            ->from('prices as a')
            ->join('semesters as b', 'a.semester_id = b.semester_id')
            ->where('a.generation_id', $generationId)
            ->group_by('a.semester_id')
            ->get()->result_array();
    }

    public function getSemesterPay($studentId)
    {
        return $this->db->select('sum(a.pay) as pay, c.semester_name')
            ->from('pays as a')
            ->join('prices as b', 'a.price_id = b.price_id')
            ->join('semesters as c', 'b.semester_id = c.semester_id')
            ->where('a.student_id', $studentId)
            ->group_by('b.semester_id')
            ->get()->result_array();
    }

}
