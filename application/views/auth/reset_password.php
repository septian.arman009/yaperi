<section class="login_content">
	<form class="form-horizontal form-auth"
		action="<?= base_url('reset') ?>"
        method="post"
        enctype="multipart/form-data"
		data-rule="default_update"
        data-redirect="<?= base_url('home') ?>"
        data-btn="#btn-reset"
        data-r_btn = "Reset">

		<h1>Reset Password</h1>

		<div class="alert alert-danger alert-dismissible fade in" role="alert" id="login-error">
			<div id="response-error"><ul></ul></div>
		</div>
        
		<div class="item form-group">
			<input type="email" name="email" value="<?= $email ?>" readonly class="form-control">
		</div>

		<div class="item form-group">
			<input type="password" name="password" placeholder="Password Baru" required="required" class="form-control">
		</div>

		<div class="item form-group">
			<input type="password" name="confirm" placeholder="Konfirmasi Password Baru" required="required" class="form-control">
		</div>

		<div>
			<button type="submit" class="btn btn-default" id="btn-reset">Reset</button>
		</div>

		<div class="separator">
			<a href="<?= base_url('login') ?>" class="pull-left"> Login</a>
		</div>

	</form>
</section>