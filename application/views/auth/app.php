<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?= $title ?></title>
	<link rel="icon" href="<?= base_url() ?>assets/admin/images/yaperi_02.png" type="image/x-icon" />

	<!-- Bootstrap -->
	<link href="<?= base_url() ?>assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?= base_url() ?>assets/admin/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- NProgress -->
	<link href="<?= base_url() ?>assets/admin/vendors/nprogress/nprogress.css" rel="stylesheet">
	<!-- Animate.css -->
	<link href="<?= base_url() ?>assets/admin/vendors/animate.css/animate.min.css" rel="stylesheet">

	<!-- Custom Theme Style -->
	<link href="<?= base_url() ?>assets/admin/build/css/custom.min.css" rel="stylesheet">
</head>

<body class="login">
	<div>
		<div class="login_wrapper">
			<div class="animate form login_form">
				<?php $this->load->view($view) ?>
			</div>
		</div>
	</div>
</body>

<!-- jQuery -->
<script src="<?= base_url() ?>assets/admin/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?= base_url() ?>assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Form Request -->
<script src="<?= base_url() ?>assets/admin/js/ajax.js"></script>
<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>
<script src="<?= base_url() ?>assets/admin/js/auth_request.js"></script>

<style>
	li {
		text-align: left;
	}
</style>

</html>