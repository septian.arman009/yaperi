<?php  flash() ?>
<div class="row tile_count">
	<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-users"></i> Jumlah Mahasiswa</span>
		<div class="count"><?= $student ?></div>
		<span class="count_bottom">Dari <i class="green"><?= ucwords(spelling($major_student)) ?></i> Jurusan</span>
	</div>
	<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-filter"></i> Jumlah Jurusan</span>
		<div class="count"><?= $major ?></div>
		<span class="count_bottom">Dari <i class="green"><?= ucwords(spelling($faculty_major)) ?></i> Fakultas</span>
	</div>
	<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-bank"></i> Jumlah Angkatan</span>
		<div class="count"><?= $generation ?></div>
	</div>
	<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-user"></i> Jumlah User</span>
		<div class="count"><?= $user ?></div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-money"></i> Pendapatan Hari Ini</span>
		<div class="count"><?= ($today) ? toRp($today) : toRp(0) ?></div>
		<span class="count_bottom">Tanggal : <?= date('d/m/Y') ?></span>
	</div>
</div>

<div class="row tile_count">
	<div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-money"></i> Pendapatan Bulan Sebelumnya</span>
		<div class="count"><?= ($prevMonth) ? toRp($prevMonth) : toRp(0) ?></div>
		<span class="count_bottom"><?= $prevName.' '.$newYear ?></span>
	</div>

	<div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-money"></i> Pendapatan Bulan Ini</span>
		<div class="count"><?= ($thisMonth) ? toRp($thisMonth) : toRp(0) ?></div>
		<span class="count_bottom"><?= $thisName.' '.$year ?></span>
		<span class="count_bottom">
			
				<?php if($thisMonth > $prevMonth){ 
					if($prevMonth > 0){
						$sisa = ($thisMonth - $prevMonth);
						if($sisa > 0){
							$percent = round(($sisa / $prevMonth) * 100);
						}
					}else{
						$percent = '> 100';
					}?>
				<i class="green">
					<i class="fa fa-sort-asc"></i>
					<?= $percent ?>% </i>
				<?php } else if($thisMonth < $prevMonth){ 
					if($thisMonth > 0){
						$sisa = ($prevMonth - $thisMonth);
						if($sisa > 0){
							$percent = round(($sisa / $thisMonth) * 100);
						}
					}else{
						$percent = 0;
					}?>
				<i class="red">
					<i class="fa fa-sort-desc"></i>
					<?= $percent ?>% </i>
				<?php } else { ?>
				<i class="blue">
					<i class="fa fa-sort-asc"></i>
					0% </i>
				<?php }?>
				Dari bulan sebelumnya
		</span>
	</div>
</div>

<hr>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="col-md-3 col-sm-12 col-xs-12 pull-right">
			<select id="month" class="form-control" onchange="changeMonth()">
				<?php for ($i=1; $i <= 12 ; $i++) { 
					$selected = ($i == date('m')) ? 'selected' : '';
					echo "<option $selected value='$i'>".toIndoMonth($i)."</option>";
				} ?>
			</select>
		</div>
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<h3 id="loading" style="display: none">Loading...</h3>
		<div id="month-graph"></div>
	</div>
</div>