<script>
    changeMonth();

    function changeMonth() {
        $("#loading").show();
        let month = $("#month").val();
        loadView('<?= base_url() ?>home/monthGraph/'+month, '#month-graph');
    }
</script>