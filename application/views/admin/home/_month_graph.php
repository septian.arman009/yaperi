<script src="<?= base_url() ?>assets/admin/vendors/raphael/raphael.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendors/morris.js/morris.min.js"></script>


<div id="month-data"></div>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Pembayaran Masuk Bulan <?= $month.' '.date('Y') ?></h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content custom-scroll">
				<ul class="list-unstyled timeline">
                <?php foreach ($payments as $payment) { ?>
					<li>
						<div class="block">
							<div class="tags">
								<a href="" class="tag">
									<span><?= $payment->inv_number ?></span>
								</a>
							</div>
							<div class="block_content">
								<h2 class="title">
									<a><?= $payment->student_name ?></a>
								</h2>
								<div class="byline">
									<span><?= toIndoDatetime($payment->created_at) ?></span> <a><?= $payment->semester_alias ?></a>
								</div>
								<p class="excerpt">
                                    Tagihan : <?= toRp($payment->bill_amount) ?>
								</p>
                                <p class="excerpt">
                                    Pembayaran : <?= toRp($payment->pay_amount) ?>
                                </p>
                                <?php if(!is_null($payment->photo)){ ?>
                                <a target="_blank" href=' <?= base_url('assets/admin/payments/'.$payment->photo) ?>' class="excerpt">
                                    Bukti Pembayaran : <?= $payment->photo ?>
                                </a>
                                <?php } ?>
							</div>
						</div>
					</li>
				<?php } ?>
				</ul>

			</div>
		</div>
	</div>
</div>

<script>
	Morris.Bar({
		element: 'month-data',
		data: <?= json_encode($day_income) ?> ,
		xkey: ['day'],
		ykeys: ['income'],
		labels: ['Total Pembayaran'],
		barRatio: 0.4,
		barColors: ['#26B99A'],
		xLabelAngle: 50,
		hideHover: 'auto',
		resize: true
	});

	$("#loading").hide();
</script>

<style>
    .timeline .tags {
        width: 120px !important;
    }
    .timeline .block {
        margin: 0 0 0 140px !important;
    }
    .excerpt {
        margin-bottom: 0px;
    }
</style>