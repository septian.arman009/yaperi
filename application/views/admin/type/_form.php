
<div class="item form-group group-category_id">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori </label>
	<div class="col-md-6 col-sm-6 col-xs-12">
	<select name="category_id" class="form-control">
        <?php 
            foreach ($categories as $cat) {
				if(!empty($type)){
					$selected = ($cat->category_id == $type['type_id']) ? "selected" : "";
				}
                echo "<option S$selected value='$cat->category_id'>$cat->category_name</option>";
            }
        ?>
    </select>
	</div>
</div>

<div class="item form-group group-type_name">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Pembayaran</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($type)) echo $type['type_name'] ?>" class="form-control col-md-7 col-xs-12"
			name="type_name" placeholder="ex. Pembayaran Gedung" required="required" type="text">
        <div id="response-type_name" class="response-error"></div>
	</div>
</div>

<div class="ln_solid"></div>
<div class="form-group">
	<div class="col-md-6 col-md-offset-3">
		<button id="btn-save" type="submit" class="btn btn-success">Simpan</button>
	</div>
</div>

