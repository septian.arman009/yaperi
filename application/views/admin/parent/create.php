<div class="page-title">
	<div class="title_left">
		<h3>Orang Tua <small> Tambah Data </small></h3>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Form Data Orang Tua (<?= $student ?>)</h2>
					<div class="clearfix"></div>
				</div>

				<a href="<?= base_url('student/edit/'.$studentId) ?>" class="btn btn-default">Kembali</a>

				<div class="ln_solid"></div>

				<div class="x_content">
					<?php flash() ?>
					<?php if(empty($parent) || empty($parent1)) {?>
					<form class="form-horizontal form-label-left form-data" novalidate="" 
							action="<?= base_url('parent/store/'.$studentId) ?>"
							method="post"
							enctype="multipart/form-data"
							data-rule="custom_store"
							data-btn="#btn-save">
					<?php }else{ ?>

						<form class="form-horizontal form-label-left form-data" novalidate="" 
							action="<?= base_url('parent/update') ?>/<?= $parent->parent_id ?>/<?= $parent1->parent_id?>"
							method="post"
							enctype="multipart/form-data"
							data-rule="default_update"
                            data-redirect="<?= base_url('parent/create/'.$studentId) ?>"
							data-btn="#btn-save">

					<?php } ?>

						<?php $this->load->view('admin/parent/_form') ?>

					</form>

				</div>
			</div>
		</div>
	</div>
</div>