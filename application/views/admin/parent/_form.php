<div class="item form-group group-parent_name">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Ayah</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($parent)) echo $parent->parent_name ?>"
			class="form-control col-md-7 col-xs-12" name="parent_name" required="required" type="text">
		<span id="response-parent_name" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-birth_place">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Tempat Lahir</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($parent)) echo $parent->birth_place ?>"
			class="form-control col-md-7 col-xs-12" name="birth_place" required="required" type="text">
		<span id="response-birth_place" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-birth_date">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Lahir</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($parent)) echo toIndoDate($parent->birth_date) ?>" name="birth_date" type="text"
			class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99/99/9999'" required="required">
		<span id="response-birth_date" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-religion">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Agama</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="religion" class="form-control" required="required">
		<option value="">- Pilih Agama -</option>
			<?php
				$religions = ['Islam', 'Katolik', 'Protestan', 'Hindu', 'Budha'];
				foreach ($religions as $key => $religion) {
					if(!empty($parent) && $religion == $parent->religion){
						echo "<option selected value='$religion'>$religion</option>";
					}else{
						echo "<option value='$religion'>$religion</option>";
					}
                }
			?>
		</select>
	</div>
</div>

<div class="item form-group group-education">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Pendidikan</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="education" class="form-control" required="required">
		<option value="">- Pilih Pendidikan -</option>
			<?php
				$educations = ['SD', 'SMP', 'SMA', 'S1', 'S2', 'S3'];
				foreach ($educations as $key => $education) {
					if(!empty($parent) && $education == $parent->education){
						echo "<option selected value='$education'>$education</option>";
					}else{
						echo "<option value='$education'>$education</option>";
					}
                }
			?>
		</select>
	</div>
</div>

<div class="item form-group group-job">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Pekerjaan</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($parent)) echo $parent->job ?>"
			class="form-control col-md-7 col-xs-12" name="job" required="required" type="text">
		<span id="response-job" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-income">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Pendapatan</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($parent)) echo toRp($parent->income) ?>"
			class="form-control col-md-7 col-xs-12 income" name="income" required="required" type="text">
		<span id="response-income" class="response-error"></span>
	</div>
</div>

<hr>

<div class="item form-group group-parent_name1">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Ibu</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($parent1)) echo $parent1->parent_name ?>"
			class="form-control col-md-7 col-xs-12" name="parent_name1" required="required" type="text">
		<span id="response-parent_name1" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-birth_place1">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Tempat Lahir</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($parent1)) echo $parent1->birth_place ?>"
			class="form-control col-md-7 col-xs-12" name="birth_place1" required="required" type="text">
		<span id="response-birth_place1" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-birth_date1">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Lahir</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($parent1)) echo toIndoDate($parent1->birth_date) ?>" name="birth_date1" type="text"
			class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99/99/9999'" required="required">
		<span id="response-birth_date1" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-religion1">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Agama</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="religion1" class="form-control" required="required">
		<option value="">- Pilih Agama -</option>
			<?php
				$religions = ['Islam', 'Katolik', 'Protestan', 'Hindu', 'Budha'];
				foreach ($religions as $key => $religion) {
					if(!empty($parent1) && $religion == $parent1->religion){
						echo "<option selected value='$religion'>$religion</option>";
					}else{
						echo "<option value='$religion'>$religion</option>";
					}
                }
			?>
		</select>
	</div>
</div>

<div class="item form-group group-education1">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Pendidikan</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="education1" class="form-control" required="required">
		<option value="">- Pilih Pendidikan -</option>
			<?php
				$educations = ['SD', 'SMP', 'SMA', 'S1', 'S2', 'S3'];
				foreach ($educations as $key => $education) {
					if(!empty($parent1) && $education == $parent1->education){
						echo "<option selected value='$education'>$education</option>";
					}else{
						echo "<option value='$education'>$education</option>";
					}
                }
			?>
		</select>
	</div>
</div>

<div class="item form-group group-job1">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Pekerjaan</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($parent1)) echo $parent1->job ?>"
			class="form-control col-md-7 col-xs-12" name="job1" required="required" type="text">
		<span id="response-job1" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-income1">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Pendapatan</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($parent1)) echo toRp($parent1->income) ?>"
			class="form-control col-md-7 col-xs-12 class income" name="income1" required="required" type="text">
		<span id="response-income1" class="response-error"></span>
	</div>
</div>

<div class="ln_solid"></div>
<div class="form-group">
	<div class="col-md-6 col-md-offset-3">
		<button id="btn-save" type="submit" class="btn btn-success">Simpan</button>
	</div>
</div>