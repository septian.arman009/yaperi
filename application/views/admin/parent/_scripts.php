<!-- validator -->
<script src="<?= base_url() ?>assets/admin/vendors/validator/validator.js"></script>
<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>
<script src="<?= base_url() ?>assets/admin/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
<script src="<?= base_url() ?>assets/admin/js/jquery.maskMoney.js"></script>

<script>
	$(document).ready(function () {
		if (document.body.contains(document.getElementById('parents-table'))) {
			$('#parents-table tfoot th').each(function () {
				var title = $(this).text();
				if (title != '')
					var inp = '<input type="text" class="form-control footer-s" placeholder="' + title +
						'" id="' + title + '" />';
				$(this).html(inp);
			});

			var table = $('#parents-table').DataTable({
				"processing": true,
				"serverSide": true,
				"order": [
					[1, 'desc']
				],
				"ajax": {
					"url": '<?= base_url('parent/table') ?>',
					"type": "POST"
				}
			});

			table.columns().every(function () {
				var that = this;
				$('input', this.footer()).on('keyup change', function () {
					if (that.search() !== this.value) {
						that.search(this.value).draw();
					}
				});
			});
		}else{
			$('.income').maskMoney({
				prefix: 'Rp. ',
				thousands: '.',
				decimal: ',',
				precision: 0
			});

			$(":input").inputmask();

		}
	});

	function custom_response($form, rule, message) {
		notify('Sukses', message, 'success');
	}
</script>

<style>
	#parents-table_filter {
		display: none;
	}
</style>