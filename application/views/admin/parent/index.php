<div class="page-title">
	<div class="title_left">
		<h3>Orang Tua</h3>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Daftar Orang Tua</h2>
					<div class="clearfix"></div>
				</div>
				<?php flash() ?>
				<div class="x_content table-responsive">
					<table id="parents-table" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th id="th" width="10%">ID</th>
								<th id="th">NIM</th>
								<th id="th">Mahasiswa</th>
								<th id="th">Hubungan</th>
								<th id="th">Orang Tua</th>
								<th id="th">Pendidikan</th>
								<th id="th">Pekerjaan</th>
								<th id="th">Pendapatan</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">ID</th>
								<th class="footer">NIM</th>
								<th class="footer">Mahasiswa</th>
								<th class="footer">Hubungan</th>
								<th class="footer">Orang Tua</th>
								<th class="footer">Pendidikan</th>
								<th class="footer">Pekerjaan</th>
								<th class="footer">Pendapatan</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>