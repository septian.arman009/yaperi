<!-- validator -->
<script src="<?= base_url() ?>assets/admin/vendors/validator/validator.js"></script>
<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>
<script src="<?= base_url() ?>assets/admin/js/jquery.maskMoney.js"></script>

<script>
	$(document).ready(function () {
		if (document.body.contains(document.getElementById('transaction-table'))) {
			$('#transaction-table thead th').each(function () {
				var title = $(this).text();
				if (title != '')
					var inp = '<textarea type="text" class="form-control footer-s" placeholder="' + title +
						'" id="' + title + '" />';
				$(this).html(inp);
			});

			var table = $('#transaction-table').DataTable({
				"processing": true,
				"serverSide": true,
				"order": [
					[0, 'desc']
				],
				"ajax": {
					"url": '<?= base_url('transaction/table') ?>',
					"type": "POST"
				}
			});

			table.columns().every(function () {
				var that = this;
				$('textarea', this.header()).on('keyup change', function () {
					if (that.search() !== this.value) {
						that.search(this.value).draw();
					}
				});
			});
		}else{

			$('.old_pay').maskMoney({
				prefix: 'Rp. ',
				thousands: '.',
				decimal: ',',
				precision: 0
			});
			
		}

	});

	this.price_id = [];

	function info(message) {
        $("#md-modal").modal('show');
		$(".modal-title").html("Info");
		$(".modal-custom").html("<p style='padding: 10px; font-size: 18px; text-align: center'>"+message+"</p>");
    }

	function addType(priceId, price, paid, maxPay, typeName, semesterName) {
		if(this.price_id.includes(priceId)){

		}else{
			this.price_id.push(priceId);
			
			$("#new_pay").append(
				"<div class='item form-group group-pay"+priceId+"'>"
					+"<label class='control-label col-md-4 col-sm-4 col-xs-12'>"+typeName+" ("+semesterName+")</label>"
					+"<div class='col-md-6 col-sm-6 col-xs-10'>"
						+"<input type='tex' value='Rp. 0' class='form-control' id='new_pay-"+priceId+"' name='new_pay[]'>"
						+"<input style='display:none' type='number' value='"+price+"' class='form-control' name='new_price[]'>"
						+"<input style='display:none' type='number' value='"+priceId+"' class='form-control' name='new_price_id[]'>"
						+"<input style='display:none' type='number' value='"+paid+"' class='form-control' name='new_paid[]'>"
						+"<input style='display:none' type='text' value='"+maxPay+"' class='form-control' name='new_max_pay[]'>"
						+"<input style='display:none' type='text' value='"+typeName+"' class='form-control' name='new_type[]'>"
						+"<span>Telah dibayar <a style='color: orange'>"+torp(paid)+"</a> pada invoice lain dari tagihan "+ torp(price) +"</span><br>"
						+"<span id='response-pay"+priceId+"' class='response-error'></span>"
					+"</div>"
					+"<div class='col-md-1'>"
						+"<button type='button' onclick='removePay("+'"'+priceId+'"'+")' class='btn btn-xs btn-danger'>X</button>"
					+"</div>"
				+"</div>"
			);

			$("#price-"+priceId).addClass('active-type');
			
			$('#new_pay-'+priceId).maskMoney({
				prefix: 'Rp. ',
				thousands: '.',
				decimal: ',',
				precision: 0
			});
			
		}
	}

	function removePay(priceId){
        $(".group-pay"+priceId).remove();
        $("#price-"+priceId).removeClass('active-type');

        var index = this.price_id.indexOf(priceId);
 
        if (index > -1) {
            this.price_id.splice(index, 1);
        }
	}
	
	function removePaid(paymentId, payId, paid){
		let url = '<?= base_url() ?>transaction/remove/'+paymentId+'/'+payId+'/'+paid;
		let type = 'post';
		postJson(url, type, data = {}, function (err, response) {
			if (response.status == 'update') {
				location.reload();
			}else{
				window.location = '<?= base_url('transaction') ?>';
			}
		});
        
	}

	function editPayAmount(paymentId) {
        $("#md-modal").modal('show');
		$(".modal-title").html("Info");
		let url = "<?= base_url() ?>transaction/edit_pay_amount/"+paymentId;
		loadView(url, ".modal-custom");
	}

	
    function paymentPrint(pageURL) {
        var left = (screen.width/2)-(800/2);
        var top = (screen.height/2)-(550/2);
        var targetWin = window.open (pageURL, 'Cetak Bukti Pembayaran', 
            'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+800+', height='+550+', top='+top+', left='+left);
    }

</script>

<style>
	#transaction-table_filter {
		display: none;
	}
</style>