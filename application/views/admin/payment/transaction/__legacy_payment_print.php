<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?= $title ?></title>

	<link rel="icon" href="<?= base_url() ?>assets/admin/images/yaperi_02.png" type="image/x-icon" />

	<!-- Bootstrap -->
	<link href="<?= base_url() ?>assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
	<div class="container">
		<div class="kotak row">
            <div class="col-sm-3 col-xs-3">
                <img src="<?= base_url() ?>assets/admin/images/yaperi_02.png" alt="" width="100" height="100">
            </div>

            <div class="col-sm-9 col-xs-9" id="header">
                <p>YAYASAN PENDIDIKAN AR-RIDHO</p>
                <p>SEKOLAH TINGGI AGAMA ISLAM (STAI), YAPERI CIBINONG</p>
                <p id="address"> JL. Raya Jakarta Bogor KM 45 Pekansari Cibinong Bogor 16915</p>
                <p id="address"> Telp. (021) 87910944 | E-mail : staiyaperi@gmail.com </p>
            </div>
		</div>

        <hr>

        <div class="row">
            <div class="col-sm-12 col-xs-12 text-center">
                <p>BUKTI PEMBAYARAN UANG KULIAH</p>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 col-xs-6">
                <table>
                    <tr>
                        <td>Nama Mahasiswa</td>
                        <td>:</td>
                        <td><?= $payment->student_name ?></td>
                    </tr>
                    <tr>
                        <td>Nomor Induk</td>
                        <td>:</td>
                        <td><?= $payment->reg_number ?></td>
                    </tr>
                    <tr>
                        <td>Program Studi</td>
                        <td>:</td>
                        <td>S1/<?= $payment->major_name ?></td>
                    </tr>
                    <tr>
                        <td>Semester</td>
                        <td>:</td>
                        <td><?= $payment->semester_id ?></td>
                    </tr>
                    <tr>
                        <td>Angkatan</td>
                        <td>:</td>
                        <td><?= $payment->generation_name ?></td>
                    </tr>
                    
                </table>
                
            </div>

            <div class="col-sm-6 col-xs-6">
                <table>
                    <tr>
                        <td>No. Kwitansi</td>
                        <td></td>
                        <td style="margin-left: 50px" class="pull-right"><?= $payment->inv_number ?></td>
                    </tr>
                    <tr>
                        <td>Untuk Pembayaran</td>
                    </tr>
                    <?php foreach ($prices as $price) { 
                        $key = array_search($price->type_id, array_column($pays, 'type_id'));
                        if(is_numeric($key)){
                            if($pays[$key]['installment'] > 0){
                                foreach ($pays as $pay) { 
                                    if($pay['type_id'] == $price->type_id){?>
                                        <tr>
                                            <td><?= $price->type_name." (Bulan #".$pay['installment'].")" ?></td>
                                            <td></td>
                                            <td style="margin-left: 50px" class="pull-right"><?= toRp($pay['pay']) ?></td>
                                        </tr>
                            <?php } } } else{ ?>
                                <tr>
                                    <td><?= $price->type_name ?></td>
                                    <td></td>
                                    <td style="margin-left: 50px" class="pull-right"><?= toRp($pays[$key]['pay']) ?></td>
                                </tr>
                            <?php } ?>
                    <?php }else{ ?>
                        <tr>
                            <td><?= $price->type_name ?></td>
                            <td></td>
                            <td style="margin-left: 50px" class="pull-right"><?= toRp(0) ?></td>
                        </tr>
                    <?php } } ?>
                </table>
            </div>

        </div>
        <br>
        <p>Bekasi, <?= toIndoDatetime($payment->created_at) ?></p>
        <hr>

        <div class="row">
            <div class="col-sm-6 col-xs-6">
                <p><i>Bukti struk pembayaran yang sah</i></p>
                <p><i>Mohon disimpan</i></p>
            </div>
            <div class="col-sm-6 col-xs-6">
                <table>
                    <tr>
                        <td>Total Tagihan</td>
                        <td></td>
                        <td style="margin-left: 110px" class="pull-right"><?= toRp($payment->bill_amount) ?></td>
                    </tr>
                    <tr>
                        <td>Total Bayar</td>
                        <td></td>
                        <td style="margin-left: 110px" class="pull-right"><?= toRp($payment->pay_amount) ?></td>
                    </tr>
                   
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-offset-6">
                <p><i><?= ucwords(spelling($payment->pay_amount)) ?></i></p>
            </div>
        </div>

        <a class="btn btn-xs btn-primary" id="btn-print" onclick="printThis()"><i class="fa fa-print"> Cetak</i></a>
	</div>
</body>

<!-- jQuery -->
<script src="<?= base_url() ?>assets/admin/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?= base_url() ?>assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

<script>

</script>

<style>
    .kotak {
         margin-top: 20px;
    }

    img {
        margin-left: 60px;
    }

    .line {
        border: 1px solid black;
    }

    #header {
        margin-top: 14px;
    }

    #address {
        font-size: 12px;
    }

    p {
        font-weight: bold;
        margin-bottom: 3px;
        font-family: "Times New Roman", Times, serif;
    }

    td {
        font-family: "Times New Roman", Times, serif;
        font-weight: bold;
        padding: 5px 5px 0px 5px;
    }

    hr {
        border-top: 1px solid #000 !important;
        margin-top: 10px !important;
        margin-bottom: 5px !important;
    }

    @media print
    {    
        #btn-print
        {
            display: none !important;
        }
    }
</style>

<script>
    function printThis() {
        window.print();
    }
</script>

</html>