<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?= $title ?></title>

	<link rel="icon" href="<?= base_url() ?>assets/admin/images/yaperi_02.png" type="image/x-icon" />

	<!-- Bootstrap -->
	<link href="<?= base_url() ?>assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
	<div class="container">
		<div class="kotak row">

            <div class="col-sm-7 col-xs-7" id="header">
                <p id="title_name">YAYASAN PENDIDIKAN AR-RIDHO</p>
                <p id="title_name">SEKOLAH TINGGI AGAMA ISLAM (STAI), YAPERI CIBINONG</p>
                <p id="address"> JL. Raya Jakarta Bogor KM 45 Pekansari Cibinong Bogor 16915</p>
                <p id="address"> Telp. (021) 837-137-49 / E-mail : stai.yaperi.cbg@gmail.com </p>
            </div>

            <div class="col-sm-5 col-xs-5">
                <table class="text-right pull-right">
                    <tr>
                        <td id="text-1">Tanggal Pembayaran</td>
                        <td id="text-1">:</td>
                        <td id="text-1"><?= toIndoDatetime($payment->created_at) ?></td>
                    </tr>
                    <tr>
                        <td id="text-1">Nomor Invoice</td>
                        <td id="text-1">:</td>
                        <td id="text-1"><?= $payment->inv_number ?></td>
                    </tr>
                    <tr>
                        <td id="text-1">NIM</td>
                        <td id="text-1">:</td>
                        <td id="text-1"><?= $payment->reg_number ?></td>
                    </tr>
                    <tr>
                        <td id="text-1">Semester</td>
                        <td id="text-1">:</td>
                        <td id="text-1"><?= str_replace('Semester-', '', $payment->semester_alias).'/S1' ?></td>
                    </tr>
                </table>
            </div>
		</div>
        
        <p class="pull-right" id="text-kwitansi">KWITANSI</p>

        <hr>

        <div class="row">
            <div class="col-sm-5 col-xs-5">
            <br>
                <table>
                    <tr>
                        <td id="text-1">Telah diterima dari</td>
                        <td id="text-1">:</td>
                        <td id="text-1"><?= $payment->student_name ?></td>
                    </tr>
                    <tr>
                        <td id="text-1">Sejumlah uang</td>
                        <td id="text-1">:</td>
                        <td id="text-1"><?= toRp($payment->pay_amount) ?></td>
                    </tr>
                </table>
            </div>
            <div class="col-sm-7 col-xs-7">
                <div id="kotak-spell">
                    <?= ucwords(spelling($payment->pay_amount)) ?> Rupiah
                </div>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-sm-12 col-xs-12" id="paylist">
                <table style="width: 100%">
                    <thead>
                        <th width="10%" id="text-1">No</th>
                        <th width="70%" id="text-1">Keterangan</th>
                        <th width="20%" id="text-1" class="text-right">Jumlah</th>
                    </thead>
                    <tbody>
                        <?php $no = 1; foreach ($pays as $pay) { ?>
                            <tr>
                                <td id="text-1"><?= $no++; ?></td>
                                <td id="text-1"><?= ($pay['installment'] > 0) ? $pay['type_name']." (Bulan #".$pay['installment'].")" : $pay['type_name'] ?></td>
                                <td id="text-1" class="text-right"><?= toRp($pay['pay']) ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <hr>

        <div class="row">
            <div class="col-sm-4 col-xs-4">
                <p id="note">* Bukti pembayaran yang sah</p>
                <p id="note">* Mohon untuk disimpan</p>
                <a class="btn btn-xs btn-primary" id="btn-print" onclick="printThis()"><i class="fa fa-print"> Cetak</i></a>
            </div>
            <div class="col-sm-8 col-xs-8">
               <p class="text-right">TOTAL: <?= toRp($payment->bill_amount) ?></p>
               <p class="text-right">Penerima</p>
                <br>
                <br>
                <br>
               <p class="text-right">( <?= me()['name'] ?> )</p>

            </div>
        </div>

	</div>
</body>

<!-- jQuery -->
<script src="<?= base_url() ?>assets/admin/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?= base_url() ?>assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

<script>

</script>

<style>
    #kotak-spell {
        border: 1px solid #000;
        margin-top: 3px;
        border-radius: 10px;
        border-style: dashed;
        height: 35px;
        padding: 7px;
        font-style: italic;
        font-size: 13px;
    }

    #note {
        font-style: italic
    }

    #paylist {
        min-height: 180px;
    }

    table>thead>tr>th {
        border-bottom: 1px solid #000 !important;
    }

    .kotak {
         margin-top: 10px;
    }

    img {
        margin-left: 60px;
    }

    .line {
        border: 1px solid black;
    }

    #header {
        margin-top: 14px;
    }

    #title_name {
        font-size: 13px;
    }

    #address, #text-1 {
        font-size: 13px;
    }

    #text-kwitansi {
        font-size: 26px;
    }

    p {
        font-weight: bold;
        margin-bottom: 3px;
        font-family: "Times New Roman", Times, serif;
    }

    td {
        font-family: "Times New Roman", Times, serif;
        font-weight: bold;
        padding: 5px 5px 0px 5px;
    }

    hr {
        border-top: 1px solid #000 !important;
        margin-top: 10px !important;
        margin-bottom: 5px !important;
    }

    @media print
    {    
        #btn-print
        {
            display: none !important;
        }
    }
</style>

<script>
    function printThis() {
        window.print();
    }
</script>

</html>