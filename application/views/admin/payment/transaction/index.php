<div class="page-title">
	<div class="title_left">
		<h3>Transaksi</h3>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Daftar Transaksi</h2>
					<div class="clearfix"></div>
				</div>
				<?php flash() ?>
				<div class="x_content table-responsive">
					<table id="transaction-table" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th id="th" width="7%">ID</th>
								<th id="th" width="12%">No. Invoice</th>
								<th id="th" width="10%">Angkatan</th>
								<th id="th" width="9%">Semester</th>
								<th id="th">Mahasiswa</th>
								<th id="th" width="10%">Tagihan</th>
								<th id="th" width="15%">Keterangan</th>
								<th id="th" width="10%">Tgl Bayar</th>
								<th id="th" class="no-sort" width="16%">Action</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>