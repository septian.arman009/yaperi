<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>
<form class="form-horizontal form-label-left form-data" 
        action="<?= base_url('transaction/update_pay_amount/'.$payment->payment_id) ?>"
        method="post" 
        enctype="multipart/form-data" 
        data-redirect="<?= base_url('transaction/edit/'.$payment->payment_id) ?>"
        data-rule="default_update" 
        data-btn="#btn-update">

	<div class="modal-body">

    <div class="item form-group group-pay">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Pembayaran</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" class="form-control price-modal" name="pay_amount" value="<?= toRp($payment->pay_amount)?>">
            <span id="response-pay" class="response-error"></span>
        </div>
    </div>

    <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Tagihan</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" class="form-control" name="bill_amount" value="<?= toRp($payment->bill_amount)?>" readonly>
        </div>
    </div>

	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary" id='btn-update'>Simpan</button>
	</div>

</form>

<script>
	$('.price-modal').maskMoney({
		prefix: 'Rp. ',
		thousands: '.',
		decimal: ',',
		precision: 0
	});
</script>