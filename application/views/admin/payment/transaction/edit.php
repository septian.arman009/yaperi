<div class="page-title">
	<div class="title_left">
		<h3>Transaksi <small> Edit Transaksi </small></h3>
	</div>
	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Data Transaksi</h2>
					<div class="clearfix"></div>
				</div>

				<a href="<?= base_url('transaction') ?>" class="btn btn-default">Kembali</a>

				<div class="ln_solid"></div>

				<?php flash() ?>

				<div class="row">
					<div class="col-md-8 col-sm-8 col-xs-12">
						<h4> Nomor Invoice : <b><?= $payment->inv_number ?></b></h4>
						<table>
							<tr>
								<td id="td">Tanggal Pembayaran</td>
								<td id="td">:</td>
								<td id="td"><b><?= toIndoDatetime($payment->payment_date) ?></b>
								</td>
							</tr>
							<tr>
								<td id="td">Jumlah Pembayaran</td>
								<td id="td">:</td>
								<td id="td"><b><?= toRp($payment->pay_amount) ?></b></td>
								<td><button onclick="editPayAmount('<?= $payment->payment_id ?>')" class="btn btn-xs"><i
											class="fa fa-pencil"></i></button></td>
							</tr>
							<tr>
								<td id="td">Tagihan</td>
								<td id="td">:</td>
								<td id="td"><b><?= toRp($payment->bill_amount) ?></b>
								</td>
							</tr>
							<tr>
								<td id="td">Kembalian</td>
								<td id="td">:</td>
								<td id="td"><b><?= toRp($payment->money_back) ?></b>
								</td>
							</tr>
						</table>
					</div>

					<div class="col-md-4 col-sm-4 col-xs-12">
						<h4>Daftar Pembayaran</h4>
						<ul class="list-unstyled timeline">
							<li>
								<div class="block">
									<div class="tags">
										<a href="" class="tag">
											<span><?= $payment->semester_alias ?></span>
										</a>
									</div>
									<div class="block_content">
										<?php foreach ($prices as $price) { 
											$payment_type = ($price['installment'] > 0) ? $price['type_name']." (Bulan #".$price['installment'].")" : $price['type_name']?>
											<?php if(!in_array($price['price_id'], $price_id)){ ?>

												<h2 class="title">
													<span class="pointer" id="price-<?= $price['price_id'] ?>" 
														onclick="addType(
																		'<?= $price['price_id'] ?>',
																		'<?= $price['price'] ?>',
																		'<?= $price['paid'] ?>',
																		'<?= $price['max_pay'] ?>',
																		'<?= $payment_type ?>',
																		'<?= $price['semester_name'] ?>')">
														<?= $payment_type ?>
													</span>
												</h2>

											<?php } else{ ?>

												<h2 class="title">
													<span style="color: #ccc"><?= $payment_type ?></span>
												</h2>
											
											<?php } ?>
										<?php } ?>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>

				<div class="ln_solid"></div>

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<form class="form-horizontal form-label-left form-data" novalidate=""
								action="<?= base_url('transaction/update/'. $payment_id. '/' .$student_id) ?>" method="post"
								enctype="multipart/form-data" data-rule="default_update"
								data-redirect="<?= base_url('transaction/edit/'.$payment_id. '/' .$student_id) ?>"
								data-btn="#btn-save">

							<input style="display:none" type="number" value="<?= $payment->pay_amount ?>"
								class="form-control" name="pay_amount">

							<?php foreach ($pays as $pay) { ?>

							<div class="item form-group group-pay<?= $pay['price_id'] ?>">
								<label class="control-label col-md-4 col-sm-4 col-xs-12">
									<?= $pay['type_name'] ?> ( <?= $pay['semester_name'] ?> )
								</label>

								<div class="col-md-6 col-sm-6 col-xs-10">
									<input type="text" class="form-control old_pay" name="old_pay[]"
										value="<?= toRp($pay['pay']) ?>">

									<input style="display:none" type="number" value="<?= $pay['price'] ?>"
										class="form-control" name="old_price[]">

									<input style="display:none" type="number" value="<?= $pay['price_id'] ?>"
										class="form-control" name="old_price_id[]">

									<input style="display:none" type="number" value="<?= $pay['pay_id'] ?>"
										class="form-control" name="old_pay_id[]">

									<input style="display:none" type="number" value="<?= $pay['paid'] ?>"
										class="form-control" name="old_paid[]">

									<input style="display:none" type="number" value="<?= $pay['max_pay'] ?>"
										class="form-control" name="old_max_pay[]">

									<input style="display:none" type="text" value="<?= $pay['type_name'] ?>"
										class="form-control" name="old_type[]">

									<span>
										Telah dibayar <a style="color: orange"><?= toRp($pay['paid']) ?></a> pada
											invoice lain dari tagihan <?= toRp($pay['price']) ?></span><br>
											
									<span id="response-pay<?= $pay['price_id'] ?>" class="response-error"></span>
								</div>

								<div class="col-md-1">
									<button type="button"
										onclick="removePaid('<?= $payment_id ?>', '<?= $pay['pay_id'] ?>', '<?= $pay['pay'] ?>')"
										class="btn btn-xs btn-danger">X</button>
								</div>

							</div>

							<?php } ?>

							<div id="new_pay"></div>

							<div class="form-group">
								<div class="col-md-6 col-md-offset-4">
									<button id="btn-save" type="submit" class="btn btn-success">Simpan</button>
									<a href="<?= base_url('transaction') ?>" class="btn btn-default">Kembali</a>
								</div>
							</div>

						</form>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<style>
	#td {
		padding: 5px;
	}

	.active-type {
		color: #da1a3e;
		font-size: 16px;
		font-style: italic;
	}

	ul.timeline li {
		border-bottom: none !important;
	}
</style>