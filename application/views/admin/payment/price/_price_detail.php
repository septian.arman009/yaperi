<h2>Total Tagihan <?= $generationName ?> adalah <?= toRp($billing) ?></h2>
<ul class="list-unstyled timeline">
    <?php foreach ($semesters as $semester) { ?>
	<li>
		<div class="block">
			<div class="tags">
				<a href="" class="tag">
					<span><?= $semester['semesterName'] ?></span>
				</a>
			</div>
			<div class="block_content">
				<?php if($semester['priceList']) {?>
                <?php foreach ($semester['priceList'] as $sm) { ?>
                  
					<h2 class="title">
						<span><?= ($sm['installment'] > 0) ? $sm['payment_type']." (Bulan #".$sm['installment'].")" : $sm['payment_type']?></span>
						<span class="pull-right">
							<?= toRp($sm['price']) ?>
							[ <a style="color: skyblue" onclick="edit('<?= $sm['price_id'] ?>')">Edit</a> 
								| <a style="color: orange" data-url="<?= base_url('price/destroy_one/'.$sm['price_id']) ?>" class="form-delete">Hapus</a> ]
						</span>
					</h2>

                <?php } }else { ?>
					<h2 class="title">
						<span>-</span>
					</h2>
				<?php } ?>

                <div class="byline">
					<span>Total </span> <?= toRp($semester['total']) ?>
				</div>

			</div>
		</div>
	</li>
    <?php } ?>
	<li>
		<div class="block">
			<div class="tags">
				<a onclick="paidLeave()" class="tag">
					<span>Cuti</span>
				</a>
			</div>
			<div class="block_content">
				<h2 class="title">
					<span><?= toRp($paidLeave) ?></span>
				</h2>

				<div class="byline">
					<span>Total </span>  Pembayaran Cuti Mahasiswa</div>

			</div>
		</div>
	</li>
</ul>