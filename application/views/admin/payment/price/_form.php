<div class="item form-group group-generation_id">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Angkatan</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="generation_id" id="generation_id" onchange="typeCheck()" class="form-control">
			<?php
				foreach ($generations as $generation) {
					if(!is_null($generationId) && $generation->generation_id == $generationId){
						echo "<option selected value='$generation->generation_id'>$generation->generation_name</option>";
					}else{
						echo "<option value='$generation->generation_id'>$generation->generation_name</option>";
					}
                }
			?>
		</select>
	</div>
</div>

<div class="item form-group group-semester_id">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Semester</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="semester_id" id="semester_id" onchange="typeCheck()" class="form-control">
			<?php
				foreach ($semesters as $semester) {
                    echo "<option value='$semester->semester_id'>$semester->semester_name</option>";
                }
			?>
		</select>
	</div>
</div>

<div class="item form-group group-type_id">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Pembyaran</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="type_id" id="type_id" class="form-control" required="required">
			<option value="">- Jenis Pembayaran -</option>
		</select>
		<span id="response-type_id" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-price">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Harga</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input type="text" class="form-control" name="price" id="price" value="Rp. 0">
		<span id="response-price" class="response-error"></span>
	</div>
</div>
<div class="item form-group group-price">
	<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	<div class="col-md-6 col-sm-6 col-xs-12">
	<input type="checkbox" name="installment" id="checkbox"> Jadikan Cicilan</div>
</div>

<div class="form-group">
	<div class="col-md-6 col-md-offset-3">
		<button id="btn-save" type="submit" class="btn btn-success">Simpan</button>
	</div>
</div>