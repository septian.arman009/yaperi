<div class="page-title">
	<div class="title_left">
		<h3>Harga</h3>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Daftar Harga Perangkatan</h2>
					<div class="clearfix"></div>
				</div>
				<?php flash() ?>
				<a href="<?= base_url('price/create') ?>" class="btn btn-default">Atur Harga</a>
				<hr>
				<div class="x_content table-responsive">
					<table id="price-table" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th id="th" width="10%">ID</th>
								<th id="th">Angkatan</th>
								<th id="th">Total Harga</th>
								<th id="th">Dibuat</th>
								<th id="th">Update Terakhir</th>
								<th id="th" class="no-sort" width="10%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">ID</th>
								<th class="footer">Angkatan</th>
								<th class="footer">Total Harga</th>
								<th class="footer">Dibuat</th>
								<th class="footer">Update Terakhir</th>
								<th class="footer"></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>