<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>
<form class="form-horizontal form-label-left form-data" 
        action="<?= base_url('price/store_leave/'.$generationId) ?>"
        method="post" 
        enctype="multipart/form-data" 
        data-rule="custom_update" 
        data-btn="#btn-update">

	<div class="modal-body">


		<div class="item form-group">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<input type="text" class="form-control" value="<?= $generation ?>" readonly>
			</div>
		</div>

		<div class="item form-group group-price_leave">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<input type="text" class="form-control" name="price_leave" id="price_leave" value="<?= toRp($price) ?>">
				<span id="response-price_leave" class="response-error"></span>
			</div>
		</div>

	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary" id='btn-update'>Simpan</button>
	</div>

</form>

<script>
	$('#price_leave').maskMoney({
		prefix: 'Rp. ',
		thousands: '.',
		decimal: ',',
		precision: 0
	});
</script>