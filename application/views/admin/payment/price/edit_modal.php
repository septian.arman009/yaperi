<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>
<form class="form-horizontal form-label-left form-data" 
        action="<?= base_url('price/update/'.$price_id) ?>"
        method="post" 
        enctype="multipart/form-data" 
        data-rule="custom_update" 
        data-btn="#btn-update">

	<div class="modal-body">


		<div class="item form-group">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<input type="text" class="form-control" value="<?= $generation ?>" readonly>
			</div>
		</div>

		<div class="item form-group">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<input type="text" class="form-control" value="<?= $semester ?>" readonly>
			</div>
		</div>

		<div class="item form-group">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<input type="text" class="form-control" value="<?= $type ?>" readonly>
			</div>
		</div>

		<div class="item form-group group-price_edit">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<input type="text" class="form-control" name="price_edit" id="price_edit" value="<?= $price ?>">
				<span id="response-price_edit" class="response-error"></span>
			</div>
		</div>

	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary" id='btn-update'>Simpan</button>
	</div>

</form>

<script>
	$('#price_edit').maskMoney({
		prefix: 'Rp. ',
		thousands: '.',
		decimal: ',',
		precision: 0
	});
</script>