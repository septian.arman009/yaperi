<!-- validator -->
<script src="<?= base_url() ?>assets/admin/vendors/validator/validator.js"></script>
<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>
<script src="<?= base_url() ?>assets/admin/js/jquery.maskMoney.js"></script>
<script>
	$(document).ready(function () {
		if (document.body.contains(document.getElementById('price-table'))) {
			$('#price-table tfoot th').each(function () {
				var title = $(this).text();
				if (title != '')
					var inp = '<input type="text" class="form-control footer-s" placeholder="' + title +
						'" id="' + title + '" />';
				$(this).html(inp);
			});

			var table = $('#price-table').DataTable({
				"processing": true,
				"serverSide": true,
				"order": [
					[1, 'desc']
				],
				"ajax": {
					"url": '<?= base_url('price/table') ?>',
					"type": "POST"
				}
			});

			table.columns().every(function () {
				var that = this;
				$('input', this.footer()).on('keyup change', function () {
					if (that.search() !== this.value) {
						that.search(this.value).draw();
					}
				});
			});
		}else{
			$('#price').maskMoney({
				prefix: 'Rp. ',
				thousands: '.',
				decimal: ',',
				precision: 0
			});

			typeCheck();
		}

	});

	function typeCheck(){
		let generation_id = $("#generation_id").val();
		let semester_id = $("#semester_id").val();
		let url = "<?= base_url() ?>price/type_check/"+generation_id+"/"+semester_id;
		loadView(url, '#type_id');

		let url_price = "<?= base_url() ?>price/price_detail/"+generation_id;
		loadView(url_price, '#price-detail');
	}

	function edit(price_id){
		$("#md-modal").modal('show');
		$(".modal-title").html("Edit Harga");
		let url = "<?= base_url() ?>price/edit_modal/"+price_id;
		loadView(url, '.modal-custom');
	}

	function custom_response($form, rule, message) {
		if(rule == 'custom_store'){
			typeCheck();
			notify('Sukses', message, 'success');
			$("#price").val("Rp. 0");
		}else if(rule == 'custom_update'){
			typeCheck();
			notify('Sukses', message, 'success');
			$("#md-modal").modal('hide');
		}else if(rule == 'custom_delete'){
			typeCheck();
			notify('Sukses', message, 'success');
		}
		
	}

	function paidLeave(){
		$("#md-modal").modal('show');
		$(".modal-title").html("Harga Cuti");
		let generation_id = $("#generation_id").val();
		let url = "<?= base_url() ?>price/paid_leave/"+generation_id;
		loadView(url, '.modal-custom');
	}
</script>

<style>
	#price-table_filter {
		display: none;
	}
</style>