<?php if(count($students) > 0){ ?>
	<ul class="list-unstyled top_profiles scroll-view">
		<? foreach ($students as $student) { ?>

			<li class="media event">
				<a class="pull-left border-aero profile_thumb">
					<i class="fa fa-user aero"></i>
				</a>
				<div class="media-body">
					<a class="title" onclick="formPayment('<?= base_url('pay/form_payment/'.$student->student_id) ?>')"><?= $student->student_name ?></a>
					<p><strong><?= $student->reg_number ?></strong></p>
					<p> <small><?= $student->semester_alias ?></small></p>
					<p> <small><?= $student->major_name ?></small></p>
					<p> <small><?= $student->gender ?></small></p>
					<p> <small>Tahun Masuk : <?= $student->join_year ?></small></p>
					<p> <small>Tahun Lulus : <?= $student->graduate_year ?></small></p>
				</div>
			</li>

		<?php } ?>
	</ul>

<?php } else { ?>

	<ul class="list-unstyled top_profiles scroll-view">

		<li class="media event">
			<a class="pull-left border-aero profile_thumb">
				<i class="fa fa-user aero"></i>
			</a>
			<div class="media-body">
				<a class="title" style="color: red"> Nama: TIDAK ADA DATA YANG COCOK</a>
				<p><strong>NIM: </strong></p>
				<p> <small>Semester: </small></p>
				<p> <small>Jurusan:</small></p>
				<p> <small>Jenis Kelamin:</small></p>
				<p> <small>Tahun Masuk : </small></p>
				<p> <small>Tahun Lulus : </small></p>
			</div>
		</li>

	</ul>

<?php } ?>

<style>
	td {
		padding: 5px;
	}

	.name {
		font-size: 13px;
	}
</style>

<script>
	$("#btn-go").html('Go!');
	$("#btn-go").removeAttr('disabled', 'disabled');
	const mahasiswa = '<?= count($students) ?>';
	$("#info").html(`Daftar Mahasiswa (${mahasiswa}  ditemukan)`);
</script>