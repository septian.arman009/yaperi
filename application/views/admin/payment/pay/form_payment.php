<div class="page-title">
	<div class="title_left">
		<h3>Pembayaran</h3>
	</div>
	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Daftar Pembayaran</h2>
					<div class="clearfix"></div>
				</div>

				<h2>Total Tagihan <?= $generationName ?> adalah <?= toRp($billing) ?></h2>

				<div class="custom-scroll">
					<ul class="list-unstyled timeline">
						<?php foreach ($semesters as $semester) {
							
							if($semester['leave'] == 'yes'){ ?>

						<li>
							<div class="block">
								<div class="tags">
									<a class="tag">
										<span><?= $semester['semesterName'] ?></span>
									</a>
								</div>
								<div class="block_content">

									<div class="byline">
										<span style="color: green; font-weight: bold">Sisa tagihan : 0 </span>
									</div>

									<div class="byline">
										<span style="color: blue; font-weight: bold">Pembayaran Cuti :
											<?= $semester['price'] ?> </span> Tgl : <?= $semester['created_at'] ?>
									</div>
									<div class="byline">
										<span>
											<a onclick="leavePrint('<?= base_url('transaction/leave_print/'.$semester['leave_id']) ?>')"
												class="btn btn-xs btn-default"><i class="fa fa-print"> Cetak Bukti
													Pembayaran</i></a>
										</span>
									</div>
								</div>
							</div>
						</li>

						<?php }else{ ?>

						<li>
							<div class="block">
								<div class="tags">
									<a class="tag">
										<span><?= $semester['semesterName'] ?></span>
									</a>
								</div>
								<div class="block_content">
									<?php if($semester['priceList']) {?>
									<?php foreach ($semester['priceList'] as $sm) { 
												$payment_type = ($sm['installment'] > 0) ? $sm['payment_type']." (Bulan #".$sm['installment'].")" : $sm['payment_type']?>

									<h2 class="title">
										<?php if($sm['pay_left']  == 0){ ?>

										<span title="Lunas"><?= $payment_type ?></span>

										<?php } else { ?>

										<span class="pointer" id="price-<?= $sm['price_id'] ?>" onclick="addType(
																				'<?= $sm['price_id'] ?>', 
																				'<?= $payment_type ?>',
																				'<?= $semester['semesterName']  ?>',
																				'<?= $sm['pay_left'] ?>',
																				'<?= $semester['semesterId'] ?>')">
											<?= $payment_type ?></span>

										<?php } ?>

										<span
											<?= ($sm['pay_left'] == 0) ? "style='color:#1abb9c;font-weight:bold;font-style:italic'" : "" ?>
											class="pull-right">
											<?= toRp($sm['price']) ?>
										</span>

										<span>
											<?php
												if($sm['pay_left'] != $sm['price']){
													if($sm['pay_left']  ==0){
														echo '<p style="font-size: 14px"> <a style="color: orange">+ '
																.toRp($sm['paid']).'</a> :: '
																.toRp($sm['pay_left']).'</p>';
													}else{
														echo '<p style="font-size: 14px"> <a style="color: orange">+ '
																.toRp($sm['paid']).'</a> :: <a style="color:red">- '
																.toRp($sm['pay_left']).'</a></p>';
													}
												}
											?>
										</span>
									</h2>

									<?php } }else { ?>

									<h2 class="title">
										<span>-</span>
									</h2>

									<?php } ?>

									<div class="byline">
										<?php if($semester['total'] == 0){ ?>
										<span style="color: green; font-weight: bold">Sisa tagihan :
											<?= toRp($semester['total']) ?> </span>
										<?php }else{ ?>
										<span style="color: red; font-weight: bold">Sisa tagihan :
											<?= toRp($semester['total']) ?> </span>
										<?php }?>
									</div>
								</div>

							</div>
						</li>
						<?php } } ?>
					</ul>
				</div>
				<hr>
				<div>
					<ul class="list-unstyled timeline">
						<li>
							<div class="block">
								<div class="tags">
									<a onclick="payLeaves('<?= $studentId ?>', '<?= $student->generation_id ?>', '<?= $length ?>')"
										class="tag">
										<span>Cuti</span>
									</a>
								</div>
								<div class="block_content">
									<h2 class="title">
										<span><?= toRp($paidLeave) ?></span>
									</h2>

									<div class="byline">
										<span>Total </span> Pembayaran Cuti Mahasiswa</div>
								</div>
							</div>
						</li>
					</ul>
				</div>

			</div>
		</div>

		<div class="col-md-6 col-sm-6 col-xs-12">
			<?php flash() ?>
			<div class="x_panel">
				<div class="x_title">
					<h2>Form Pembayaran</h2>
					<div class="clearfix"></div>
				</div>

				<form class="form-horizontal form-label-left form-data" novalidate=""
					action="<?= base_url('pay/store_pay/'.$studentId) ?>" method="post" enctype="multipart/form-data"
					data-rule="default_update" data-redirect="<?= base_url('pay/form_payment/'.$studentId) ?>"
					data-btn="#btn-save">

					<div id="form-list">
						<div class="item form-group group-student_pay">
							<label class="control-label col-md-6 col-sm-6 col-xs-12">Bayar : </label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input onkeyup="moneyBack()" type="text" class="form-control" name="student_pay"
									id="student_pay" value="Rp. 0">
								<span id="response-student_pay" class="response-error"></span>
							</div>
						</div>

						<div class="item form-group">
							<label class="control-label col-md-6 col-sm-6 col-xs-12">Total Tagihan : </label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="text" class="form-control" name="bill" id="bill" readonly value="Rp. 0">
							</div>
						</div>

						<div class="item form-group">
							<label class="control-label col-md-6 col-sm-6 col-xs-12">Kembalian : </label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="text" class="form-control" name="money_back" id="money_back" value="Rp. 0"
									readonly>
							</div>
						</div>

						<div class="item form-group group-created_at">
							<label class="control-label col-md-6 col-sm-6 col-xs-12">Tgl Pembayaran : </label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input requried="requried" data-inputmask="'mask': '99/99/9999'" type="text"
									class="form-control" name="created_at" id="created_at" value="<?= date('d/m/Y') ?>">
								<span id="response-created_at" class="response-error"></span>
							</div>
						</div>

						<div class="ln_solid"></div>

						<input style="display: none" id="semester_id" name="semester_id" type="number">
						<input style="display: none" id="semester_alias" name="semester_alias" type="text">

						<div class="item form-group group-photo">
							<label class="control-label col-md-6 col-sm-6 col-xs-12">Foto</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input id="photo" class="form-control col-md-6 col-sm-6 col-xs-12" name="photo"
									type="file">
								<span id="response-photo" class="response-error"></span>
							</div>
						</div>

					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-6">
							<button id="btn-save" type="submit" class="btn btn-success">Simpan</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>

	<div class="x_panel">
		<div class="x_title">
			<h2>Data Mahasiswa</h2>
			<div class="clearfix"></div>
		</div>

		<div class="col-md-3 col-sm-3 col-xs-12 widget widget_tally_box">
			<div class="x_panel fixed_height_410">
				<div class="x_content">

					<div class="flex">
						<ul class="list-inline widget_profile_box">
							<li>
								<a>
									<i onclick="info('<?= $student->mobile ?>')" title="<?= $student->mobile ?>"
										class="fa fa-phone"></i>
								</a>
							</li>
							<li>
								<img src="<?= base_url('assets/admin/images/user.png') ?>" alt="..."
									class="img-circle profile_img">
							</li>
							<li>
								<a>
									<i onclick="info('<?= $student->email ?>')" title="<?= $student->email ?>"
										class="fa fa-envelope"></i>
								</a>
							</li>
						</ul>
					</div>

					<h4 class="name"><b><?= $student->student_name ?></b></h4>

					<div class="flex">
						<ul class="list-inline count2">
							<li>
								<h3><?= str_replace("Semester-", "", $student->semester_alias) ?></h3>
								<span>Semester</span>
							</li>
							<li>
								<h3></h3>
								<span></span>
							</li>
							<li>
								<h3><?= $student->join_year ?></h3>
								<span>Masuk</span>
							</li>
						</ul>
					</div>

					<table>
						<tr>
							<td id="td">NIM</td>
							<td id="td">:</td>
							<td id="td"><b><?= $student->reg_number ?></b></td>
						</tr>
						<tr>
							<td id="td">Jurusan</td>
							<td id="td">:</td>
							<td id="td"><b><?= $student->major_name ?></b></td>
						</tr>
						<tr>
							<td id="td">Angkatan</td>
							<td id="td">:</td>
							<td id="td"><b><?= $student->generation_name ?></b></td>
						</tr>
						<tr>
							<td id="td">Tahun Lulus</td>
							<td id="td">:</td>
							<td id="td"><b><?= ($student->graduate_year == 0) ? '-' : $student->graduate_year ?></b>
							</td>
						</tr>
					</table>

				</div>
			</div>
		</div>

		<div class="col-md-9 col-sm-9 col-xs-12">

			<table>
				<tr>
					<td id="td1">Jenis Kelamin</td>
					<td id="td1">:</td>
					<td id="td1"><b><?= $student->gender ?></b></td>
				</tr>
				<tr>
					<td id="td1">TTL</td>
					<td id="td1">:</td>
					<td id="td1"><b><?= $student->birth_place.', '.toIndoDate($student->birth_date) ?></b>
					</td>
				</tr>
				<tr>
					<td id="td1">Agama</td>
					<td id="td1">:</td>
					<td id="td1"><b><?= $student->religion ?></b></td>
				</tr>
				<tr>
					<td id="td1">Alamat</td>
					<td id="td1">:</td>
					<td id="td1"><b><?= $student->street.' RT'.$student->rtrw.' Kel.'
										.$student->village.' Kec.'.$student->district.', '
										.$student->regency.', '.$student->province.' '.$student->zipcode ?></b>
					</td>
				</tr>
				<tr>
					<td id="td1">Handphone</td>
					<td id="td1">:</td>
					<td id="td1"><b><?= $student->mobile ?></b>
					</td>
				</tr>
				<tr>
					<td id="td1">Rumah</td>
					<td id="td1">:</td>
					<td id="td1"><b><?= $student->phone ?></b>
					</td>
				</tr>
				<tr>
					<td id="td1">Email</td>
					<td id="td1">:</td>
					<td id="td1"><b><?= $student->email ?></b>
					</td>
				</tr>
			</table>
		</div>

	</div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Detail Pembayaran</h2>
					<div class="clearfix"></div>
				</div>

				<div class="row">
					<div class="col-md-3 col-sm-3 col-xs-12">
						<select id="year" class="form-control" onchange="changeYear()">
							<?php 
								for ($i=1980; $i <= 2099 ; $i++) { 
									$selected = ($i == $year) ? 'Selected' : '';
									echo "<option $selected value='$i'>$i</option>";
								}
							?>
						</select>
					</div>
				</div>

				<div class="ln_solid"></div>

				<div class="row">
					<div class="col-md-3 col-sm-3 col-xs-12">
						<div id="semester-graph" style="width:100%; height:250px;"></div>
					</div>

					<div class="col-md-9 col-sm-9 col-xs-12">
						<div id="month-graph" style="width:100%; height:280px;"></div>
					</div>
				</div>

				<div class="ln_solid"></div>

				<div class="x_content table-responsive">

					<table id="pay-list-table" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th id="th" width="7%">ID</th>
								<th id="th" width="12%">No. Invoice</th>
								<th id="th" width="10%">Angkatan</th>
								<th id="th" width="9%">Semester</th>
								<th id="th">Mahasiswa</th>
								<th id="th" width="10%">Tagihan</th>
								<th id="th" width="15%">Keterangan</th>
								<th id="th" width="10%">Tgl Bayar</th>
								<th id="th" class="no-sort" width="16%">Action</th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>

</div>

<style>
	.active-type {
		color: #da1a3e;
		font-size: 18px;
		font-style: italic;
	}

	#td {
		padding: 5px;
	}

	#td1 {
		padding: 5px;
	}

	#pay-list-table_filter {
		display: none;
	}

	.timeline .tags {
		width: 100px !important;
	}

	.timeline .block {
		margin: 0 0 0 120px !important;
	}
</style>