<script src="<?= base_url() ?>assets/admin/vendors/raphael/raphael.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendors/morris.js/morris.min.js"></script>
<div id="month-graph" style="width:100%; height:280px;"></div>

<script>
    Morris.Bar({
        element: 'month-graph',
        data: <?= json_encode($paymentGraph) ?>,
        xkey: ['month'],
        ykeys: ['pay'],
        labels: ['Total Pembayaran'],
        barRatio: 0.4,
        barColors: ['#26B99A'],
        xLabelAngle: 30,
        hideHover: 'auto',
        resize: true
    });
</script>