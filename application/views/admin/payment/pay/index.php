<div class="page-title">
	<div class="title_left">
		<h3>Bayar</h3>
	</div>

	<div class="title_right">
		<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
			<div class="input-group">
				<input type="text" class="form-control" placeholder="Nama Mahasiswa..." id="search">
				<span class="input-group-btn">
					<button class="btn btn-default" id="btn-go" onclick="search()" type="button">Go!</button>
				</span>
			</div>
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2 id='info'>Daftar Mahasiswa</h2>
					<div class="clearfix"></div>
				</div>

				<div class="custom-scroll">
				<ul class="list-unstyled top_profiles scroll-view" id='student-list'>

					<li class="media event">
						<a class="pull-left border-aero profile_thumb">
							<i class="fa fa-user aero"></i>
						</a>
						<div class="media-body">
							<a class="title">Nama:</a>
							<p><strong>NIM: </strong></p>
							<p> <small>Semester: </small></p>
							<p> <small>Jurusan:</small></p>
							<p> <small>Jenis Kelamin:</small></p>
							<p> <small>Tahun Masuk : </small></p>
							<p> <small>Tahun Lulus : </small></p>
						</div>
					</li>

				</ul>
				</div>

			</div>
		</div>
	</div>
</div>

<style>
	td {
		padding: 5px;
	}

	.name {
		font-size: 13px;
	}
</style>