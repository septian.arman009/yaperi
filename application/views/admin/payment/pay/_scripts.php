<!-- validator -->
<script src="<?= base_url() ?>assets/admin/vendors/validator/validator.js"></script>
<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>
<script src="<?= base_url() ?>assets/admin/js/jquery.maskMoney.js"></script>
<script src="<?= base_url() ?>assets/admin/vendors/raphael/raphael.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendors/morris.js/morris.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>

<script>
    $(document).ready(function(){
        if (document.body.contains(document.getElementById('student-list'))) {
        	// search();
        } else {
        	$('#student_pay').maskMoney({
        		prefix: 'Rp. ',
        		thousands: '.',
        		decimal: ',',
        		precision: 0
        	});

        	Morris.Donut({
        		element: 'semester-graph',
                data: <?= json_encode($semesterPrice) ?>,
                colors: <?= json_encode($color) ?>,
        		formatter: function (y) {
        			return torp(y);
        		},
        		resize: true
            });
                        
        	$('#pay-list-table thead th').each(function () {
        		var title = $(this).text();
        		if (title != '')
        			var inp = '<textarea type="text" class="form-control footer-s" placeholder="' + title +
        				'" id="' + title + '" />';
        		$(this).html(inp);
        	});

        	var table = $('#pay-list-table').DataTable({
        		"processing": true,
        		"serverSide": true,
        		"order": [
        			[1, 'desc']
        		],
        		"ajax": {
        			"url": '<?= base_url('pay/pay_list/table/'.$studentId) ?>',
        			"type": "POST"
        		}
        	});

        	table.columns().every(function () {
        		var that = this;
        		$('textarea', this.header()).on('keyup change', function () {
        			if (that.search() !== this.value) {
        				that.search(this.value).draw();
        			}
        		});
            });

            changeYear();

            $('#created_at').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            
            $(":input").inputmask();
        }

        
    });

    this.semester = 'new';
    this.price_id = [];
    
    function changeYear() {
        let year = $("#year").val();
        loadView('<?= base_url() ?>pay/paymentGraph/<?= $studentId ?>/'+year, '#month-graph');
    }

    function search(){
        $("#btn-go").html('Loading..');
        $("#btn-go").attr('disabled', 'disabled');
        $.ajax({
            url: "<?= base_url() ?>pay/search/",
            type: 'post',
            data: {
                search: $("#search").val()
            },
            success: function (response) {
                $("#student-list").html(response);
            },
            error: function (err) {
                alert(err);
            }
        });
    }

    $("#search").on("change", function(){
        search();
    });

    function info(message) {
        $("#md-modal").modal('show');
		$(".modal-title").html("Info");
		$(".modal-custom").html("<p style='padding: 10px; font-size: 18px; text-align: center'>"+message+"</p>");
    }

    function formPayment(url) {
        window.location = url;
    }

    function custom_response($form, rule, message) {

	}

    // function addType(priceId, paymentType, semesterName, payLeft, semesterId, installment, price) {
    function addType(priceId, paymentType, semesterName, payLeft, semesterId) {

        if(this.price_id.includes(priceId)){

        }else{

            if(this.semester == 'new'){
                this.semester = semesterName;
                $("#semester_id").val(semesterId);
                $("#semester_alias").val(semesterName);
            }
            
            if(this.semester == semesterName){
                this.price_id.push(priceId);
                $("#form-list").append(
                    "<div class='item form-group group-pay"+priceId+"'>"
                        +"<label class='control-label col-md-6'>"+paymentType+" ("+semesterName+")</label>"
                        +"<div class='col-md-5'>"
                            +"<input type='text'value='Rp. 0' onkeyup='billing()' class='form-control' id='pay-"+priceId+"' name='pay[]'>"
                            // +"<input style='display:none' type='number' value='"+price+"' class='form-control' name='price[]'>"
                            // +"<input style='display:none' type='number' value='"+installment+"' class='form-control' name='installment[]'>"
                            +"<input style='display:none' type='number' value='"+payLeft+"' class='form-control' name='pay_left[]'>"
                            +"<input style='display:none' type='number' value='"+priceId+"' class='form-control' name='price_id[]'>"
                            +"<input style='display:none' type='text' value='"+paymentType+"' class='form-control' name='type[]'>"
                            +"<span id='response-pay"+priceId+"' class='response-error'></span>"
                        +"</div>"
                        +"<div class='col-md-1'>"
                            +"<button type='button' onclick='removeForm("+'"'+priceId+'"'+")' class='btn btn-xs btn-danger'>X</button>"
                        +"</div>"
                    +"</div>"
                );

                $("#price-"+priceId).addClass('active-type');
                $('#pay-'+priceId).maskMoney({
                    prefix: 'Rp. ',
                    thousands: '.',
                    decimal: ',',
                    precision: 0
                });
            }else{
                notify('Oopss..', "Pembayaran berikutnya harus dari tagihan " + this.semester, 'warning');
            }
        }
        
    }

    function removeForm(priceId){
        $(".group-pay"+priceId).remove();
        $("#price-"+priceId).removeClass('active-type');

        var index = this.price_id.indexOf(priceId);
 
        if (index > -1) {
            this.price_id.splice(index, 1);
        }

        setTimeout(() => {
            if(this.price_id.length == 0){
                this.semester = 'new';
            }
        }, 500);
      
        billing();

    }

    function billing() {
        let total = 0;
        $.each(this.price_id, function (key, value) {	

            if($("#pay-" + value).val() == ''){
                $("#pay-" + value).val('Rp. 0');
            }

            total += parseInt(moneyClean($("#pay-" + value).val()));

        });

        $("#bill").val(torp(total));
        moneyBack();
    }

    function moneyBack() {
        
        let studentPay = 0;
        let bill = 0;

        if($("#student_pay").val() != 'Rp. 0' || $("#student_pay").val() != ''){
            studentPay = moneyClean($("#student_pay").val());
        }

        if($("#bill").val() != 'Rp. 0' || $("#bill").val() != ''){
            bill = moneyClean($("#bill").val());
        }

        let moneyBack = parseInt(studentPay) - parseInt(bill);

        $("#money_back").val(torp(moneyBack));
        
    }

    function payLeaves(studentId, generationId, length) {
        $("#md-modal").modal('show');
		$(".modal-title").html("Pembayaran Cuti");
		let url = "<?= base_url() ?>pay/pay_leaves/"+studentId+"/"+generationId+"/"+length;
        loadView(url, '.modal-custom');
    }

    function paymentPrint(pageURL) {
        var left = (screen.width/2)-(800/2);
        var top = (screen.height/2)-(550/2);
        var targetWin = window.open (pageURL, 'Cetak Bukti Pembayaran', 
            'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+800+', height='+550+', top='+top+', left='+left);
    }

    function leavePrint(pageURL) {
        var left = (screen.width/2)-(800/2);
        var top = (screen.height/2)-(550/2);
        var targetWin = window.open (pageURL, 'Cetak Bukti Pembayaran Cuti', 
            'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+800+', height='+550+', top='+top+', left='+left);
    }
</script>