<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>
<form class="form-horizontal form-label-left form-data" 
        action="<?= base_url('pay/store_leaves/'.$studentId.'/'.$generationId) ?>"
        method="post" 
        enctype="multipart/form-data" 
        data-redirect="<?= base_url('pay/form_payment/'.$studentId) ?>"
        data-rule="default_update" 
        data-btn="#btn-update">

	<div class="modal-body">

    <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Semester</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <select name="semester_id" class="form-control">
                <?php foreach ($semesters as $semester) { 
                    if($semester['leave'] == 'no'){?>
                    <option value="<?= $semester['semesterId'] ?>"><?= $semester['semesterName'] ?></option>
                <?php } } ?>
            </select>
        </div>
    </div>

    <input type="text" style="display:none" id="semester_name" name="semester_name">

    <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Tarif Cuti</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="price_leave" value="<?= (!empty($leaves)) ? toRp($leaves->price) : toRp(0) ?>" class="form-control" readonly>
            <span id="response-leaveprice" class="response-error"></span>
        </div>
    </div>


	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary" id='btn-update'>Simpan</button>
	</div>

</form>

<script>
	$('.price-modal').maskMoney({
		prefix: 'Rp. ',
		thousands: '.',
		decimal: ',',
		precision: 0
	});
</script>