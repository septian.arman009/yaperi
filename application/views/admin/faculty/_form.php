<div class="item form-group group-faculty_name">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Fakultas</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($faculty)) echo $faculty['faculty_name'] ?>" class="form-control col-md-7 col-xs-12"
			name="faculty_name" required="required" type="text">
        <span id="response-faculty_name" class="response-error"></span>
	</div>
</div>

<div class="ln_solid"></div>
<div class="form-group">
	<div class="col-md-6 col-md-offset-3">
		<button id="btn-save" type="submit" class="btn btn-success">Simpan</button>
	</div>
</div>