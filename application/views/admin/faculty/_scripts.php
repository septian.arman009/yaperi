<!-- validator -->
<script src="<?= base_url() ?>assets/admin/vendors/validator/validator.js"></script>
<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>
<script>
	$(document).ready(function () {
		if (document.body.contains(document.getElementById('faculty-table'))) {
			$('#faculty-table tfoot th').each(function () {
				var title = $(this).text();
				if (title != '')
					var inp = '<input type="text" class="form-control footer-s" placeholder="' + title +
						'" id="' + title + '" />';
				$(this).html(inp);
			});

			var table = $('#faculty-table').DataTable({
				"processing": true,
				"serverSide": true,
				"order": [
					[0, 'desc']
				],
				"ajax": {
					"url": '<?= base_url('faculty/table') ?>',
					"type": "POST"
				}
			});

			table.columns().every(function () {
				var that = this;
				$('input', this.footer()).on('keyup change', function () {
					if (that.search() !== this.value) {
						that.search(this.value).draw();
					}
				});
			});
		}
	});

	function custom_response($form, rule, message) {

	}
</script>

<style>
	#faculty-table_filter {
		display: none;
	}
</style>