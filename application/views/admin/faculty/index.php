<div class="page-title">
	<div class="title_left">
		<h3>Fakultas</h3>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Daftar Fakultas</h2>
					<div class="clearfix"></div>
				</div>
				<?php flash() ?>
				<a href="<?= base_url('faculty/create') ?>" class="btn btn-default">Tambah Data</a>
				<hr>
				<div class="x_content table-responsive">
					<table id="faculty-table" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th id="th" width="10%">ID</th>
								<th id="th">Nama</th>
								<th id="th">Dibuat</th>
								<th id="th">Update Terkahir</th>
								<th id="th" class="no-sort" width="10%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">ID</th>
								<th class="footer">Nama</th>
								<th class="footer">Dibuat</th>
								<th class="footer">Update Terakhir</th>
								<th class="footer"></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>