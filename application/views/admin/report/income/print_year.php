<?php
$this->load->library('fpdf');
class PDF extends FPDF
{
	//Page header
	function Header()
	{
		//Logo
		$this->Image('assets/admin/images/yaperi_02.png',10, 10, 25, 25);
		//Arial bold 15
		$this->SetFont('Times','B',15);
		//pindah ke posisi ke tengah untuk membuat judul
		$this->Cell(80);
		//judul
        $this->Cell(50,10,'YAYASAN PENDIDIKAN AR-RIDHO',0,0,'C');
        $this->Ln(5);
		$this->SetFont('Times','B',14);
		$this->Cell(80);
        $this->Cell(50,10,'SEKOLAH TINGGI AGAMA ISLAM (STAI), YAPERI CIBINONG',0,0,'C');
        $this->Ln(5);
		$this->SetFont('Times','',12);
		$this->Cell(80);
        $this->Cell(50,10,'Jl. Raya Jakarta Bogor KM 45 Pekansari Cibinong Bogor 16915',0,0,'C');
        $this->Ln(5);
		$this->SetFont('Times','',12);
		$this->Cell(80);
        $this->Cell(50,10,'Telp. (021) 837-137-49 / E-mail: stai.yaperi.cbg@gmail.com',0,0,'C');
		//pindah baris
		$this->Ln(20);
		//buat garis horisontal
		$this->Line(10,37,200,37);
		$this->Line(10,38,200,38);
	}
 
	//Page Content
	function Content($header, $payments, $year)
	{   
		$this->SetFont('Times','B',12);
        $this->Cell(80);
        $this->Cell(40, 15,"Laporan Pendapatan Tahun : ".$year, 0, 0, 'C');
        $this->Ln();
        
		$this->SetFont('Times','',12);
		// Column widths
        $w = array(10, 140, 40);
        // Header
        for($i=0;$i<count($header);$i++)
            $this->Cell($w[$i],10,$header[$i],1,0,'C');
        $this->Ln();
        // Data
        $no = 1;
        $total = 0;
        foreach($payments as $payment)
        {
            $total += $payment['income'];
            $this->Cell($w[0],7,$no++,1,0);
            $this->Cell($w[1],7,$payment['month'],1,0,'L');
            $this->Cell($w[2],7,str_replace('Rp. ', '', toRp($payment['income'])),1,0,'R');
            $this->Ln();
        }
            $this->Cell(150,7,'Total Pendapatan',1,0, 'R');
            $this->Cell(40,7,str_replace('Rp. ', '', toRp($total)),1,0,'R');
            $this->Ln();
        // Closing line
        $this->Cell(array_sum($w),0,'','T');
	}
 
	//Page footer
	function Footer()
	{
		//atur posisi 1.5 cm dari bawah
		$this->SetY(-15);
		//buat garis horizontal
		$this->Line(10,$this->GetY(),200,$this->GetY());
		//Arial italic 9
		$this->SetFont('Times','I',9);
		//nomor halaman
		$this->Cell(0,10,'Halaman '.$this->PageNo().' dari {nb}',0,0,'R');
	}
}
 
//contoh pemanggilan class
$pdf = new PDF();
$pdf->SetTitle('Laporan Pendapatan Tahun '.$year);

//Header
$header = array('No', 'Bulan', 'Pendapatan');
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->Content($header, $payments, $year);
$pdf->Output();
