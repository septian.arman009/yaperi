<script>
    detailReport();
    
    function detailReport() {
        $("#loading").show();
        let month = $("#month").val();
        let year = $("#year").val();
        let url = '<?= base_url() ?>report/detailReport/'+month+'/'+year;

        loadView(url, '#detail_report')
    }
</script>