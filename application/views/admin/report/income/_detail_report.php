<script src="<?= base_url() ?>assets/admin/vendors/raphael/raphael.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendors/morris.js/morris.min.js"></script>

<div class="row tile_count">
	<div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-money"></i> Pendapatan Bulan Sebelumnya</span>
		<div class="count"><?= ($prevMonth) ? toRp($prevMonth) : toRp(0) ?></div>
		<span class="count_bottom"><?= $prevName.' '.$newYear ?></span>
	</div>

	<div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
		<span class="count_top"><i class="fa fa-money"></i> Pendapatan Bulan Ini</span>
		<div class="count"><?= ($thisMonth) ? toRp($thisMonth) : toRp(0) ?></div>
		<span class="count_bottom"><?= $thisName.' '.$year ?></span>
		<span class="count_bottom">

			<?php if($thisMonth > $prevMonth){ 
					if($prevMonth > 0){
						$sisa = ($thisMonth - $prevMonth);
						if($sisa > 0){
							$percent = round(($sisa / $prevMonth) * 100);
						}
					}else{
						$percent = '> 100';
					}?>
			<i class="green">
				<i class="fa fa-sort-asc"></i>
				<?= $percent ?>% </i>
			<?php } else if($thisMonth < $prevMonth){ 
					if($thisMonth > 0){
						$sisa = ($prevMonth - $thisMonth);
						if($sisa > 0){
							$percent = round(($sisa / $thisMonth) * 100);
						}
					}else{
						$percent = 0;
					}?>
			<i class="red">
				<i class="fa fa-sort-desc"></i>
				<?= $percent ?>% </i>
			<?php } else { ?>
			<i class="blue">
				<i class="fa fa-sort-asc"></i>
				0% </i>
			<?php }?>
			Dari bulan sebelumnya
		</span>
	</div>
</div>
<h3>Pendapatan bulan <?= $thisName.' '.$year ?></h3>
<span class="pull-right"><a target="_blank" href="<?= base_url('print_monthly_report/'.$month.'/'.$year) ?>" class="btn btn-sx btn-success"><i class="fa fa-print"> Cetak</i></a></span>
<br>
<div id="month-graph" style="height: 350px;"></div>
<hr>
<h3>Pendapatan sepanjang tahun <?= $year.' sebesar '.toRp($yearIncome) ?></h3>
<span class="pull-right"><a target="_blank" href="<?= base_url('print_yearly_report/'.$year) ?>" class="btn btn-sx btn-warning"><i class="fa fa-print"> Cetak</i></a></span>
<br>
<div id="year-graph" style="height: 350px;"></div>

<script>
	Morris.Bar({
		element: 'month-graph',
		data: <?= json_encode($day_income) ?>,
		xkey: ['day'],
		ykeys: ['income'],
		labels: ['Total Pembayaran'],
		barRatio: 0.4,
		barColors: ['#26B99A'],
		xLabelAngle: 50,
		hideHover: 'auto',
		resize: true
	});

	Morris.Bar({
		element: 'year-graph',
		data: <?= json_encode($month_income) ?>,
		xkey: ['month'],
		ykeys: ['income'],
		labels: ['Total Pembayaran'],
		barRatio: 0.4,
		barColors: ['#f0ad4e'],
		xLabelAngle: 30,
		hideHover: 'auto',
		resize: true
	});

	$("#loading").hide();
</script>