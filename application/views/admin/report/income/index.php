<div class="page-title">
	<div class="title_left">
		<h3>Pendapatan</h3>
	</div>

	<div class="clearfix"></div>
</div>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Pendapatan Bulanan</h2>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				<form class="form-horizontal form-label-left">
					<div class="item form-group group-nisn">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Bulan</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
                        <select id="month" class="form-control" onchange="detailReport()">
                            <?php for ($i=1; $i <= 12 ; $i++) { 
                                $selected = ($i == $month) ? 'selected' : '';
                                echo "<option $selected value='$i'>".toIndoMonth($i)."</option>";
                            } ?>
                        </select>
						</div>
					</div>
					<div class="item form-group group-nisn">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Tahun</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
                        <select id="year" class="form-control" onchange="detailReport()">
                            <?php for ($i=1980; $i <= 2099 ; $i++) { 
                                $selected = ($i == $year) ? 'selected' : '';
                                echo "<option $selected value='$i'>".$i."</option>";
                            } ?>
                        </select>
						</div>
					</div>
				</form>
				<h3 id="loading" style="display: none">Loading...</h3>
				<hr>
				
				<div id="detail_report">

				</div>
			</div>
		</div>
	</div>
</div>