<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Semester </label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="input-group">
			<span class="input-group-btn">
				<button type="button" class="btn btn-default">Semester-</button>
			</span>
			<select name="semester_name" class="form-control">
			<?php
				for ($i=1; $i <= 16; $i++) { 
					if(!in_array($i, $smNumber)){
						if(!empty($semester)){
							$selected = ($i == $smPosition) ? "selected" : "";
						}else{
							$selected = "";
						}
						echo "<option $selected id='sm-$i' value='$i'>$i</option>";                        
					}
				}
			?>
			</select>
		</div>
	</div>
</div>

<div class="ln_solid"></div>
<div class="form-group">
	<div class="col-md-6 col-md-offset-3">
		<button id="btn-save" type="submit" class="btn btn-success">Simpan</button>
	</div>
</div>