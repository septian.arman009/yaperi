<!-- validator -->
<script src="<?= base_url() ?>assets/admin/vendors/validator/validator.js"></script>
<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>
<script>
	$(document).ready(function () {
		if (document.body.contains(document.getElementById('semester-table'))) {
			$('#semester-table tfoot th').each(function () {
				var title = $(this).text();
				if (title != '')
					var inp = '<input type="text" class="form-control footer-s" placeholder="' + title +
						'" id="' + title + '" />';
				$(this).html(inp);
			});

			var table = $('#semester-table').DataTable({
				"processing": true,
				"serverSide": true,
				"order": [
					[1, 'desc']
				],
				"ajax": {
					"url": '<?= base_url('semester/table') ?>',
					"type": "POST"
				}
			});

			table.columns().every(function () {
				var that = this;
				$('input', this.footer()).on('keyup change', function () {
					if (that.search() !== this.value) {
						that.search(this.value).draw();
					}
				});
			});
		}
	});

	function custom_response($form, rule, message) {
		notify('Sukses', 'Berhasil menambah semester', 'success');
		$form.trigger('reset');
		$("#sm-"+message).remove();
	}
</script>

<style>
	#semester-table_filter {
		display: none;
	}
</style>