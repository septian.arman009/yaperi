<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Angkatan </label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="input-group">
			<span class="input-group-btn">
				<button type="button" class="btn btn-default">Angkatan-</button>
			</span>
			<select <?= (!empty($generation)) ? 'disabled' : '' ?> name="generation_name" class="form-control">
				<?php
                    for ($i=1; $i < 99; $i++) { 
						if(!in_array($i, $genNumber)){
							echo "<option id='gen".$i."' value='$i'>$i</option>";                        
						}
                    }
                ?>
			</select>
		</div>
	</div>
</div>

<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Tahun </label>
	<div class="col-md-6 col-sm-6 col-xs-12">
	<select name="year" class="form-control">
		<?php
			for ($i=1980; $i < 2099; $i++) { 
				if(!in_array($i, $year)){
					if(!empty($generation)){
						$selected = ($i == $generation->year) ? "selected" : "";
					}else{
						$selected = ($i == date('Y')) ? "selected" : "";
					}
					echo "<option $selected id='gen".$i."' value='$i'>$i</option>";                        
				}
			}
		?>
	</select>
	</div>
</div>

<div class="ln_solid"></div>
<div class="form-group">
	<div class="col-md-6 col-md-offset-3">
		<button id="btn-save" type="submit" class="btn btn-success">Simpan</button>
	</div>
</div>