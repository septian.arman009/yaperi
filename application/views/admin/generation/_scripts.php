<!-- validator -->
<script src="<?= base_url() ?>assets/admin/vendors/validator/validator.js"></script>
<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>
<script>
	$(document).ready(function () {
		if (document.body.contains(document.getElementById('generation-table'))) {
			$('#generation-table tfoot th').each(function () {
				var title = $(this).text();
				if (title != '')
					var inp = '<input type="text" class="form-control footer-s" placeholder="' + title +
						'" id="' + title + '" />';
				$(this).html(inp);
			});

			var table = $('#generation-table').DataTable({
				"processing": true,
				"serverSide": true,
				"order": [
					[1, 'desc']
				],
				"ajax": {
					"url": '<?= base_url('generation/table') ?>',
					"type": "POST"
				}
			});

			table.columns().every(function () {
				var that = this;
				$('input', this.footer()).on('keyup change', function () {
					if (that.search() !== this.value) {
						that.search(this.value).draw();
					}
				});
			});
		}
	});

	function custom_response($form, rule, message) {
		notify('Sukses', 'Berhasil menambah angkatan', 'success');
		$form.trigger('reset');
		$.each(message, function (key, value) {
			$("#gen"+value).remove();
		});
		
	}
</script>

<style>
	#generation-table_filter {
		display: none;
	}
</style>