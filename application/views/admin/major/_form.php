<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Fakultas</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="faculty_id" id="faculty_id" onchange="typeCheck()" class="form-control" required="required">
        <option value="">- Pilih Fakultas -</option>
			<?php
				foreach ($faculties as $faculty) {
					if(!is_null($major) && $faculty->faculty_id == $major['faculty_id']){
						echo "<option selected value='$faculty->faculty_id'>$faculty->faculty_name</option>";
					}else{
						echo "<option value='$faculty->faculty_id'>$faculty->faculty_name</option>";
					}
                }
			?>
		</select>
	</div>
</div>

<div class="item form-group group-major_name">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Jurusan</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($major)) echo $major['major_name'] ?>" class="form-control col-md-7 col-xs-12"
			name="major_name" required="required" type="text">
        <span id="response-major_name" class="response-error"></span>
	</div>
</div>


<div class="ln_solid"></div>
<div class="form-group">
	<div class="col-md-6 col-md-offset-3">
		<button id="btn-save" type="submit" class="btn btn-success">Simpan</button>
	</div>
</div>
