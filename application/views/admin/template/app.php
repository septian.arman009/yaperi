<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?= $title ?></title>

	<link rel="icon" href="<?= base_url() ?>assets/admin/images/yaperi_02.png" type="image/x-icon" />

	<!-- Bootstrap -->
	<link href="<?= base_url() ?>assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>assets/admin/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?= base_url() ?>assets/admin/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- NProgress -->
	<link href="<?= base_url() ?>assets/admin/vendors/nprogress/nprogress.css" rel="stylesheet">
	<!-- jQuery custom content scroller -->
	<link href="<?= base_url() ?>assets/admin/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css"
		rel="stylesheet" />
	<!-- PNotify -->
	<link href="<?= base_url() ?>assets/admin/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
	<link href="<?= base_url() ?>assets/admin/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
	<link href="<?= base_url() ?>assets/admin/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
	<!-- Custom Theme Style -->
	<link href="<?= base_url() ?>assets/admin/build/css/custom.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>assets/admin/css/dataTables.bootstrap.min.css" rel="stylesheet">
	<!-- Sweetalert2 -->
	<link href="<?= base_url() ?>assets/admin/vendors/sweetalert/sweetalert.css" rel="stylesheet">
</head>

<body class="<?= (!isset(me()['sidebar'])) ? 'nav-md' : me()['sidebar'] ?>">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col menu_fixed">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<a class="site_title"><i class="fa fa-money"></i> <span>Yaperi</span></a>
					</div>

					<div class="clearfix"></div>

					<!-- menu profile quick info -->
					<div class="profile clearfix">
						<div class="profile_pic">
							<?php if(is_null(me()['photo'])) {?>
							<img src="<?= base_url() ?>assets/admin/images/user.png" alt="..."
								class="img-circle profile_img">
							<?php }else{ ?>
							<img src="<?= base_url() ?>assets/admin/photos/<?= me()['photo'] ?>" alt="..."
								class="img-circle profile_img">
							<?php } ?>
						</div>
						<div class="profile_info">
							<span>Selamat Datang</span>
							<h2><?php echo me()['name'] ?></h2>
						</div>
					</div>
					<!-- /menu profile quick info -->

					<br />

					<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<h3>Menu Utama</h3>
							<ul class="nav side-menu">

								<?php menu('home', $menu, 'fa fa-home', 'Beranda') ?>

								<li
									 <?php if(me()['sidebar'] == 'nav-md' && $menu == 'master') echo 'class="active"' ?>>
									 <a><i class="fa fa-hdd-o <?php if(me()['sidebar'] == 'nav-sm' && $menu == 'master') echo 'sm-active' ?>"></i> Master 
									<span class="fa fa-chevron-down"></span></a>

									<?php if(me()['sidebar'] == 'nav-md'){ ?>

									<ul class="nav child_menu" <?php if($menu == "master") echo 'style="display: block;"' ?>>
										<li <?php if($sub == 'user') echo 'class="active"'?>>
											<a href="<?= base_url('user') ?>">User</a>
										</li>

										<li <?php if($sub == 'faculty') echo 'class="active"'?>>
											<a href="<?= base_url('faculty') ?>">Fakultas</a>
										</li>

										<li <?php if($sub == 'major') echo 'class="active"'?>>
											<a href="<?= base_url('major') ?>">Jurusan</a>
										</li>
										
										<li <?php if($sub == 'generation') echo 'class="active"'?>>
											<a href="<?= base_url('generation') ?>">Angkatan</a>
										</li>

										<li <?php if($sub == 'semester') echo 'class="active"'?>>
											<a href="<?= base_url('semester') ?>">Semester</a>
										</li>

										<li <?php if($sub == 'category') echo 'class="active"'?>>
											<a href="<?= base_url('category') ?>">Kategori Pembayaran</a>
										</li>

										<li <?php if($sub == 'type') echo 'class="active"'?>>
											<a href="<?= base_url('type') ?>">Jenis Pembayaran</a>
										</li>
									</ul>

									<?php } else { ?>
										<ul class="nav child_menu_master child_menu">

										</ul>
									<?php } ?>

								</li>

								<li
									<?php if(me()['sidebar'] == 'nav-md' && $menu == "students") echo 'class="active"' ?>>
									<a><i class="fa fa-users <?php if(me()['sidebar'] == 'nav-sm' && $menu == 'students') echo 'sm-active' ?>"></i>Mahasiswa 
									<span class="fa fa-chevron-down"></span></a>

									<?php if(me()['sidebar'] == 'nav-md'){ ?>

									<ul class="nav child_menu" <?php if($menu == "students") echo 'style="display: block;"' ?>>
										
										<li <?php if($sub == 'student') echo 'class="active"'?>>
											<a href="<?= base_url('student') ?>">Data Mahasiswa</a>
										</li>

										<li <?php if($sub == 'parent') echo 'class="active"'?>>
											<a href="<?= base_url('parent') ?>">Orang Tua / Wali</a>
										</li>
									</ul>

									<?php } else { ?>
										<ul class="nav child_menu_students child_menu">

										</ul>
									<?php } ?>

								</li>

								<li <?php if(me()['sidebar'] == 'nav-md' && $menu == "payment") echo 'class="active"' ?>><a>
									<i class="fa fa-money <?php if(me()['sidebar'] == 'nav-sm' && $menu == 'payment') echo 'sm-active' ?>"></i>Pembayaran 
									<span class="fa fa-chevron-down"></span></a>

									<?php if(me()['sidebar'] == 'nav-md'){ ?>

									<ul class="nav child_menu" <?php if($menu == "payment") echo 'style="display: block;"' ?>>
										<li <?php if($sub == 'price') echo 'class="active"'?>>
											<a href="<?= base_url('price') ?>">Manajemen Harga</a>
										</li>

										<li <?php if($sub == 'pay') echo 'class="active"'?>>
											<a href="<?= base_url('pay') ?>">Bayar</a>
										</li>

										<li <?php if($sub == 'transaction') echo 'class="active"'?>>
											<a href="<?= base_url('transaction') ?>">Transaksi</a>
										</li>
									</ul>

									<?php } else { ?>
										<ul class="nav child_menu_payment child_menu">

										</ul>
									<?php } ?>

								</li>

								<li <?php if(me()['sidebar'] == 'nav-md' && $menu == "report") echo 'class="active"' ?>><a>
									<i class="fa fa-bar-chart <?php if(me()['sidebar'] == 'nav-sm' && $menu == 'report') echo 'sm-active' ?>"></i>Laporan 
									<span class="fa fa-chevron-down"></span></a>

									<?php if(me()['sidebar'] == 'nav-md'){ ?>

									<ul class="nav child_menu" <?php if($menu == "report") echo 'style="display: block;"' ?>>

										<li <?php if($sub == 'income') echo 'class="active"'?>>
											<a href="<?= base_url('income') ?>">Pendapatan</a>
										</li>
									
									</ul>

									<?php } else { ?>
										<ul class="nav child_menu_report child_menu">

										</ul>
									<?php } ?>

								</li>

							</ul>
						</div>

					</div>
					<!-- /sidebar menu -->

					<!-- /menu footer buttons -->
					<!-- <div class="sidebar-footer hidden-small">
						<a data-toggle="tooltip" data-placement="top" title="Settings">
							<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="FullScreen">
							<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Lock">
							<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
							<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
						</a>
					</div> -->
					<!-- /menu footer buttons -->
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<nav>
						<div class="nav toggle">
							<a id="menu_toggle" onclick="setSidebar()"><i class="fa fa-bars"></i></a>
						</div>

						<ul class="nav navbar-nav navbar-right">
							<li class="">
								<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
									aria-expanded="false">
									<?php if(is_null(me()['photo'])) {?>
									<img src="<?= base_url() ?>assets/admin/images/user.png" alt=""><?= me()['name'] ?>
									<?php }else{?>
									<img src="<?= base_url() ?>assets/admin/photos/<?= me()['photo'] ?>"
										alt=""><?= me()['name'] ?>
									<?php } ?>
									<span class=" fa fa-angle-down"></span>
								</a>
								<ul class="dropdown-menu dropdown-usermenu pull-right">
									<li><a href="<?= base_url('change_password')?>"> Ganti Password</a></li>
									<li><a class="logout"><i class="fa fa-sign-out pull-right"></i> Keluar</a></li>
								</ul>
							</li>

							<!-- <li role="presentation" class="dropdown">
								<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
									aria-expanded="false">
									<i class="fa fa-envelope-o"></i>
									<span class="badge bg-green">6</span>
								</a>
								<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
									<li>
										<a>
											<span class="image"><img src="<?= base_url() ?>assets/admin/images/img.jpg"
													alt="Profile Image" /></span>
											<span>
												<span>John Smith</span>
												<span class="time">3 mins ago</span>
											</span>
											<span class="message">
												Film festivals used to be do-or-die moments for movie makers. They were
												where...
											</span>
										</a>
									</li>
								</ul>
							</li> -->

						</span>
					</nav>
				</div>
			</div>
			<!-- /top navigation -->

			<!-- page content -->
			<div class="right_col" role="main">
				<div>
					<?php $this->load->view($view) ?>
				</div>
			</div>
			<!-- /page content -->

			<!-- footer content -->
			<footer>
				<div class="pull-right">
					<a>Yaperi.com</a> - 2019
				</div>
				<div class="clearfix"></div>
			</footer>
			<!-- /footer content -->
			<?php if(!empty($_modal)) $this->load->view($_modal) ?>
		</div>
	</div>

	<!-- jQuery -->
	<script src="<?= base_url() ?>assets/admin/vendors/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?= base_url() ?>assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/vendors/moment/min/moment.min.js"></script>
	<script src="<?= base_url() ?>assets/admin/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
	<!-- FastClick -->
	<script src="<?= base_url() ?>assets/admin/vendors/fastclick/lib/fastclick.js"></script>
	<!-- NProgress -->
	<script src="<?= base_url() ?>assets/admin/vendors/nprogress/nprogress.js"></script>
	<!-- jQuery custom content scroller -->
	<script
		src="<?= base_url() ?>assets/admin/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js">
	</script>
	<!-- PNotify -->
	<script src="<?= base_url() ?>assets/admin/vendors/pnotify/dist/pnotify.js"></script>
	<script src="<?= base_url() ?>assets/admin/vendors/pnotify/dist/pnotify.buttons.js"></script>
	<script src="<?= base_url() ?>assets/admin/vendors/pnotify/dist/pnotify.nonblock.js"></script>
	<!-- Datatables -->
	<script src="<?= base_url() ?>assets/admin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?= base_url() ?>assets/admin/vendors/sweetalert/sweetalert.min.js"></script>
	<script src="<?= base_url() ?>assets/admin/js/ajax.js"></script>

	<?php if(!empty($scripts)) $this->load->view($scripts) ?>

	<script>
		$(document.body).on('click', '.logout', function (e) {
			swal({
					title: "Konfirmasi",
					text: "Apakah anda yakin ingin keluar ?",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Ya, Keluar",
					closeOnConfirm: false
				},
				function () {
					let url = "<?= base_url('logout') ?>";
					let type = 'get';
					postJson(url, type, data = {}, function (err, response) {
						if (response) {
							window.location.href = "<?= base_url('login') ?>";
						} else {
							console.log('ini error : ', err);
						}
					});
				});

		});

		function setSidebar() {
			doSomething('<?= base_url() ?>auth/setSidebar');
			setTimeout(() => {
				location.reload();
			}, 200);
		}

			let nav = '<?= me()['sidebar'] ?>';
			if(nav == 'nav-sm'){
				setTimeout(() => {
					$(".child_menu_master").html(
						"<li><a href='<?= base_url() ?>user'>User</a></li>"+
						"<li><a href='<?= base_url() ?>faculty'>Fakultas</a></li>"+
						"<li><a href='<?= base_url() ?>major'>Jurusan</a></li>"+
						"<li><a href='<?= base_url() ?>generation'>Angkatan</a></li>"+
						"<li><a href='<?= base_url() ?>semester'>Semester</a></li>"+
						"<li><a href='<?= base_url() ?>category'>Kategory Pembayaran</a></li>"+
						"<li><a href='<?= base_url() ?>type'>Jenis Pembayaran</a></li>"
					);
				}, 500);
				setTimeout(() => {
					$(".child_menu_students").html(
						"<li><a href='<?= base_url() ?>student'>Data Mahasiswa</a></li>"+
						"<li><a href='<?= base_url() ?>parent'>Orang Tua</a></li>"
					);
				}, 500);
				setTimeout(() => {
					$(".child_menu_payment").html(
						"<li><a href='<?= base_url() ?>price'>Manajemen Harga</a></li>"+
						"<li><a href='<?= base_url() ?>pay'>Bayar</a></li>"+
						"<li><a href='<?= base_url() ?>transaction'>Transaksi</a></li>"
					);
				}, 500);
				setTimeout(() => {
					$(".child_menu_report").html(
						"<li><a href='<?= base_url() ?>income'>Pendapatan</a></li>"
					);
				}, 500);
			}
	</script>

	<!-- Custom Theme Scripts -->
	<script src="<?= base_url() ?>assets/admin/build/js/custom.js"></script>

	<style>
		.custom-scroll {
			overflow-y: scroll;
			height: 450px;
		}

		.sm-active {
			color: #1abb9c;
		}

		.response-error {
			color: red;
		}

		.no-sort::after {
			display: none !important;
		}

		.no-sort {
			pointer-events: none !important;
			cursor: default !important;
		}

		a { 
			cursor: pointer;
		}

		footer {
			background: #f7f7f7;
			padding: 15px 20px;
			display: block;
		}

		.pointer {
			cursor: pointer;
		}

		::-webkit-scrollbar {
			width: 5px;
		}
		
		::-webkit-scrollbar-track {
			-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);     
			background: #ffffff;    
		}
		
		::-webkit-scrollbar-thumb {
			background: #1abb9c;
		}

	</style>
</body>

</html>