<div class="page-title">
	<div class="title_left">
		<h3>Ganti Password</h3>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Form Password</h2>
					<div class="clearfix"></div>
				</div>

				<div class="x_content">

					<form class="form-horizontal form-label-left form-data" novalidate=""
						action="<?= base_url('change') ?>" 
						method="post"
						enctype="multipart/form-data"
						data-rule="default_store"
						data-btn="#btn-save">

						<div class="item form-group group-old_password">
							<label for="old_password" class="control-label col-md-3">Password Lama</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="password" name="old_password" data-validate-length="1,2,3,4,5,6,7"
									class="form-control col-md-7 col-xs-12" required="required">
								<div id="response-old_password" class="response-error"></div>
							</div>
						</div>

						<div class="item form-group group-password">
							<label for="password" class="control-label col-md-3">Password Baru</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="password" name="password" data-validate-length="1,2,3,4,5,6,7"
									class="form-control col-md-7 col-xs-12" required="required">
								<div id="response-password" class="response-error"></div>
								<span>* Minimal 8 karakter</span>
							</div>
						</div>

						<div class="item form-group group-confirm">
							<label for="confirm" class="control-label col-md-3">Konfirmasi Password</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="password" name="confirm" data-validate-length="1,2,3,4,5,6,7"
									class="form-control col-md-7 col-xs-12" required="required">
								<div id="response-confirm" class="response-error"></div>
							</div>
						</div>

						<div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-md-6 col-md-offset-3">
								<button id="btn-save" type="submit" class="btn btn-success">Simpan</button>
							</div>
						</div>

					</form>

				</div>
			</div>
		</div>
	</div>
</div>