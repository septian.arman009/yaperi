<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($user)) echo $user->name ?>" class="form-control col-md-7 col-xs-12"
			name="name" placeholder="nama lengkap ex. Spongebob Squarepants" required="required" type="text">
	</div>
</div>

<div class="item form-group group-email">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($user)) echo $user->email ?>" type="email" name="email"
			required="required" class="form-control col-md-7 col-xs-12">
		<div id="response-email" class="response-error"></div>
		<?php if(empty($user)){ ?>
			<input type="checkbox" name="verification" id="checkbox"> Verifikasi Email
		<?php } ?>
	</div>
</div>

<?php if(empty($user)){ ?>
<div class="item form-group">
	<label for="password" class="control-label col-md-3">Password</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input id="password" type="password" name="password" data-validate-length="1,2,3,4,5,6,7,8"
			class="form-control col-md-7 col-xs-12" required="required">
		<span>* Minimal 8 karakter</span>
	</div>
</div>

<div class="item form-group">
	<label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">Ulangi Password</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input id="password2" type="password" name="password2" data-validate-linked="password"
			class="form-control col-md-7 col-xs-12" required="required">
	</div>
</div>

<?php } ?>

<div class="item form-group group-photo">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Foto</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input id="photo" class="form-control col-md-7 col-xs-12" name="photo" type="file">
		<span id="response-photo" class="response-error"></span>
	</div>
</div>

<?php if(!empty($user) && !is_null($user->photo)){ ?>
<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<img src="<?= base_url('assets/admin/photos/'.$user->photo) ?>" alt="Photo" width="200px" height="200px"
			style="border-radius: 10px">
	</div>
</div>
<?php } ?>

<div class="ln_solid"></div>
<div class="form-group">
	<div class="col-md-6 col-md-offset-3">
		<button id="btn-save" type="submit" class="btn btn-success">Simpan</button>
	</div>
</div>