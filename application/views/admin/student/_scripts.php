<!-- validator -->
<script src="<?= base_url() ?>assets/admin/vendors/validator/validator.js"></script>
<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>
<script src="<?= base_url() ?>assets/admin/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>

<script>
	$(document).ready(function () {
		if (document.body.contains(document.getElementById('student-table'))) {
			$('#student-table tfoot th').each(function () {
				var title = $(this).text();
				if (title != '')
					var inp = '<input type="text" class="form-control footer-s" placeholder="' + title +
						'" id="' + title + '" />';
				$(this).html(inp);
			});

			var table = $('#student-table').DataTable({
				"processing": true,
				"serverSide": true,
				"order": [
					[0, 'desc']
				],
				"ajax": {
					"url": '<?= base_url('student/table') ?>',
					"type": "POST"
				}
			});

			table.columns().every(function () {
				var that = this;
				$('input', this.footer()).on('keyup change', function () {
					if (that.search() !== this.value) {
						that.search(this.value).draw();
					}
				});
			});
		}
	});
	
	$(":input").inputmask();

	function custom_response($form, rule, message) {
		$form.trigger("reset");
		new PNotify({
			title: 'Sukses',
			text: message,
			type: 'success',
			hide: false,
			styling: 'bootstrap3'
		});
	}

	function getCity(province_id) {
		$("#district_id").html("<option value=''>- Pilih Kecamatan -</option>");
		$("#village_id").html("<option value=''>- Pilih Kelurahan -</option>");
		loadView('<?= base_url() ?>student/getCity/'+province_id, '#regency_id');
	}

	function getSubDistrict() {
		$("#village_id").html("<option value=''>- Pilih Kelurahan -</option>");
		let city_id = $("#regency_id").val();
		loadView('<?= base_url() ?>student/getSubDistrict/'+city_id, '#district_id');
	}
	
	function getVillageOffice() {
		let district_id = $("#district_id").val();
		loadView('<?= base_url() ?>student/getVillageOffice/'+district_id, '#village_id');
	}

</script>

<style>
	#student-table_filter {
		display: none;
	}
</style>