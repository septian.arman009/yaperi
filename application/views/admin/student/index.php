<div class="page-title">
	<div class="title_left">
		<h3>Mahasiswa</h3>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Daftar Mahasiswa</h2>
					<div class="clearfix"></div>
				</div>
				<?php flash() ?>
				<a href="<?= base_url('student/create') ?>" class="btn btn-default">Tambah Data</a>
				<hr>
				<div class="x_content table-responsive">
					<table id="student-table" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th id="th" width="7%">ID</th>
								<th id="th" width="10%">No. Induk</th>
								<th id="th">Nama</th>
								<th id="th" width="10%">Angkatan</th>
								<th id="th" width="10%">Semester</th>
								<th id="th">Jurusan</th>
								<th id="th" width="13%">Mobile</th>
								<th id="th" class="no-sort" width="17%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">ID</th>
								<th class="footer">No. Induk</th>
								<th class="footer">Nama</th>
								<th class="footer">Angkatan</th>
								<th class="footer">Semester</th>
								<th class="footer">Jurusan</th>
								<th class="footer">Mobile</th>
								<th class="footer"></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>