<?php
$this->load->library('fpdf');
class PDF extends FPDF
{
	//Page header
	function Header()
	{
		//Logo
		$this->Image('assets/admin/images/yaperi_02.png',10, 10, 25, 25);
		//Arial bold 15
		$this->SetFont('Times','B',15);
		//pindah ke posisi ke tengah untuk membuat judul
		$this->Cell(80);
		//judul
        $this->Cell(50,10,'YAYASAN PENDIDIKAN AR-RIDHO',0,0,'C');
        $this->Ln(5);
		$this->SetFont('Times','B',14);
		$this->Cell(80);
        $this->Cell(50,10,'SEKOLAH TINGGI AGAMA ISLAM (STAI), YAPERI CIBINONG',0,0,'C');
        $this->Ln(5);
		$this->SetFont('Times','',12);
		$this->Cell(80);
        $this->Cell(50,10,'Jl. Raya Jakarta Bogor KM 45 Pekansari Cibinong Bogor 16915',0,0,'C');
        $this->Ln(5);
		$this->SetFont('Times','',12);
		$this->Cell(80);
        $this->Cell(50,10,'Telp. (021) 837-137-49 / E-mail: stai.yaperi.cbg@gmail.com',0,0,'C');
		//pindah baris
		$this->Ln(20);
		//buat garis horisontal
		$this->Line(10,37,200,37);
		$this->Line(10,38,200,38);
	}
 
	//Page Content
	function Content($header, $payments, $semesterName, $student)
	{   
		$this->SetFont('Times','B',12);
        $this->Cell(80);
        $this->Cell(40, 15,"Rancian Pembayaran $semesterName", 0, 0, 'C');
        $this->Ln(20);

        $this->SetFont('Times','',12);
        $this->Cell(25, 5,"NIM", 0, 0, 'L');
        $this->Cell(5, 5,":", 0, 0, 'L');
        $this->Cell(0, 5,$student->reg_number, 0, 0, 'L');
        $this->Ln();

        $this->Cell(25, 5,"Nama", 0, 0, 'L');
        $this->Cell(5, 5,":", 0, 0, 'L');
        $this->Cell(0, 5,$student->student_name, 0, 0, 'L');
        $this->Ln();

        $this->Cell(25, 5,"Jurusan", 0, 0, 'L');
        $this->Cell(5, 5,":", 0, 0, 'L');
        $this->Cell(0, 5,$student->major_name, 0, 0, 'L');
        $this->Ln();

        $this->Cell(25, 5,"Angkatan", 0, 0, 'L');
        $this->Cell(5, 5,":", 0, 0, 'L');
        $this->Cell(0, 5,$student->generation_name, 0, 0, 'L');
        $this->Ln();

        $this->Cell(25, 5,"Semester", 0, 0, 'L');
        $this->Cell(5, 5,":", 0, 0, 'L');
        $this->Cell(0, 5,str_replace('Semester-','',$semesterName), 0, 0, 'L');
        $this->Ln(10);
        
		$this->SetFont('Times','',12);
		// Column widths
        $w = array(10, 100, 40, 40);
        // Header
        for($i=0;$i<count($header);$i++)
            $this->Cell($w[$i],10,$header[$i],1,0,'C');
        $this->Ln();
        // Data
        $no = 1;
        $total = 0;
        $totalBill = 0;
        foreach($payments as $payment)
        {
            $total += $payment['pay'];
            $totalBill += $payment['bill']; 
            $this->Cell($w[0],7,$no++,1,0);
            $this->Cell($w[1],7,$payment['type_name'],1,0,'L');
            $this->Cell($w[2],7,str_replace('Rp. ', '', toRp($payment['bill'])),1,0,'R');
            $this->Cell($w[3],7,str_replace('Rp. ', '', toRp($payment['pay'])),1,0,'R');
            $this->Ln();
        }
            $this->Cell(150,7,'Total Tagihan',1,0, 'R');
            $this->Cell(40,7,str_replace('Rp. ', '', toRp($totalBill)),1,0,'R');
            $this->Ln();
            $this->Cell(150,7,'Total Pembayaran',1,0, 'R');
            $this->Cell(40,7,str_replace('Rp. ', '', toRp($total)),1,0,'R');
            $this->Ln();
            $this->Cell(150,7,'Kekurangan',1,0, 'R');
            $this->Cell(40,7,str_replace('Rp. ', '', toRp($totalBill-$total)),1,0,'R');
            $this->Ln();
        // Closing line
        $this->Cell(array_sum($w),0,'','T');
	}
 
	//Page footer
	function Footer()
	{
		//atur posisi 1.5 cm dari bawah
		$this->SetY(-15);
		//buat garis horizontal
		$this->Line(10,$this->GetY(),200,$this->GetY());
		//Arial italic 9
		$this->SetFont('Times','I',9);
		//nomor halaman
		$this->Cell(0,10,'Halaman '.$this->PageNo().' dari {nb}',0,0,'R');
	}
}
 
//contoh pemanggilan class
$pdf = new PDF();
$pdf->SetTitle('Riwayat Pembayaran Mahasiswa');

//Header
$header = array('No', 'Pembayaran', 'Tagihan', 'Bibayar');
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->Content($header, $payments, $semesterName, $student);
$pdf->Output();
