<!-- validator -->
<script src="<?= base_url() ?>assets/admin/vendors/raphael/raphael.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendors/morris.js/morris.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendors/validator/validator.js"></script>
<script src="<?= base_url() ?>assets/admin/js/form_request.js"></script>

<script>
	$(document).ready(function () {
		
        Morris.Donut({
            element: 'semester-graph',
            data: <?= json_encode($semesterPrice) ?>,
            colors: <?= json_encode($color) ?>,
            formatter: function (y) {
                return torp(y);
            },
            resize: true
        });

        $('#pay-list-table thead th').each(function () {
			var title = $(this).text();
			if (title != '')
				var inp = '<textarea type="text" class="form-control footer-s" placeholder="' + title +
					'" id="' + title + '"/>';
			$(this).html(inp);
		});

		var table = $('#pay-list-table').DataTable({
			"processing": true,
			"serverSide": true,
			"order": [
				[1, 'desc']
			],
			"ajax": {
				"url": '<?= base_url('pay/pay_list/table/'.$studentId) ?>',
				"type": "POST"
			}
		});

		table.columns().every(function () {
			var that = this;
			$('textarea', this.header()).on('keyup change', function () {
				if (that.search() !== this.value) {
					that.search(this.value).draw();
				}
			});
		});

		changeYear();
    });
    
    function info(message) {
        $("#md-modal").modal('show');
		$(".modal-title").html("Info");
		$(".modal-custom").html("<p style='padding: 10px; font-size: 18px; text-align: center'>"+message+"</p>");
    }

	function fullInfo() {
		$("#md-modal").modal('show');
		$(".modal-title").html("Infomasi Lengkap");
		loadView("<?= base_url('student/studentDetailModal/'.$studentId) ?>", ".modal-custom");
	}

    function changeYear() {
        let year = $("#year").val();
        loadView('<?= base_url() ?>pay/paymentGraph/<?= $studentId ?>/'+year, '#month-graph');
    }

	function paymentPrint(pageURL) {
        var left = (screen.width/2)-(800/2);
        var top = (screen.height/2)-(550/2);
        var targetWin = window.open (pageURL, 'Cetak Bukti Pembayaran', 
            'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+800+', height='+550+', top='+top+', left='+left);
    }

</script>