<div class="page-title">
	<div class="title_left">
		<h3>Mahasiswa <small> Edit Data </small></h3>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Form Mahasiswa</h2>
					<div class="clearfix"></div>
				</div>
				
				<a href="<?= base_url('student') ?>" class="btn btn-default">Kembali</a>

				<div class="ln_solid"></div>

				<div class="x_content">

					<form class="form-horizontal form-label-left form-data" novalidate="" 
							action="<?= base_url('student/update/'. $student->student_id) ?>"
							method="post"
							enctype="multipart/form-data"
							data-rule="default_update"
                            data-redirect="<?= base_url('student') ?>"
							data-btn="#btn-save">

						<?php $this->load->view('admin/student/_form') ?>

					</form>

				</div>
			</div>
		</div>
	</div>
</div>