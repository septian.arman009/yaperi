<div class="page-title">
	<div class="title_left">
		<h3>Pembayaran</h3>
	</div>
	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-3 col-sm-3 col-xs-12 widget widget_tally_box">
			<div class="x_panel fixed_height_410">
				<div class="x_content">

					<div class="flex">
						<ul class="list-inline widget_profile_box">
							<li>
								<a>
									<i onclick="info('<?= $student->mobile ?>')" title="<?= $student->mobile ?>"
										class="fa fa-phone"></i>
								</a>
							</li>
							<li>
								<img src="<?= base_url('assets/admin/images/user.png') ?>" alt="..."
									class="img-circle profile_img">
							</li>
							<li>
								<a>
									<i onclick="fullInfo()" title="Informasi Lengkap" class="fa fa-exclamation"></i>
								</a>
							</li>
						</ul>
					</div>

					<h4 class="name"><b><?= $student->student_name ?></b></h4>

					<div class="flex">
						<ul class="list-inline count2">
							<li>
								<h3><?= str_replace("Semester-", "", $student->semester_alias) ?></h3>
								<span>Semester</span>
							</li>
							<li>
								<h3></h3>
								<span></span>
							</li>
							<li>
								<h3><?= $student->join_year ?></h3>
								<span>Masuk</span>
							</li>
						</ul>
					</div>

					<table>
						<tr>
							<td id="td">NIM</td>
							<td id="td">:</td>
							<td id="td"><b><?= $student->reg_number ?></b></td>
						</tr>
						<tr>
							<td id="td">Jurusan</td>
							<td id="td">:</td>
							<td id="td"><b><?= $student->major_name ?></b></td>
						</tr>
						<tr>
							<td id="td">Angkatan</td>
							<td id="td">:</td>
							<td id="td"><b><?= $student->generation_name ?></b></td>
						</tr>
						<tr>
							<td id="td">Tahun Lulus</td>
							<td id="td">:</td>
							<td id="td"><b><?= ($student->graduate_year == 0) ? '-' : $student->graduate_year ?></b>
							</td>
						</tr>
					</table>

				</div>
			</div>
		</div>

		<div class="col-md-9 col-sm-9 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Daftar Pembayaran</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content table-responsive">

					<a href="<?= base_url('student') ?>" class="btn btn-default"> Kembali</a>
					<a target="_blank" href="<?= base_url('student/printAll/'.$studentId) ?>" class="btn btn-default">
						Cetak Riwayat Pembayaran</a>

					<h2>Total Tagihan <?= $generationName ?> adalah <?= toRp($billing) ?></h2>

					<table class="table table-bordered">
						<thead>
							<th width="20%">Semester</th>
							<th width="30%">Jenis Pembayaran</th>
							<th width="25%">Tagihan</th>
							<th width="25%">Dibayar</th>
						</thead>
					</table>
					<div class="custom-scroll">
						<table class="table table-bordered">
							<tbody>
								<?php $no = 1; foreach ($semesters as $semester) { 
									if($semester['leave'] == 'yes'){ ?>
								<tr>
									<td id="cuti" width="20%"><?= $semester['semesterName'] ?></td>
									<td id="cuti" width="30%">Cuti <?= $semester['semesterName'] ?></td>
									<td id="cuti" width="25%"><?= $semester['price'] ?></td>
									<td id="cuti" width="25%"><?= $semester['price'] ?></td>
								</tr>
								<?php }else{ ?>
								<?php if($semester['priceList']){ 
                                        $price_length = count($semester['priceList']);
                                        $data = 0;
                                        foreach ($semester['priceList'] as $sm) { 
                                        $data++;
                                        $payment_type = ($sm['installment'] > 0) ? $sm['payment_type']." (Bulan #".$sm['installment'].")" : $sm['payment_type']?>

								<tr>
									<?php if($data == 1){ ?>
									<td width="20.1%" rowspan="<?= $price_length ?>">
										<strong><?= $semester['semesterName'] ?></strong>
										<br>
										<a target="_blank"
											href="<?= base_url('student/payment_detail/print_payment/'.$student->student_id.'/'.$semester['semesterId'].'/'.$semester['semesterName']) ?>"><i
												class="fa fa-print"> Cetak Pembayaran</i></a>
									</td>
									<?php } ?>
									<td width="30.1%"><?= $payment_type ?></td>
									<td width="25.1%"><?= toRp($sm['price']) ?></td>
									<td width="25%"><?= toRp($sm['paid']) ?></td>
								</tr>

								<?php if($data == $price_length){ ?>
								<tr>
									<td id="sp" width="20%"></td>
									<td id="sp" width="30%"></td>
									<td id="sp" width="25%"></td>
									<td id="sp" width="25%"></td>
								</tr>
								<?php } } } ?>


								<?php } } ?>
							</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Detail Pembayaran</h2>
					<div class="clearfix"></div>
				</div>

				<div class="row">
					<div class="col-md-3 col-sm-3 col-xs-12">
						<select id="year" class="form-control" onchange="changeYear()">
							<?php 
								for ($i=1980; $i <= 2099 ; $i++) { 
									$selected = ($i == $year) ? 'Selected' : '';
									echo "<option $selected value='$i'>$i</option>";
								}
							?>
						</select>
					</div>
				</div>

				<div class="ln_solid"></div>

				<div class="row">
					<div class="col-md-3 col-sm-3 col-xs-12">
						<div id="semester-graph" style="width:100%; height:250px;"></div>
					</div>

					<div class="col-md-9 col-sm-9 col-xs-12">
						<div id="month-graph" style="width:100%; height:280px;"></div>
					</div>
				</div>

				<div class="ln_solid"></div>

				<div class="x_content table-responsive">
					<table id="pay-list-table" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th id="th" width="7%">ID</th>
								<th id="th" width="12%">No. Invoice</th>
								<th id="th" width="10%">Angkatan</th>
								<th id="th" width="9%">Semester</th>
								<th id="th">Mahasiswa</th>
								<th id="th" width="10%">Tagihan</th>
								<th id="th" width="15%">Keterangan</th>
								<th id="th" width="10%">Tgl Bayar</th>
								<th id="th" class="no-sort" width="16%">Action</th>
							</tr>
						</thead>
					</table>

				</div>
			</div>
		</div>
	</div>

</div>

<style>
	.active-type {
		color: #da1a3e;
		font-size: 18px;
		font-style: italic;
	}

	#sp {
		background: #1abb9c;
	}

	#cuti {
		background: #ffdfa4;
	}

	#td {
		padding: 5px;
	}

	#td1 {
		padding: 5px;
	}

	#pay-list-table_filter {
		display: none;
	}

	.timeline .tags {
		width: 100px !important;
	}

	.custom-scroll {
		margin-top: -21px;
		min-height: 500px !important;
	}

	.timeline .block {
		margin: 0 0 0 120px !important;
	}
</style>