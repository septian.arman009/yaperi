<div class="item form-group group-reg_number">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor Induk Mahasiswa</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($student)) echo $student->reg_number ?>" class="form-control col-md-7 col-xs-12"
			name="reg_number" required="required" type="text" data-inputmask="'mask': '9999.9.999'">
		<span id="response-reg_number" class="response-error"></span>
	</div>
</div>
<!-- 
<div class="item form-group group-reg_number2">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">NIRM</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($student)) echo $student->reg_number2 ?>" class="form-control col-md-7 col-xs-12"
			name="reg_number2" required="required" type="text" data-inputmask="'mask': '9999.9.999'">
		<span id="response-reg_number2" class="response-error"></span>
	</div>
</div> -->

<div class="item form-group group-nisn">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">NISN</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($student)) echo $student->nisn ?>" class="form-control col-md-7 col-xs-12"
			name="nisn" type="number">
		<span id="response-nisn" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-id_card">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">No. KTP</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($student)) echo $student->id_card ?>" class="form-control col-md-7 col-xs-12"
			name="id_card" required="required" type="number">
		<span id="response-id_card" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-npwp">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">No. NPWP</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($student)) echo $student->npwp ?>" class="form-control col-md-7 col-xs-12"
			name="npwp" type="text" data-inputmask="'mask': '99.999.999.9-999.999'">
		<span id="response-npwp" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-generation_id">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Angkatan</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
	<?php if(empty($pay)){ ?>
		<select name="generation_id" id="generation_id" class="form-control" required="required">
			<option value="">- Pilih Angkatan -</option>
			<?php
				foreach ($generations as $generation) {
					if(!empty($student) && $generation->generation_id == $student->generation_id){
						echo "<option selected value='$generation->generation_id'>$generation->generation_name</option>";
					}else{
						echo "<option value='$generation->generation_id'>$generation->generation_name</option>";
					}
                }
			?>
	<?php }else{?>
		<input value="<?= $student->generation_name ?>" class="form-control col-md-7 col-xs-12" type="text" readonly>
		<input value="<?= $student->generation_id ?>" style="display: none" name="generation_id" type="text">
	<?php } ?>
		</select>
	</div>
</div>

<div class="item form-group group-semester_id">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Semester</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
	<?php if(empty($pay)){ ?>
		<select name="semester_id" id="semester_id" class="form-control" required="required">
			<option value="">- Pilih Semester -</option>
			<?php
				foreach ($semesters as $semester) {
					if(!empty($student) && $semester->semester_id == $student->semester_id){
						echo "<option selected value='$semester->semester_id'>$semester->semester_name</option>";
					}else{
						echo "<option value='$semester->semester_id'>$semester->semester_name</option>";
					}
                }
			?>
	<?php }else{?>
		<input value="<?= $student->semester_alias ?>" class="form-control col-md-7 col-xs-12" type="text" readonly>
		<input value="<?= $student->semester_id ?>" style="display: none" name="semester_id" type="text">
	<?php } ?>
		</select>
	</div>
</div>

<div class="item form-group group-student_name">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Mahasiswa</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($student)) echo $student->student_name ?>"
			class="form-control col-md-7 col-xs-12" name="student_name" required="required" type="text">
		<span id="response-student_name" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-birth_place">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Tempat Lahir</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($student)) echo $student->birth_place ?>"
			class="form-control col-md-7 col-xs-12" name="birth_place" required="required" type="text">
		<span id="response-birth_place" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-birth_date">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Lahir</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($student)) echo toIndoDate($student->birth_date) ?>" name="birth_date" type="text"
			class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '99/99/9999'" required="required">
		<span id="response-birth_date" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-gender">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Kelamin</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div id="gender" class="btn-group" data-toggle="buttons">
			<label class="btn btn-default <?php if(!empty($student) && $student->gender == 'Laki-Laki'){ echo 'active'; } ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
				<input <?php if(!empty($student) && $student->gender == 'Laki-Laki'){ echo 'checked'; } ?> type="radio" name="gender" value="Laki-Laki"> &nbsp; Laki - Laki &nbsp;
			</label>
			<label class="btn btn-primary <?php if(!empty($student) && $student->gender == 'Perempuan'){ echo 'active'; } ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
				<input <?php if(!empty($student) && $student->gender == 'Perempuan'){ echo 'checked'; } ?> type="radio" name="gender" value="Perempuan"> Perempuan
			</label>
		</div>
		<span id="response-gender" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-religion">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Agama</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="religion" id="religion" class="form-control" required="required">
		<option value="">- Pilih Agama -</option>
			<?php
				$religions = ['Islam', 'Katolik', 'Protestan', 'Hindu', 'Budha'];
				foreach ($religions as $key => $religion) {
					if(!empty($student) && $religion == $student->religion){
						echo "<option selected value='$religion'>$religion</option>";
					}else{
						echo "<option value='$religion'>$religion</option>";
					}
                }
			?>
		</select>
	</div>
</div>

<div class="item form-group group-nationality">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Kewarganegaraan</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?= (!empty($student)) ? $student->nationality : 'Indonesia' ?>" class="form-control col-md-7 col-xs-12"
			name="nationality" required="required" type="text">
		<span id="response-nationality" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-street">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input name="street" type="text" class="form-control col-md-7 col-xs-12" required="required" placeholder="Jalan"
			value="<?php if(!empty($student)) echo $student->street ?>">
		<span id="response-street" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-rtrw">
	<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input name="rtrw" type="text" class="form-control col-md-7 col-xs-12" required="required" placeholder="RT/RW"
			value="<?php if(!empty($student)) echo $student->rtrw ?>" data-inputmask="'mask': '99/99'">
		<span id="response-rtrw" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-province_id">
	<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="province_id" id="province_id" class="form-control col-md-7 col-xs-12" required="required" onchange="getCity($(this).val())">
			<option value="">- Pilih Provinsi -</option>
			<?php foreach ($provinces as $province) {
				$selected = ($student->province_id == $province->id) ? "selected" : "";
				echo "<option $selected value='$province->id'>".$province->name."</option>";
			} ?>
		</select>
		<span id="response-province_id" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-regency_id">
	<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="regency_id" id="regency_id" class="form-control col-md-7 col-xs-12" required="required" onchange="getSubDistrict()">
			<option value="">- Pilih Kota -</option>
			<?php foreach ($regencies as $regency) {
				$selected = ($student->regency_id == $regency->id) ? "selected" : "";
				echo "<option $selected value='$regency->id'>".$regency->name."</option>";
			} ?>
		</select>
		<span id="response-regency_id" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-district_id">
	<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="district_id" id="district_id" class="form-control col-md-7 col-xs-12" required="required" onchange="getVillageOffice()">
			<option value="">- Pilih Kecamatan -</option>
			<?php foreach ($districts as $district) {
				$selected = ($student->district_id == $district->id) ? "selected" : "";
				echo "<option $selected value='$district->id'>".$district->name."</option>";
			} ?>
		</select>
		<span id="response-district_id" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-village_id">
	<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="village_id" id="village_id" class="form-control col-md-7 col-xs-12" required="required">
			<option value="">- Pilih Kelurahan -</option>
			<?php foreach ($villages as $village) {
				$selected = ($student->village_id == $village->id) ? "selected" : "";
				echo "<option $selected value='$village->id'>".$village->name."</option>";
			} ?>
		</select>
		<span id="response-village_id" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-zipcode">
	<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input type="number" name="zipcode" class="form-control col-md-7 col-xs-12" placeholder="Kode Pos"
			value="<?php if(!empty($student)) echo $student->zipcode ?>">
		<span id="response-zipcode" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-phone">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor Telpon</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($student)) echo $student->phone ?>" name="phone" type="text"
			class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '(999) 9999-9999'">
		<span id="response-phone" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-mobile">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor Hanphone</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($student)) echo $student->mobile ?>"
			class="form-control col-md-7 col-xs-12" name="mobile" type="text" data-inputmask="'mask': '9999-9999-99999'">
		<span id="response-mobile" class="response-error"></span>
	</div>
</div>

<div class="item form-group group-email">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($student)) echo $student->email ?>" type="email" name="email"
			class="form-control col-md-7 col-xs-12" type="text">
		<div id="response-email" class="response-error"></div>
	</div>
</div>

<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Jurusan</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="major_id" id="major_id" class="form-control" required="required">
        <option value="">- Pilih Jurusan -</option>
			<?php
				foreach ($majors as $major) {
					if(!empty($student) && $major->major_id == $student->major_id){
						echo "<option selected value='$major->major_id'>$major->major_name</option>";
					}else{
						echo "<option value='$major->major_id'>$major->major_name</option>";
					}
                }
			?>
		</select>
	</div>
</div>

<div class="item form-group group-join_date">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Tahun Masuk</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="join_date" id="join_date" class="form-control" required="required">
		<option value="">- Pilih Tahun Masuk -</option>
			<?php
				for ($i = 1990; $i <= 2099; $i++) {
					if(!empty($student) && $i == $student->join_date){
						echo "<option selected value='$i'>$i</option>";
					}else{
						if($i == date('Y')){
							echo "<option selected value='$i'>$i</option>";
						}else{
							echo "<option value='$i'>$i</option>";
						}
					}
                }
			?>
		</select>
	</div>
</div>

<div class="item form-group group-graduate_date">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Tahun Lulus</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<select name="graduate_date" id="graduate_date" class="form-control">
		<option value="">- Pilih Tahun Lulus -</option>
			<?php
				for ($i = 1990; $i <= 2099; $i++) {
					if(!empty($student) && $i == $student->graduate_date){
						echo "<option selected value='$i'>$i</option>";
					}else{
						echo "<option value='$i'>$i</option>";
					}
                }
			?>
		</select>
	</div>
</div>


<div class="ln_solid"></div>
<div class="form-group">
	<div class="col-md-6 col-md-offset-3">
		<button id="btn-save" type="submit" class="btn btn-success">Simpan</button>
		<?php if(!empty($student)){ ?>
			<a href="<?= base_url() ?>parent/create/<?= $student->student_id ?>" class="btn btn-info">Data Orang Tua</a>
		<?php } ?>
	</div>
</div>