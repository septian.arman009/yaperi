<?php
$this->load->library('fpdf');
class PDF extends FPDF
{
	//Page header
	function Header()
	{
		//Logo
		$this->Image('assets/admin/images/yaperi_02.png',10, 10, 25, 25);
		//Arial bold 15
		$this->SetFont('Times','B',15);
		//pindah ke posisi ke tengah untuk membuat judul
		$this->Cell(80);
		//judul
        $this->Cell(50,10,'YAYASAN PENDIDIKAN AR-RIDHO',0,0,'C');
        $this->Ln(5);
		$this->SetFont('Times','B',14);
		$this->Cell(80);
        $this->Cell(50,10,'SEKOLAH TINGGI AGAMA ISLAM (STAI), YAPERI CIBINONG',0,0,'C');
        $this->Ln(5);
		$this->SetFont('Times','',12);
		$this->Cell(80);
        $this->Cell(50,10,'Jl. Raya Jakarta Bogor KM 45 Pekansari Cibinong Bogor 16915',0,0,'C');
        $this->Ln(5);
		$this->SetFont('Times','',12);
		$this->Cell(80);
        $this->Cell(50,10,'Telp. (021) 837-137-49 / E-mail: stai.yaperi.cbg@gmail.com',0,0,'C');
		//pindah baris
		$this->Ln(20);
		//buat garis horisontal
		$this->Line(10,37,200,37);
		$this->Line(10,38,200,38);
	}
 
	//Page Content
	function Content($header, $student, $billing,  $generationName, $semesters, $pay)
	{   
        $this->SetFont('Times','B',12);
        $this->Cell(80);
        $this->Cell(40, 15,"Riwayar Pembayaran", 0, 0, 'C');
        $this->Ln(15);

        $this->SetFont('Times','',12);
        $this->Cell(35, 5,"NIM", 0, 0, 'L');
        $this->Cell(5, 5,":", 0, 0, 'L');
        $this->Cell(0, 5,$student->reg_number, 0, 0, 'L');
        $this->Ln();

        $this->Cell(35, 5,"Nama", 0, 0, 'L');
        $this->Cell(5, 5,":", 0, 0, 'L');
        $this->Cell(0, 5,$student->student_name, 0, 0, 'L');
        $this->Ln();

        $this->Cell(35, 5,"Jurusan", 0, 0, 'L');
        $this->Cell(5, 5,":", 0, 0, 'L');
        $this->Cell(0, 5,$student->major_name, 0, 0, 'L');
        $this->Ln();

        $this->Cell(35, 5,"Angkatan", 0, 0, 'L');
        $this->Cell(5, 5,":", 0, 0, 'L');
        $this->Cell(0, 5,$student->generation_name, 0, 0, 'L');
        $this->Ln();

        $this->Cell(35, 5,"Semester", 0, 0, 'L');
        $this->Cell(5, 5,":", 0, 0, 'L');
        $this->Cell(0, 5,str_replace('Semester-','',$student->semester_alias), 0, 0, 'L');
        $this->Ln();

        $this->Cell(35, 5,"Total Tagihan", 0, 0, 'L');
        $this->Cell(5, 5,":", 0, 0, 'L');
        $this->Cell(0, 5,str_replace('Semester-','',toRp($billing)), 0, 0, 'L');
        $this->Ln();

        $this->Cell(35, 5,"Total Pembayaran", 0, 0, 'L');
        $this->Cell(5, 5,":", 0, 0, 'L');
        $this->Cell(0, 5,str_replace('Semester-','',toRp($pay)), 0, 0, 'L');
        $this->Ln(10);

		$this->SetFont('Times','',12);
		// Column widths
        $w = array(30, 100, 30, 30);
        // Header
        for($i=0;$i<count($header);$i++)
            $this->Cell($w[$i],10,$header[$i],1,0,'C');
        $this->Ln();
        // Data
        $no = 1;
        foreach($semesters as $semester)
        {
            if($semester['priceList']){
                $price_length = count($semester['priceList']);
                $data = 0;
                $totalBill = 0;
                $totalPay = 0;
                foreach ($semester['priceList'] as $sm) { 
                    $data++;
                    $totalBill += $sm['price'];
                    $totalPay += $sm['paid'];
                    $payment_type = ($sm['installment'] > 0) ? $sm['payment_type']." (Bulan #".$sm['installment'].")" : $sm['payment_type'];
                    $this->SetFont('Times','',12);
                    if($data == 1){
                        $this->Cell($w[0],7*$price_length,$semester['semesterName'],1,0,'C');
                    }else{
                        $this->Cell($w[0],7,'',0,0);
                    }
                
                    $this->Cell($w[1],7,$payment_type,1,0,'L');
                    $this->Cell($w[2],7,str_replace('Rp. ', '', toRp($sm['price'])),1,0,'R');
                    $this->Cell($w[3],7,str_replace('Rp. ', '', toRp($sm['paid'])),1,0,'R');
                    $this->Ln();

                    if($data == $price_length){
                        $this->SetFont('Times','B',12);
                        $this->Cell(130,7,'Total',1,0, 'R');
                        $this->Cell(30,7,str_replace('Rp. ', '', toRp($totalBill)),1,0,'R');
                        $this->Cell(30,7,str_replace('Rp. ', '', toRp($totalPay)),1,0,'R');
                        $this->Ln();
                    }

                }
            }
        }
        // Closing line
        $this->Cell(array_sum($w),0,'','T');
	}
 
	//Page footer
	function Footer()
	{
		//atur posisi 1.5 cm dari bawah
		$this->SetY(-15);
		//buat garis horizontal
		$this->Line(10,$this->GetY(),200,$this->GetY());
		//Arial italic 9
		$this->SetFont('Times','I',9);
		//nomor halaman
		$this->Cell(0,10,'Halaman '.$this->PageNo().' dari {nb}',0,0,'R');
	}
}
 
//contoh pemanggilan class
$pdf = new PDF();
$pdf->SetTitle('Riwayat Pembayaran Mahasiswa');

//Header
$header = array('Semester', 'Jenis Pembayaran', 'Tagihan', 'Bibayar');
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->Content($header, $student, $billing, $generationName, $semesters, $pay);
$pdf->Output();
