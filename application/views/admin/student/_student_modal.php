<div class="row" style="padding: 20px">
	<div class="col-md-12">
		<table>
			<tr>
				<td id="td">NIM</td>
				<td id="td">:</td>
				<td id="td"><b><?= $student->reg_number ?></b></td>
			</tr>
			<tr>
				<td id="td">Nama</td>
				<td id="td">:</td>
				<td id="td"><b><?= $student->student_name ?></b></td>
			</tr>
			<tr>
				<td id="td">Jurusan</td>
				<td id="td">:</td>
				<td id="td"><b><?= $student->major_name ?></b></td>
			</tr>
			<tr>
				<td id="td">Angkatan</td>
				<td id="td">:</td>
				<td id="td"><b><?= $student->generation_name ?></b></td>
			</tr>
			<tr>
				<td id="td">Semester</td>
				<td id="td">:</td>
				<td id="td"><b><?= $student->semester_alias ?></b></td>
			</tr>
            <tr>
				<td id="td">Tahun Masuk</td>
				<td id="td">:</td>
				<td id="td"><b><?= $student->join_year ?></b>
				</td>
			</tr>
			<tr>
				<td id="td">Tahun Lulus</td>
				<td id="td">:</td>
				<td id="td"><b><?= ($student->graduate_year == 0) ? '-' : $student->graduate_year ?></b>
				</td>
			</tr>
			<tr>
				<td id="td1">Jenis Kelamin</td>
				<td id="td1">:</td>
				<td id="td1"><b><?= $student->gender ?></b></td>
			</tr>
			<tr>
				<td id="td1">TTL</td>
				<td id="td1">:</td>
				<td id="td1"><b><?= $student->birth_place.', '.toIndoDate($student->birth_date) ?></b>
				</td>
			</tr>
			<tr>
				<td id="td1">Agama</td>
				<td id="td1">:</td>
				<td id="td1"><b><?= $student->religion ?></b></td>
			</tr>
			<tr>
				<td id="td1">Alamat</td>
				<td id="td1">:</td>
				<td id="td1"><b><?= $student->street.' RT'.$student->rtrw.' Kel.'
                            .$student->village.' Kec.'.$student->district.', '
                            .$student->regency.', '.$student->province.' '.$student->zipcode ?></b>
				</td>
			</tr>
			<tr>
				<td id="td1">Handphone</td>
				<td id="td1">:</td>
				<td id="td1"><b><?= $student->mobile ?></b>
				</td>
			</tr>
			<tr>
				<td id="td1">Rumah</td>
				<td id="td1">:</td>
				<td id="td1"><b><?= $student->phone ?></b>
				</td>
			</tr>
			<tr>
				<td id="td1">Email</td>
				<td id="td1">:</td>
				<td id="td1"><b><?= $student->email ?></b>
				</td>
			</tr>
		</table>
	</div>

</div>

<style>
    #td {
        padding: 10px;
    }
</style>