<div class="item form-group group-category_name">
	<label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori</label>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<input value="<?php if(!empty($category)) echo $category['category_name'] ?>" class="form-control col-md-7 col-xs-12"
			name="category_name" placeholder="ex. Pembayaran SKS" required="required" type="text">
        <div id="response-category_name" class="response-error"></div>
	</div>
</div>

<div class="ln_solid"></div>
<div class="form-group">
	<div class="col-md-6 col-md-offset-3">
		<button id="btn-save" type="submit" class="btn btn-success">Simpan</button>
	</div>
</div>