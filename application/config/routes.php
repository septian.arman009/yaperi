<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//Auth
$route['logout'] = 'user/logout';
$route['login'] = 'auth/login';
$route['login_check'] = 'auth/loginCheck';
$route['forgot_password'] = 'auth/forgotPassword';
$route['send_link'] = 'auth/sendLink';
$route['reset_password/(:any)'] = 'auth/resetPassword/$1';
$route['reset'] = 'auth/reset';
$route['verification/(:any)'] = 'auth/emailVerification/$1';

//User Management
$route['user'] = 'user/index';
$route['user/table'] = 'user/userTable';    
$route['user/create'] = 'user/create';
$route['user/store'] = 'user/store';
$route['user/edit/(:num)'] = 'user/edit/$1';
$route['user/update/(:num)'] = 'user/update/$1';
$route['user/destroy/(:num)'] = 'user/destroy/$1';

//Change Password
$route['change_password'] = 'user/changePassword';
$route['change'] = 'user/change';

//Generation
$route['generation'] = 'generation/index';
$route['generation/table'] = 'generation/generationTable';
$route['generation/create'] = 'generation/create';
$route['generation/store'] = 'generation/store';
$route['generation/edit/(:num)'] = 'generation/edit/$1';
$route['generation/update/(:num)'] = 'generation/update/$1';
$route['generation/destroy/(:num)'] = 'generation/destroy/$1';

//Semester
$route['semester'] = 'semester/index';
$route['semester/table'] = 'semester/semesterTable';
$route['semester/create'] = 'semester/create';
$route['semester/store'] = 'semester/store';
$route['semester/edit/(:num)'] = 'semester/edit/$1';
$route['semester/update/(:num)'] = 'semester/update/$1';
$route['semester/destroy/(:num)'] = 'semester/destroy/$1';

//Payment category
$route['category'] = 'payment_category/index';
$route['category/table'] = 'payment_category/categoryTable';
$route['category/create'] = 'payment_category/create';
$route['category/store'] = 'payment_category/store';
$route['category/edit/(:num)'] = 'payment_category/edit/$1';
$route['category/update/(:num)'] = 'payment_category/update/$1';
$route['category/destroy/(:num)'] = 'payment_category/destroy/$1';

//Payment category
$route['type'] = 'payment_type/index';
$route['type/table'] = 'payment_type/typeTable';
$route['type/create'] = 'payment_type/create';
$route['type/store'] = 'payment_type/store';
$route['type/edit/(:num)'] = 'payment_type/edit/$1';
$route['type/update/(:num)'] = 'payment_type/update/$1';
$route['type/destroy/(:num)'] = 'payment_type/destroy/$1';

//Price Management
$route['price'] = 'price/index';
$route['price/table'] = 'price/priceTable';
$route['price/create'] = 'price/create';
$route['price/store'] = 'price/store';
$route['price/edit/(:num)'] = 'price/edit/$1';
$route['price/update/(:num)'] = 'price/update/$1';
$route['price/destroy/(:num)'] = 'price/destroy/$1';
$route['price/type_check/(:num)/(:num)'] = 'price/typeCheck/$1/$2';
$route['price/price_detail/(:num)'] = 'price/priceDetail/$1';
$route['price/edit_modal/(:num)'] = 'price/editModal/$1';
$route['price/destroy_one/(:num)'] = 'price/destroyOne/$1';
$route['price/paid_leave/(:num)'] = 'price/paidLeave/$1';
$route['price/store_leave/(:num)'] = 'price/storeLeave/$1';

//Faculty
$route['faculty'] = 'faculty/index';
$route['faculty/table'] = 'faculty/facultyTable';
$route['faculty/create'] = 'faculty/create';
$route['faculty/store'] = 'faculty/store';
$route['faculty/edit/(:num)'] = 'faculty/edit/$1';
$route['faculty/update/(:num)'] = 'faculty/update/$1';
$route['faculty/destroy/(:num)'] = 'faculty/destroy/$1';

//Major
$route['major'] = 'major/index';
$route['major/table'] = 'major/majorTable';
$route['major/create'] = 'major/create';
$route['major/store'] = 'major/store';
$route['major/edit/(:num)'] = 'major/edit/$1';
$route['major/update/(:num)'] = 'major/update/$1';
$route['major/destroy/(:num)'] = 'major/destroy/$1';

//Payment student
$route['student'] = 'student/index';
$route['student/table'] = 'student/studentTable';
$route['student/create'] = 'student/create';
$route['student/store'] = 'student/store';
$route['student/edit/(:num)'] = 'student/edit/$1';
$route['student/update/(:num)'] = 'student/update/$1';
$route['student/destroy/(:num)'] = 'student/destroy/$1';
$route['student/payment_detail/(:num)'] = 'student/paymentDetail/$1';
$route['student/payment_detail/print_payment/(:num)/(:num)/(:any)'] = 'student/printPaymentSemester/$1/$2/$3';

//Pay
$route['pay'] = 'pay/index';
$route['pay/search/(:any)'] = 'pay/search/$1';
$route['pay/form_payment/(:num)'] = 'pay/formPayment/$1';
$route['pay/edit_pay/(:num)'] = 'pay/editPay/$1';
$route['pay/store_pay/(:num)'] = 'pay/storePay/$1';
$route['pay/update_pay/(:num)'] = 'pay/updatePay/$1';
$route['pay/pay_list/table/(:num)'] = 'pay/payList/$1';
$route['pay/pay_list/destroy/(:num)'] = 'pay/destroy/$1';
$route['pay/pay_leaves/(:num)/(:num)/(:num)'] = 'pay/payLeaves/$1/$2/$3';
$route['pay/store_leaves/(:num)/(:num)'] = 'pay/storeLeaves/$1/$2';

//Transaction
$route['transaction'] = 'transaction/index';
$route['transaction/table'] = 'transaction/transactionTable';
$route['transaction/destroy/(:num)'] = 'transaction/destroy/$1';
$route['transaction/edit/(:num)'] = 'transaction/edit/$1';
$route['transaction/update/(:num)/(:num)'] = 'transaction/update/$1/$2';
$route['transaction/remove/(:num)/(:num)/(:num)'] = 'transaction/remove/$1/$2/$3';
$route['transaction/edit_pay_amount/(:num)'] = 'transaction/editPayAmount/$1';
$route['transaction/update_pay_amount/(:num)'] = 'transaction/updatePayAmount/$1';
$route['transaction/payment_print/(:num)'] = 'transaction/paymentPrint/$1';
$route['transaction/leave_print/(:num)'] = 'transaction/leavePrint/$1';

//Parents
$route['parent'] = 'parents/index';
$route['parent/table'] = 'parents/parentTable';
$route['parent/create/(:num)'] = 'parents/create/$1';
$route['parent/store/(:num)'] = 'parents/store/$1';
$route['parent/update/(:num)/(:num)'] = 'parents/update/$1/$2';


//Parents
$route['income'] = 'report/income';
$route['print_monthly_report/(:num)/(:num)'] = 'report/printMonthly/$1/$2';
$route['print_yearly_report/(:num)'] = 'report/printYearly/$1';