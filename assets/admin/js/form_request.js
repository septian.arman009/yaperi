if (typeof (validator) != 'undefined') {
	$('form')
		.on('blur', 'input[required], input.optional, select.required', validator.checkField)
		.on('change', 'select.required', validator.checkField)
		.on('keypress', 'input[required][pattern]', validator.keypress);

	$('.multi.required').on('keyup blur', 'input', function () {
		validator.checkField.apply($(this).siblings().last()[0]);
	});
}

$('.form-data').submit(function (e) {
	e.preventDefault();
	var submit = true;

	if (typeof (validator) != 'undefined') {
		if (!validator.checkAll($(this))) {
			submit = false;
		}
	}

	if (submit) {
		$(".response-error").html('');
		$(".has-error").removeClass('has-error');

		let $form = $(this);
		let data = new FormData(this);
		let method = $form.attr('method');
		let url = $form.attr('action');
		let rule = $form.data('rule');
		let btn = $form.data('btn');
		$(btn).html('Loading..');

		postFormData(url, method, data, function (err, response) {
			if (response) {
				if ($.isEmptyObject(response.errors)) {
					document.activeElement.blur();
					$(btn).html('Simpan');
					form_save_response($form, rule, response.message);
				} else {
					$(btn).html('Simpan');
					$.each(response.errors, function (key, value) {
						$(".group-" + key).addClass('has-error');
						$("#response-" + key).append('<span>' + value + '</span>');
					});
				}
			} else {
				console.log("Error : ", err)
			}
		});
	}

	return false;
});

function form_save_response($form, rule, message) {
	if (rule == "default_store") {
		$form.trigger("reset");
		notify('Sukses', message, 'success');
	} else if (rule == "default_update") {
		window.location = $form.data('redirect');
	} else {
		custom_response($form, rule, message);
	}
}

$(document.body).on('click', '.form-delete', function (e) {
	let $form = $(this);
	let message = $form.data('confirm') ? $form.data('confirm') : 'Anda yakin ingin melakukan hal ini ?';
	swal({
			title: "Konfirmasi",
			text: message,
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Ya, Hapus",
			closeOnConfirm: false
		},
		function () {
			let url = $form.data('url');
			let type = 'delete';
			postJson(url, type, data = {}, function (err, response) {
				if (response) {
					if ($.isEmptyObject(response.errors)) {
						if ($.isEmptyObject(response.rules)) {

							if (response.id) {
								swal.close();
								baris = $('#form-' + response.id).closest('tr');
								baris.fadeOut(200, function () {
									$form.remove();
								});
							} else {
								location.reload();
							}

						} else {
							swal.close();
							custom_response(null, response.rules.rule, response.rules.message);
						}

					} else {
						swal.close();
						notify('Terjadi Kesalahan', response.errors, 'error');
					}
				} else {
					console.log('Error : ', err);
				}
			});
		});

});

function notify(title, text, type) {
	new PNotify({
		title: title,
		text: text,
		type: type,
		styling: 'bootstrap3'
	});
}

function torp(angka) {
	var reverse = angka.toString().split('').reverse().join(''),
		ribuan = reverse.match(/\d{1,3}/g);
	ribuan = ribuan.join('.').split('').reverse().join('');
	if (angka >= 0) {
		return "Rp. " + ribuan;
	} else {
		return "Rp. - " + ribuan;
	}

}

function moneyClean(money) {
	let fix = money.replace('Rp. ', '');
	return fix.replaceAll('.', '');
}

String.prototype.replaceAll = function (str1, str2, ignore) {
	return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, "\\$&"), (ignore ? "gi" : "g")), (typeof (str2) == "string") ? str2.replace(/\$/g, "$$$$") : str2);
}