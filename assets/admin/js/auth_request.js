$(document).ready(function(){
    $("#login-error").hide();
});

$('.form-auth').submit(function (e) {
    e.preventDefault();
    $("#login-error").hide();
    $("#response-error").html('<ul></ul>');
    
    let $form = $(this);
    let data = new FormData(this);
    let method = $form.attr('method');
    let url = $form.attr('action');
    let rule = $form.data('rule');
    let btn = $form.data('btn');
    $(btn).html("Loading..");
    postFormData(url, method, data, function (err, response) {
        if (response) {
            if ($.isEmptyObject(response.errors)) {
                window.location = $form.data('redirect');
            } else {
                $(btn).html($form.data('r_btn'));
                $("#login-error").show();
                $.each(response.errors, function (key, value) {
                    $("#response-error").find("ul").append('<li>' + value + '</li>');
                });
            }
        }else{
            console.log("Error :", err);
        }
    });
});