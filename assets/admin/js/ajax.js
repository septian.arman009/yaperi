function postFormData(url, method, data, callback) {
	$.ajax({
		url: url,
		type: method,
		data: data,
		processData: false,
		contentType: false,
		success: function (response) {
			return callback(null, response);
		},
		error: function (err) {
			return callback(true, err);
		}
	});
}

function postJson(url, method, data, callback) {
	$.ajax({
		url: url,
		type: method,
		dataType: 'json',
		data: data,
		success: function (response) {
			return callback(null, response);
		},
		error: function (err) {
			return callback(true, err);
		}
	});
}

function loadView(url, div) {
    $.ajax({
        url: url,
        success: function (data) {
            $(div).html(data);
        }
    });
}

function doSomething(url) {
    $.ajax({
        url: url,
        success: function () {
            console.log('Do something success...');
        }
    });
}